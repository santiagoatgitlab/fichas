--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.13
-- Dumped by pg_dump version 9.6.13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: cards; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA cards;


ALTER SCHEMA cards OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: unaccent; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS unaccent WITH SCHEMA public;


--
-- Name: EXTENSION unaccent; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION unaccent IS 'text search dictionary that removes accents';


--
-- Name: user_role_type; Type: TYPE; Schema: cards; Owner: postgres
--

CREATE TYPE cards.user_role_type AS ENUM (
    'r',
    'c',
    'a'
);


ALTER TYPE cards.user_role_type OWNER TO postgres;

--
-- Name: clean_final_deliveries(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.clean_final_deliveries() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN 
    DELETE FROM cards.final_deliveries
    WHERE id NOT IN (
        SELECT distinct(fd.id) FROM cards.final_deliveries fd
        INNER JOIN cards.deliveries d ON d.final_delivery_id = fd.id
    );
    RETURN NULL;
END;
$$;


ALTER FUNCTION public.clean_final_deliveries() OWNER TO postgres;

--
-- Name: touch_card_after_new_relation(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.touch_card_after_new_relation() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN 
    UPDATE cards.cards
    SET last_update = CURRENT_TIMESTAMP
    WHERE id = NEW.card_id;
    RETURN NULL;
END;
$$;


ALTER FUNCTION public.touch_card_after_new_relation() OWNER TO postgres;

--
-- Name: touch_card_after_relation_delete(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.touch_card_after_relation_delete() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN 
    UPDATE cards.cards
    SET last_update = CURRENT_TIMESTAMP
    WHERE id = OLD.card_id;
    RETURN NULL;
END;
$$;


ALTER FUNCTION public.touch_card_after_relation_delete() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: campaigns; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.campaigns (
    id integer NOT NULL,
    name character varying(255),
    begin_date date,
    end_date date,
    created_at timestamp without time zone DEFAULT now(),
    goal_currency character varying(3),
    goal_amount double precision
);


ALTER TABLE cards.campaigns OWNER TO postgres;

--
-- Name: campaigns_id_seq; Type: SEQUENCE; Schema: cards; Owner: postgres
--

CREATE SEQUENCE cards.campaigns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cards.campaigns_id_seq OWNER TO postgres;

--
-- Name: campaigns_id_seq; Type: SEQUENCE OWNED BY; Schema: cards; Owner: postgres
--

ALTER SEQUENCE cards.campaigns_id_seq OWNED BY cards.campaigns.id;


--
-- Name: card_contributor; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.card_contributor (
    card_id integer NOT NULL,
    contributor_id integer NOT NULL
);


ALTER TABLE cards.card_contributor OWNER TO postgres;

--
-- Name: cards; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.cards (
    id integer NOT NULL,
    campaign_id integer NOT NULL,
    user_id integer NOT NULL,
    comments text,
    closed date,
    created_at timestamp without time zone DEFAULT now(),
    last_update timestamp without time zone DEFAULT now()
);


ALTER TABLE cards.cards OWNER TO postgres;

--
-- Name: cards_id_seq; Type: SEQUENCE; Schema: cards; Owner: postgres
--

CREATE SEQUENCE cards.cards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cards.cards_id_seq OWNER TO postgres;

--
-- Name: cards_id_seq; Type: SEQUENCE OWNED BY; Schema: cards; Owner: postgres
--

ALTER SEQUENCE cards.cards_id_seq OWNED BY cards.cards.id;


--
-- Name: commitments; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.commitments (
    id integer NOT NULL,
    card_id integer NOT NULL,
    currency_id character varying(3) NOT NULL,
    amount real NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);


ALTER TABLE cards.commitments OWNER TO postgres;

--
-- Name: commitments_id_seq; Type: SEQUENCE; Schema: cards; Owner: postgres
--

CREATE SEQUENCE cards.commitments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cards.commitments_id_seq OWNER TO postgres;

--
-- Name: commitments_id_seq; Type: SEQUENCE OWNED BY; Schema: cards; Owner: postgres
--

ALTER SEQUENCE cards.commitments_id_seq OWNED BY cards.commitments.id;


--
-- Name: contributors; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.contributors (
    id integer NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);


ALTER TABLE cards.contributors OWNER TO postgres;

--
-- Name: contributors_id_seq; Type: SEQUENCE; Schema: cards; Owner: postgres
--

CREATE SEQUENCE cards.contributors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cards.contributors_id_seq OWNER TO postgres;

--
-- Name: contributors_id_seq; Type: SEQUENCE OWNED BY; Schema: cards; Owner: postgres
--

ALTER SEQUENCE cards.contributors_id_seq OWNED BY cards.contributors.id;


--
-- Name: currency; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.currency (
    id character varying(3) NOT NULL,
    denomination character varying(255) NOT NULL,
    value real NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);


ALTER TABLE cards.currency OWNER TO postgres;

--
-- Name: deliveries; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.deliveries (
    id integer NOT NULL,
    card_id integer NOT NULL,
    currency_id character varying(3) NOT NULL,
    amount real NOT NULL,
    user_id integer NOT NULL,
    reception_date date NOT NULL,
    final_delivery_id integer,
    created_at timestamp without time zone DEFAULT now(),
    comments text
);


ALTER TABLE cards.deliveries OWNER TO postgres;

--
-- Name: deliveries_id_seq; Type: SEQUENCE; Schema: cards; Owner: postgres
--

CREATE SEQUENCE cards.deliveries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cards.deliveries_id_seq OWNER TO postgres;

--
-- Name: deliveries_id_seq; Type: SEQUENCE OWNED BY; Schema: cards; Owner: postgres
--

ALTER SEQUENCE cards.deliveries_id_seq OWNED BY cards.deliveries.id;


--
-- Name: final_deliveries; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.final_deliveries (
    id integer NOT NULL,
    reception_date date NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    comments text,
    confirmed boolean DEFAULT false NOT NULL
);


ALTER TABLE cards.final_deliveries OWNER TO postgres;

--
-- Name: final_deliveries_id_seq; Type: SEQUENCE; Schema: cards; Owner: postgres
--

CREATE SEQUENCE cards.final_deliveries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cards.final_deliveries_id_seq OWNER TO postgres;

--
-- Name: final_deliveries_id_seq; Type: SEQUENCE OWNED BY; Schema: cards; Owner: postgres
--

ALTER SEQUENCE cards.final_deliveries_id_seq OWNED BY cards.final_deliveries.id;


--
-- Name: users; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.users (
    id integer NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    user_type cards.user_role_type NOT NULL
);


ALTER TABLE cards.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: cards; Owner: postgres
--

CREATE SEQUENCE cards.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cards.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: cards; Owner: postgres
--

ALTER SEQUENCE cards.users_id_seq OWNED BY cards.users.id;


--
-- Name: campaigns id; Type: DEFAULT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.campaigns ALTER COLUMN id SET DEFAULT nextval('cards.campaigns_id_seq'::regclass);


--
-- Name: cards id; Type: DEFAULT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.cards ALTER COLUMN id SET DEFAULT nextval('cards.cards_id_seq'::regclass);


--
-- Name: commitments id; Type: DEFAULT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.commitments ALTER COLUMN id SET DEFAULT nextval('cards.commitments_id_seq'::regclass);


--
-- Name: contributors id; Type: DEFAULT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.contributors ALTER COLUMN id SET DEFAULT nextval('cards.contributors_id_seq'::regclass);


--
-- Name: deliveries id; Type: DEFAULT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.deliveries ALTER COLUMN id SET DEFAULT nextval('cards.deliveries_id_seq'::regclass);


--
-- Name: final_deliveries id; Type: DEFAULT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.final_deliveries ALTER COLUMN id SET DEFAULT nextval('cards.final_deliveries_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.users ALTER COLUMN id SET DEFAULT nextval('cards.users_id_seq'::regclass);


--
-- Data for Name: campaigns; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.campaigns (id, name, begin_date, end_date, created_at, goal_currency, goal_amount) FROM stdin;
4	2021	2021-02-01	2021-11-30	2020-01-16 20:10:11.389952	\N	\N
\.


--
-- Name: campaigns_id_seq; Type: SEQUENCE SET; Schema: cards; Owner: postgres
--

SELECT pg_catalog.setval('cards.campaigns_id_seq', 13, true);


--
-- Data for Name: card_contributor; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.card_contributor (card_id, contributor_id) FROM stdin;
84	439
85	251
86	109
87	15
88	48
89	49
90	98
57	419
58	286
59	420
91	406
60	56
61	395
92	250
62	67
63	302
93	122
64	59
94	336
65	414
66	397
95	626
68	263
69	4
71	127
72	486
70	417
67	217
73	34
74	402
75	345
76	142
77	293
78	252
79	33
80	255
81	173
82	323
83	167
\.


--
-- Data for Name: cards; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.cards (id, campaign_id, user_id, comments, closed, created_at, last_update) FROM stdin;
64	4	2	10000 en Junio. 8000 en Septiembre. 7000 en Diciembre.	\N	2021-04-05 01:04:46.777736	2021-04-05 01:04:46.785992
75	4	11		\N	2021-04-07 23:47:37.598153	2021-04-07 23:55:50.981907
72	4	2	No confirmó qué monto va aportar.	\N	2021-04-06 15:23:54.02296	2021-04-06 15:23:54.03052
74	4	11		\N	2021-04-07 23:46:49.883055	2021-04-07 23:57:46.821759
65	4	2		\N	2021-04-05 01:05:38.95989	2021-04-05 01:06:01.214576
70	4	2	No confirmó qué monto va aportar.	\N	2021-04-06 15:20:55.592147	2021-04-06 15:24:15.983967
66	4	2	1000 por mes al principio y después irá subiendo.	\N	2021-04-05 01:06:39.361251	2021-04-05 01:06:39.368798
83	4	11		\N	2021-04-07 23:58:41.512054	2021-04-07 23:59:04.278108
77	4	11		\N	2021-04-07 23:49:26.75912	2021-04-08 00:02:38.015224
78	4	11		\N	2021-04-07 23:50:17.624119	2021-04-08 00:04:29.450113
79	4	11		\N	2021-04-07 23:50:49.37815	2021-04-08 00:05:20.078546
62	4	2	25mil pesos y 200 dólares, no indicó fechas.	\N	2021-04-04 13:21:58.458942	2021-04-04 13:21:58.468695
80	4	11		\N	2021-04-07 23:51:29.356329	2021-04-08 00:08:46.248159
67	4	2	A la mitad llega seguro. Va intentar 25mil. No indicó fechas.	\N	2021-04-05 01:08:47.11034	2021-04-06 15:27:03.703831
81	4	11		\N	2021-04-07 23:51:58.22141	2021-04-08 00:09:32.182186
82	4	11		\N	2021-04-07 23:52:37.92858	2021-04-08 00:10:12.349317
73	4	11		\N	2021-04-07 23:41:27.076352	2021-04-08 00:22:48.946076
84	4	2		\N	2021-04-08 01:28:43.764583	2021-04-08 01:28:43.7725
68	4	2		\N	2021-04-06 13:28:02.682505	2021-04-06 13:28:21.210485
58	4	2	Mensualmente pasa aprox 800 pesos	\N	2021-04-04 13:20:27.456459	2021-04-06 13:28:46.892166
85	4	11		\N	2021-04-10 16:37:55.476529	2021-04-10 16:39:42.469861
59	4	2	25mil en 4 cuotas en marzo; junio; septiembre y diciembre	\N	2021-04-04 13:20:46.71159	2021-04-04 23:46:40.762707
60	4	2	25mil en 2 cuotas, marzo y septiembre	\N	2021-04-04 13:21:03.267079	2021-04-04 23:46:40.767645
61	4	2	25mil en 2 cuotas, marzo y septiembre	\N	2021-04-04 13:21:21.476505	2021-04-04 23:46:40.770141
57	4	2	25mil en 4 cuotas en marzo; junio; septiembre y diciembre	\N	2021-04-04 13:16:52.687892	2021-04-04 23:46:40.774758
63	4	2	25mil en 4 cuotas, en Marzo, Junio, Septiembre y Diciembre.	\N	2021-04-05 01:03:14.165389	2021-04-05 01:03:14.173298
69	4	2	1000 por mes a partir de Mayo. En Junio y Diciembre por ahí pone más.	\N	2021-04-06 14:14:11.747645	2021-04-06 15:09:41.279004
76	4	11		\N	2021-04-07 23:48:51.812653	2021-04-07 23:48:51.820305
71	4	2	No confirmó qué monto va aportar.	\N	2021-04-06 15:23:01.174915	2021-04-06 15:23:25.607715
86	4	10	$ 750 por mes. Deposita en la cuenta de Jose Miranda	\N	2021-04-10 18:45:18.689156	2021-04-10 18:58:32.673963
87	4	10	A lo largo del año	\N	2021-04-10 18:46:21.800816	2021-04-10 18:59:07.536786
88	4	10		\N	2021-04-10 18:46:48.533218	2021-04-10 18:59:36.53135
89	4	10		\N	2021-04-10 18:47:03.053047	2021-04-10 18:59:49.602166
91	4	10	$9000 marzo\r\n$8000 abril\r\n$8000 mayo	\N	2021-04-10 18:48:27.608918	2021-04-10 19:00:08.756313
90	4	10	$500 por mes	\N	2021-04-10 18:47:47.486599	2021-04-10 19:00:22.702085
93	4	10	Antes de junio 2021	\N	2021-04-10 19:45:41.592243	2021-04-10 19:45:41.599255
92	4	10	A lo largo del año	\N	2021-04-10 18:49:50.460837	2021-04-12 20:05:48.25587
94	4	10	En mayo $ 5000 y luego de junio el resto	\N	2021-04-12 20:06:54.441277	2021-04-12 20:06:54.44883
95	4	10	Fin de abril	\N	2021-04-15 20:08:22.5988	2021-04-15 20:08:22.606184
\.


--
-- Name: cards_id_seq; Type: SEQUENCE SET; Schema: cards; Owner: postgres
--

SELECT pg_catalog.setval('cards.cards_id_seq', 95, true);


--
-- Data for Name: commitments; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.commitments (id, card_id, currency_id, amount, created_at) FROM stdin;
143	63	ARS	25000	2021-04-05 01:03:14.173298
144	64	ARS	25000	2021-04-05 01:04:46.785992
146	65	ARS	14000	2021-04-05 01:06:01.214576
147	66	ARS	15000	2021-04-05 01:06:39.368798
151	68	ARS	25000	2021-04-06 13:28:02.692647
154	69	ARS	10000	2021-04-06 15:09:41.279004
158	71	ARS	1	2021-04-06 15:23:25.607715
159	72	ARS	1	2021-04-06 15:23:54.03052
160	70	ARS	1	2021-04-06 15:24:15.983967
162	67	ARS	12500	2021-04-06 15:27:03.703831
164	73	ARS	10000	2021-04-07 23:42:08.995386
165	74	ARS	1000	2021-04-07 23:46:49.890904
166	75	ARS	6000	2021-04-07 23:47:37.6059
167	76	ARS	10000	2021-04-07 23:48:51.820305
168	77	ARS	15000	2021-04-07 23:49:26.766771
169	78	ARS	10000	2021-04-07 23:50:17.631531
170	79	ARS	25000	2021-04-07 23:50:49.385759
171	80	USD	200	2021-04-07 23:51:29.363779
172	81	ARS	10000	2021-04-07 23:51:58.229238
173	82	ARS	5000	2021-04-07 23:52:37.936545
174	83	ARS	10000	2021-04-07 23:58:41.519252
175	84	ARS	25000	2021-04-08 01:28:43.7725
176	85	ARS	10000	2021-04-10 16:37:55.483878
177	86	ARS	9000	2021-04-10 18:45:18.697025
178	87	ARS	25000	2021-04-10 18:46:21.808724
179	88	ARS	25000	2021-04-10 18:46:48.540615
180	89	ARS	25000	2021-04-10 18:47:03.060491
181	90	ARS	6000	2021-04-10 18:47:47.494124
182	91	ARS	25000	2021-04-10 18:48:27.616945
183	92	ARS	25000	2021-04-10 18:49:50.468664
184	93	USD	100	2021-04-10 19:45:41.599255
185	94	ARS	25000	2021-04-12 20:06:54.44883
186	95	ARS	12000	2021-04-15 20:08:22.606184
136	57	ARS	25000	2021-04-04 13:17:50.187809
137	58	ARS	9400	2021-04-04 13:20:27.465393
138	59	ARS	25000	2021-04-04 13:20:46.719078
139	60	ARS	25000	2021-04-04 13:21:03.27518
140	61	ARS	25000	2021-04-04 13:21:21.484389
141	62	ARS	25000	2021-04-04 13:21:58.466479
142	62	USD	200	2021-04-04 13:21:58.468695
\.


--
-- Name: commitments_id_seq; Type: SEQUENCE SET; Schema: cards; Owner: postgres
--

SELECT pg_catalog.setval('cards.commitments_id_seq', 186, true);


--
-- Data for Name: contributors; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.contributors (id, first_name, last_name, email, created_at) FROM stdin;
1	Diego	Martinez	email1	2021-04-03 12:09:41.710648
2	Eva	Guzmán	email2	2021-04-03 12:09:41.710648
3	Hebe Irene	Roig	email3	2021-04-03 12:09:41.710648
4	Luis	Beltrán	email4	2021-04-03 12:09:41.710648
5	Miryam Beatriz	Gomez	email5	2021-04-03 12:09:41.710648
6	Pedro Andres	Velazquez	email6	2021-04-03 12:09:41.710648
7	Pia	Argimon	email7	2021-04-03 12:09:41.710648
8	Susana Graciela	López	email8	2021-04-03 12:09:41.710648
9	Virginia	Nasif	email9	2021-04-03 12:09:41.710648
10	Fernando	Quijano	email10	2021-04-03 12:09:41.710648
11	Pablo	Ennis	email11	2021-04-03 12:09:41.710648
12	Sergio Gustavo	Fernandez	email12	2021-04-03 12:09:41.710648
13	Corina	Ghergo	email13	2021-04-03 12:09:41.710648
14	Diego	Rodriguez	email14	2021-04-03 12:09:41.710648
15	Maria Eugenia	Vittori	email15	2021-04-03 12:09:41.710648
16	Amelia	Vargas	email16	2021-04-03 12:09:41.710648
17	Anabela	Barberena	email17	2021-04-03 12:09:41.710648
18	Beatriz	Casalis	email18	2021-04-03 12:09:41.710648
19	Graciela Carmen	Lazzatti	email19	2021-04-03 12:09:41.710648
20	Luis Horacio	Martínez	email20	2021-04-03 12:09:41.710648
21	Roberto	Kohanoff	email21	2021-04-03 12:09:41.710648
22	Roberto	Rodriguez	email22	2021-04-03 12:09:41.710648
23	Adriana Alicia	Ocampo	email23	2021-04-03 12:09:41.710648
24	Cecilia Ines	Frontini	email24	2021-04-03 12:09:41.710648
25	Claudia Alejandra	Alvarez 	email25	2021-04-03 12:09:41.710648
26	Julio	Barrios	email26	2021-04-03 12:09:41.710648
27	Luis	Tebes	email27	2021-04-03 12:09:41.710648
28	Marcelina	Gonzalez	email28	2021-04-03 12:09:41.710648
29	Norma	Coronel	email29	2021-04-03 12:09:41.710648
30	Osvaldo	Sosa	email30	2021-04-03 12:09:41.710648
31	Ruben	Ismain	email31	2021-04-03 12:09:41.710648
32	Jose	Vissani	email32	2021-04-03 12:09:41.710648
33	Blanca	Leal	email33	2021-04-03 12:09:41.710648
34	Héctor Omar	Méndez	email34	2021-04-03 12:09:41.710648
35	Fabian	Scorpino	email35	2021-04-03 12:09:41.710648
36	Juan José	Pescio	email36	2021-04-03 12:09:41.710648
37	Marcelo Fabián	Salas	email37	2021-04-03 12:09:41.710648
38	Noemi	Quiles	email38	2021-04-03 12:09:41.710648
39	Pina	Greco	email39	2021-04-03 12:09:41.710648
40	Marcelo	Repetto	email40	2021-04-03 12:09:41.710648
41	María Cristina	Güntsche	email41	2021-04-03 12:09:41.710648
42	Mayra	Serrudo	email42	2021-04-03 12:09:41.710648
43	Néstor Jorge	Avella	email43	2021-04-03 12:09:41.710648
44	Guillermo	Sullings	email44	2021-04-03 12:09:41.710648
45	Kary	Belossi	email45	2021-04-03 12:09:41.710648
46	María Fernanda	Caprara	email46	2021-04-03 12:09:41.710648
47	Roberto	Briganti	email47	2021-04-03 12:09:41.710648
48	Rodolfo Oscar	Juarez Lertora	email48	2021-04-03 12:09:41.710648
49	Thelma	Reymundo-Juarez	email49	2021-04-03 12:09:41.710648
50	Hector Felipe	Baños	email50	2021-04-03 12:09:41.710648
51	Sergio	Spano	email51	2021-04-03 12:09:41.710648
52	Celia Patricia	Centurión Espínola	email52	2021-04-03 12:09:41.710648
53	Dora Alicia	Isla	email53	2021-04-03 12:09:41.710648
54	Julio	Aquino	email54	2021-04-03 12:09:41.710648
55	Eduardo Pascual	Molinaro	email55	2021-04-03 12:09:41.710648
56	Pablo Ariel	Lopez	email56	2021-04-03 12:09:41.710648
57	Roberto Agustìn	Ruiz	email57	2021-04-03 12:09:41.710648
58	Ariel	Niro	email58	2021-04-03 12:09:41.710648
59	Carlos Damián	Muñoz	email59	2021-04-03 12:09:41.710648
60	Eduardo	Vallejo	email60	2021-04-03 12:09:41.710648
61	Graciela	Gastaldi	email61	2021-04-03 12:09:41.710648
62	Hernán	Castro	email62	2021-04-03 12:09:41.710648
63	Laura Abril	Campo Cordero	email63	2021-04-03 12:09:41.710648
64	Mariana Lucia	Pereyra	email64	2021-04-03 12:09:41.710648
65	Maximiliano	García Tellechea	email65	2021-04-03 12:09:41.710648
66	Patricia	Segovia	email66	2021-04-03 12:09:41.710648
67	Santiago	Gonzalez Rojo	email67	2021-04-03 12:09:41.710648
68	Astrid	Neiff	email68	2021-04-03 12:09:41.710648
69	Verónica Julieta	Torres	email69	2021-04-03 12:09:41.710648
70	Beatriz Marisa	Canelo	email70	2021-04-03 12:09:41.710648
71	María Laura	Chaumeri	email71	2021-04-03 12:09:41.710648
72	Alejandro	Loiácono	email72	2021-04-03 12:09:41.710648
73	Anna	Furdada	email73	2021-04-03 12:09:41.710648
74	Maria Laura	Eceiza	email74	2021-04-03 12:09:41.710648
75	Casto Roberto	Ruiz 	email75	2021-04-03 12:09:41.710648
76	Enrique	Pantuso	email76	2021-04-03 12:09:41.710648
77	Jorge	Dostal	email77	2021-04-03 12:09:41.710648
78	Dorval	Rivero	email78	2021-04-03 12:09:41.710648
79	Susana Maria	Grillo	email79	2021-04-03 12:09:41.710648
80	Carla Antonela	Pascuale	email80	2021-04-03 12:09:41.710648
81	Claudia Cristina	Balea	email81	2021-04-03 12:09:41.710648
82	Roberto	Aiello	email82	2021-04-03 12:09:41.710648
83	Susana	Olveira	email83	2021-04-03 12:09:41.710648
84	Andrea	Novotny	email84	2021-04-03 12:09:41.710648
85	Corinne	Figueroa	email85	2021-04-03 12:09:41.710648
86	Hernan	Gentili	email86	2021-04-03 12:09:41.710648
87	Jose Luis	Miranda	email87	2021-04-03 12:09:41.710648
88	Juan Armando	Caro	email88	2021-04-03 12:09:41.710648
89	Martin Ubaldo	Baez	email89	2021-04-03 12:09:41.710648
90	Micaela	Herbón Browne	email90	2021-04-03 12:09:41.710648
91	Gustavo	Viglieca	email91	2021-04-03 12:09:41.710648
92	Juana Aurora	Barragán	email92	2021-04-03 12:09:41.710648
93	Carlos Daniel	Veron	email93	2021-04-03 12:09:41.710648
94	Gerardo	Martin	email94	2021-04-03 12:09:41.710648
95	Mari	Basile	email95	2021-04-03 12:09:41.710648
96	Daniel Santiago	Domínguez	email96	2021-04-03 12:09:41.710648
97	Evelyn	Tracchia	email97	2021-04-03 12:09:41.710648
98	Gabriela Iris	Balbuena	email98	2021-04-03 12:09:41.710648
99	Iván Ezequiel	Bustos	email99	2021-04-03 12:09:41.710648
100	Ludmila	Mlikota Gatica	email100	2021-04-03 12:09:41.710648
101	Mirta Norma	Gatica	email101	2021-04-03 12:09:41.710648
102	Rocío	Balboa	email102	2021-04-03 12:09:41.710648
103	Sandra	Caprara	email103	2021-04-03 12:09:41.710648
104	Carlos Enrique	Sotomayor	email104	2021-04-03 12:09:41.710648
105	Catalina	De Darás	email105	2021-04-03 12:09:41.710648
106	Cecilia	Benitez	email106	2021-04-03 12:09:41.710648
107	Isaias	Nobel	email107	2021-04-03 12:09:41.710648
108	Raúl Eduardo	Tolentino Zamorano	email108	2021-04-03 12:09:41.710648
109	Silvia	Goyeneche	email109	2021-04-03 12:09:41.710648
110	Silvia	Galan	email110	2021-04-03 12:09:41.710648
111	Ana Isabel	Sarmiento Vidal	email111	2021-04-03 12:09:41.710648
112	Eduardo	Montes	email112	2021-04-03 12:09:41.710648
113	Jorge	Pardes	email113	2021-04-03 12:09:41.710648
114	Miguel Oscar	Cohen	email114	2021-04-03 12:09:41.710648
115	Adrian	Abraham	email115	2021-04-03 12:09:41.710648
116	Gisela	Chalabe	email116	2021-04-03 12:09:41.710648
117	Carina Mariana	Fichera	email117	2021-04-03 12:09:41.710648
118	Celia	Latuf	email118	2021-04-03 12:09:41.710648
119	Lea	Kovensky	email119	2021-04-03 12:09:41.710648
120	Nicolás	Blanco	email120	2021-04-03 12:09:41.710648
121	Soledad	Nasif	email121	2021-04-03 12:09:41.710648
122	Armengol Pedro	Espiritu	email122	2021-04-03 12:09:41.710648
123	Marcos	Pignataro	email123	2021-04-03 12:09:41.710648
124	Daniel	Sabaté	email124	2021-04-03 12:09:41.710648
125	Maria Ines Nezi	Vásquez	email125	2021-04-03 12:09:41.710648
126	Sonia	Cordoba	email126	2021-04-03 12:09:41.710648
127	Angel	De Natale	email127	2021-04-03 12:09:41.710648
128	Lucila Diana	Bercu	email128	2021-04-03 12:09:41.710648
129	Maria Andrea	Díaz Robledo	email129	2021-04-03 12:09:41.710648
130	Marta Susana	Aylán	email130	2021-04-03 12:09:41.710648
131	María Antonia	Ruiz De Eguílaz	email131	2021-04-03 12:09:41.710648
132	Ezequiel	Nicastro	email132	2021-04-03 12:09:41.710648
133	Fabián	Nicastro	email133	2021-04-03 12:09:41.710648
134	Marta Mabel	Blanco	email134	2021-04-03 12:09:41.710648
135	Mónica Silvia	Flores	email135	2021-04-03 12:09:41.710648
136	Norma Beatriz	Flores 	email136	2021-04-03 12:09:41.710648
137	Ariel	Lazovic	email137	2021-04-03 12:09:41.710648
138	Daniela	Yastrubni	email138	2021-04-03 12:09:41.710648
139	Hernán	Trinidad	email139	2021-04-03 12:09:41.710648
140	Martin	Slucki	email140	2021-04-03 12:09:41.710648
141	Pablo	Di Leo	email141	2021-04-03 12:09:41.710648
142	Carlos Eduardo	Sarmasky	email142	2021-04-03 12:09:41.710648
143	Carlos Silverio	Polla	email143	2021-04-03 12:09:41.710648
144	Marina	Olivella	email144	2021-04-03 12:09:41.710648
145	Nilda Ramona	Venencio	email145	2021-04-03 12:09:41.710648
146	Noemi	Otero	email146	2021-04-03 12:09:41.710648
147	Amelia Beatriz	Caresana	email147	2021-04-03 12:09:41.710648
148	Gabriel	Drab	email148	2021-04-03 12:09:41.710648
149	Juana Francisca	Gonzalez	email149	2021-04-03 12:09:41.710648
150	Candy	Montenegro	email150	2021-04-03 12:09:41.710648
151	Denisa Isis	Rosas	email151	2021-04-03 12:09:41.710648
152	Fernando	Haddad	email152	2021-04-03 12:09:41.710648
153	Ignacio Miguel	Arruez	email153	2021-04-03 12:09:41.710648
154	Marcela	Browne	email154	2021-04-03 12:09:41.710648
155	Maria Eugenia	Pirolo	email155	2021-04-03 12:09:41.710648
156	Nieves	Barbi	email156	2021-04-03 12:09:41.710648
157	Victor	Piccinini	email157	2021-04-03 12:09:41.710648
158	Ana	Arduino	email158	2021-04-03 12:09:41.710648
159	Daniela	Caldas	email159	2021-04-03 12:09:41.710648
160	Néstor Miguel	Tato	email160	2021-04-03 12:09:41.710648
161	Diana Miriam	Cabrera	email161	2021-04-03 12:09:41.710648
162	Jose Maria	Constantini	email162	2021-04-03 12:09:41.710648
163	Lea Verónica	Sadi	email163	2021-04-03 12:09:41.710648
164	Pablo	Fernandez	email164	2021-04-03 12:09:41.710648
165	Cristina	Suárez	email165	2021-04-03 12:09:41.710648
166	Ezio	Gangale	email166	2021-04-03 12:09:41.710648
167	Miguel Angel	Giulidoro	email167	2021-04-03 12:09:41.710648
168	Ana Maria Luisa	Tapia	email168	2021-04-03 12:09:41.710648
169	Ana	Martinez	email169	2021-04-03 12:09:41.710648
170	Charito	Algarañaz Alegre	email170	2021-04-03 12:09:41.710648
171	Cristina	Bretta	email171	2021-04-03 12:09:41.710648
172	Hernan	Gonzalez	email172	2021-04-03 12:09:41.710648
173	Jorge	Rocha	email173	2021-04-03 12:09:41.710648
174	Juan José	Castro	email174	2021-04-03 12:09:41.710648
175	Angélica	Branciforte	email175	2021-04-03 12:09:41.710648
176	Rodolfo	Bondoni	email176	2021-04-03 12:09:41.710648
177	Silvia Susana	Gonzalez	email177	2021-04-03 12:09:41.710648
178	Maria Eugenia	Montemurro	email178	2021-04-03 12:09:41.710648
179	Norma Gloria	Montemurro	email179	2021-04-03 12:09:41.710648
180	Teresita	Frenquelli	email180	2021-04-03 12:09:41.710648
181	Enzo	Momo	email181	2021-04-03 12:09:41.710648
182	Marcelo Jose	Campos	email182	2021-04-03 12:09:41.710648
183	Maria Mercedes	Luna	email183	2021-04-03 12:09:41.710648
184	Victor Ariel	Juan	email184	2021-04-03 12:09:41.710648
185	Gustavo	Tenaglia	email185	2021-04-03 12:09:41.710648
186	Luis	Ammann	email186	2021-04-03 12:09:41.710648
187	Maria Guillermina	Noro	email187	2021-04-03 12:09:41.710648
188	Nelida	Rey	email188	2021-04-03 12:09:41.710648
189	Federico	Vissani	email189	2021-04-03 12:09:41.710648
190	Florencia	Uruzuna 	email190	2021-04-03 12:09:41.710648
191	Gabriel	Yastrubni	email191	2021-04-03 12:09:41.710648
192	Javier	Hernandez	email192	2021-04-03 12:09:41.710648
193	Ana Laura	Arroyo	email193	2021-04-03 12:09:41.710648
194	Lautaro	Mazzoldi	email194	2021-04-03 12:09:41.710648
195	Alejandro	Roger	email195	2021-04-03 12:09:41.710648
196	Ricardo	Boix	email196	2021-04-03 12:09:41.710648
197	Patricia	Nagy	email197	2021-04-03 12:09:41.710648
198	Adriana Miriam	Torres	email198	2021-04-03 12:09:41.710648
199	Andrés	Pellegrini	email199	2021-04-03 12:09:41.710648
200	Eliana Judit	Gavilan	email200	2021-04-03 12:09:41.710648
201	Ernesto	Bacigalupo	email201	2021-04-03 12:09:41.710648
202	Sharon	Solar	email202	2021-04-03 12:09:41.710648
203	Alberto	Berdeal	email203	2021-04-03 12:09:41.710648
204	Carlos	Acevedo	email204	2021-04-03 12:09:41.710648
205	Haydee	Leguizamon	email205	2021-04-03 12:09:41.710648
206	Luis Ernesto	Rossetti	email206	2021-04-03 12:09:41.710648
207	Rubén Osvaldo	Aiassa	email207	2021-04-03 12:09:41.710648
208	Gustavo	Bruno	email208	2021-04-03 12:09:41.710648
209	Liliana	Ambrosio	email209	2021-04-03 12:09:41.710648
210	Graciela	Tibaudin	email210	2021-04-03 12:09:41.710648
211	Jorge	Serran	email211	2021-04-03 12:09:41.710648
212	Juan Pablo	Aló	email212	2021-04-03 12:09:41.710648
213	María Marcela	Venturino	email213	2021-04-03 12:09:41.710648
214	Pablo	Ales	email214	2021-04-03 12:09:41.710648
215	Vicente	Rivero	email215	2021-04-03 12:09:41.710648
216	Julio César	Galarza	email216	2021-04-03 12:09:41.710648
217	Mariel	Valentin	email217	2021-04-03 12:09:41.710648
218	Melanie	Clutterbuck	email218	2021-04-03 12:09:41.710648
219	Cynthia	Fisdel	email219	2021-04-03 12:09:41.710648
220	Jorge	Pompei	email220	2021-04-03 12:09:41.710648
221	Patricia	Pedernera	email221	2021-04-03 12:09:41.710648
222	Jose Mario	Nuñez	email222	2021-04-03 12:09:41.710648
223	Pachy	Tagliafico	email223	2021-04-03 12:09:41.710648
224	Renato	Bruno	email224	2021-04-03 12:09:41.710648
225	Ricardo Anibal	Lucero	email225	2021-04-03 12:09:41.710648
226	Jorge	Pozo	email226	2021-04-03 12:09:41.710648
227	Oscar	Ceballos	email227	2021-04-03 12:09:41.710648
228	Alicia	Ovejero	email228	2021-04-03 12:09:41.710648
229	Fernando	Liska	email229	2021-04-03 12:09:41.710648
230	Jorge Eduardo	Aló	email230	2021-04-03 12:09:41.710648
231	Santa Gladys	Cáceres	email231	2021-04-03 12:09:41.710648
232	Catalino	Paredes	email232	2021-04-03 12:09:41.710648
233	Ema	Formiconi	email233	2021-04-03 12:09:41.710648
234	German	Bes	email234	2021-04-03 12:09:41.710648
235	Mario Juan	Martínez	email235	2021-04-03 12:09:41.710648
236	Alejandro	Drandich	email236	2021-04-03 12:09:41.710648
237	Gustavo	Schierff	email237	2021-04-03 12:09:41.710648
238	Ana Maria	Araya	email238	2021-04-03 12:09:41.710648
239	Bruno	Milani	email239	2021-04-03 12:09:41.710648
240	Carlos Alberto	Jaimon	email240	2021-04-03 12:09:41.710648
241	Mariana	Alzaga	email241	2021-04-03 12:09:41.710648
242	Carlos	De Luca	email242	2021-04-03 12:09:41.710648
243	José	Fiducia	email243	2021-04-03 12:09:41.710648
244	Roxana	Diaz	email244	2021-04-03 12:09:41.710648
245	Santos Raul	Moreta	email245	2021-04-03 12:09:41.710648
246	Hector	Lefer	email246	2021-04-03 12:09:41.710648
247	Laura	Díaz	email247	2021-04-03 12:09:41.710648
248	Juan Pedro	Serrano	email248	2021-04-03 12:09:41.710648
249	Luisa	Boggiano	email249	2021-04-03 12:09:41.710648
250	Ana Maria	Angela Ribeiro	email250	2021-04-03 12:09:41.710648
251	Jorge Alberto	Miérez	email251	2021-04-03 12:09:41.710648
252	Maria Irene	Ceballos	email252	2021-04-03 12:09:41.710648
253	Milton Nahuel Javier	Cuellar	email253	2021-04-03 12:09:41.710648
254	Eugenio	Ruiz	email254	2021-04-03 12:09:41.710648
255	Mario	Martinez Angelini	email255	2021-04-03 12:09:41.710648
256	María Julieta	Spagnuolo	email256	2021-04-03 12:09:41.710648
257	Oscar Alberto	Yódice	email257	2021-04-03 12:09:41.710648
258	Javier	Zaldarriaga	email258	2021-04-03 12:09:41.710648
259	Maria Teresa	Ritzer	email259	2021-04-03 12:09:41.710648
260	Solange	Herrera	email260	2021-04-03 12:09:41.710648
261	Wilmer	Gonzalez	email261	2021-04-03 12:09:41.710648
262	Mayra Victoria	Spagnuolo Rozas	email262	2021-04-03 12:09:41.710648
263	Diego	Recuna	email263	2021-04-03 12:09:41.710648
264	Leonardo	Slucki	email264	2021-04-03 12:09:41.710648
265	Debora Isabel	Aprozov	email265	2021-04-03 12:09:41.710648
266	Cristina	Soto	email266	2021-04-03 12:09:41.710648
267	Norma Beatriz	Veloz	email267	2021-04-03 12:09:41.710648
268	Sandra	Pampuro	email268	2021-04-03 12:09:41.710648
269	Guido Emilio	Lamanna	email269	2021-04-03 12:09:41.710648
270	Carlos	Santos	email270	2021-04-03 12:09:41.710648
271	Marcela Inés	Bruno	email271	2021-04-03 12:09:41.710648
272	Mariela	Fernandez	email272	2021-04-03 12:09:41.710648
273	Daniel	Tagliafico	email273	2021-04-03 12:09:41.710648
274	Delia Beatriz	Fernandez	email274	2021-04-03 12:09:41.710648
275	Gabriel	Del Valle	email275	2021-04-03 12:09:41.710648
276	Americo	Huespe	email276	2021-04-03 12:09:41.710648
277	Sebastian	Spano	email277	2021-04-03 12:09:41.710648
278	Carmen	Souto	email278	2021-04-03 12:09:41.710648
279	Alberto Esteban	Garcia	email279	2021-04-03 12:09:41.710648
280	Griselda Dora	Nicolini	email280	2021-04-03 12:09:41.710648
281	Juan José	Fulgi	email281	2021-04-03 12:09:41.710648
282	Edgardo Antonio	Viera	email282	2021-04-03 12:09:41.710648
283	Mónica Alejandra	Gross Brown	email283	2021-04-03 12:09:41.710648
284	Julio Walter	Sosa	email284	2021-04-03 12:09:41.710648
285	Carmen	Rios	email285	2021-04-03 12:09:41.710648
286	Alberto Mario	Druetta	email286	2021-04-03 12:09:41.710648
287	Javier Ernesto	Martinez	email287	2021-04-03 12:09:41.710648
288	Maria Monica	Fantini	email288	2021-04-03 12:09:41.710648
289	Olga	Veloz	email289	2021-04-03 12:09:41.710648
290	Paola Silvana	Peralta	email290	2021-04-03 12:09:41.710648
291	Ana Maria	Riso	email291	2021-04-03 12:09:41.710648
292	Irene Alicia	Hoss	email292	2021-04-03 12:09:41.710648
293	Jorge Alberto	Gomez Libonatti	email293	2021-04-03 12:09:41.710648
294	Emilio José	Ducau	email294	2021-04-03 12:09:41.710648
295	Estanislao Ismael	Castillo	email295	2021-04-03 12:09:41.710648
296	Sergio Adrian	Pignataro	email296	2021-04-03 12:09:41.710648
297	Eduardo	Haddad	email297	2021-04-03 12:09:41.710648
298	Nicolás	Baranowski	email298	2021-04-03 12:09:41.710648
299	Lia	Mendez	email299	2021-04-03 12:09:41.710648
300	Norma Nilda	Melo	email300	2021-04-03 12:09:41.710648
301	Francisco	Gorsin	email301	2021-04-03 12:09:41.710648
302	Marina Beatriz	Rojas	email302	2021-04-03 12:09:41.710648
303	Jorge	D Alesio	email303	2021-04-03 12:09:41.710648
304	Susana	Carosella	email304	2021-04-03 12:09:41.710648
305	Tony	Bernardini	email305	2021-04-03 12:09:41.710648
306	Yasmín	Unfer	email306	2021-04-03 12:09:41.710648
307	Adriana Lina	Militano	email307	2021-04-03 12:09:41.710648
308	Juan Carlos	Udabe	email308	2021-04-03 12:09:41.710648
309	Fernando	Contreras	email309	2021-04-03 12:09:41.710648
310	Silvina Ines	Mirò	email310	2021-04-03 12:09:41.710648
311	Blas	Quiñones	email311	2021-04-03 12:09:41.710648
312	Gerardo Daniel	Grinstein	email312	2021-04-03 12:09:41.710648
313	Rolando	Landa	email313	2021-04-03 12:09:41.710648
314	Alejandro Ariel	Marcovecchio	email314	2021-04-03 12:09:41.710648
315	Jorge	Luczka	email315	2021-04-03 12:09:41.710648
316	Mario Carlos	Basile	email316	2021-04-03 12:09:41.710648
317	Hernán	Daulte	email317	2021-04-03 12:09:41.710648
318	Pablo	Sequeira	email318	2021-04-03 12:09:41.710648
319	Nestor Aníbal	Caprara	email319	2021-04-03 12:09:41.710648
320	Nestor	Camillieri	email320	2021-04-03 12:09:41.710648
321	Andres Omar	Perez	email321	2021-04-03 12:09:41.710648
322	Claudia Alejandra	Silva	email322	2021-04-03 12:09:41.710648
323	Salvador	Castro	email323	2021-04-03 12:09:41.710648
324	Edgardo	Perez	email324	2021-04-03 12:09:41.710648
325	Omar	Abraham	email325	2021-04-03 12:09:41.710648
326	Roberto	Rojas	email326	2021-04-03 12:09:41.710648
327	Juan	Cavicchia	email327	2021-04-03 12:09:41.710648
328	Cristina	Guzzetti	email328	2021-04-03 12:09:41.710648
329	Marina	Repetto	email329	2021-04-03 12:09:41.710648
330	Carmen	Rodriguez Abraham	email330	2021-04-03 12:09:41.710648
331	Mónica Andrea	Kernc	email331	2021-04-03 12:09:41.710648
332	Ricardo	Lejovitzky	email332	2021-04-03 12:09:41.710648
333	Sandra	Salazar	email333	2021-04-03 12:09:41.710648
334	Daniela Cecilia	Gomez Ceballos	email334	2021-04-03 12:09:41.710648
335	Luis	Manso	email335	2021-04-03 12:09:41.710648
336	Pablo	Oviedo	email336	2021-04-03 12:09:41.710648
337	Jorge	Lhande	email337	2021-04-03 12:09:41.710648
338	Alejandra	Paiz	email338	2021-04-03 12:09:41.710648
339	Maria	Dominguez	email339	2021-04-03 12:09:41.710648
340	Lila	Fernandez	email340	2021-04-03 12:09:41.710648
341	Claudia Liliana	Lavorato	email341	2021-04-03 12:09:41.710648
342	David	Parentti	email342	2021-04-03 12:09:41.710648
343	Rubén Alberto	García	email343	2021-04-03 12:09:41.710648
344	Hector Francisco	Blanco	email344	2021-04-03 12:09:41.710648
345	Ramiro Alejo	Ruiz Cabaleiro	email345	2021-04-03 12:09:41.710648
346	Gustavo	Luna	email346	2021-04-03 12:09:41.710648
347	Maria	Yofre	email347	2021-04-03 12:09:41.710648
348	Gisela	Saslavsky	email348	2021-04-03 12:09:41.710648
349	Diana	Astete	email349	2021-04-03 12:09:41.710648
350	Rita Patricia	Losada	email350	2021-04-03 12:09:41.710648
351	Alberto	Falabella	email351	2021-04-03 12:09:41.710648
352	Enrique Horacio	De Ines	email352	2021-04-03 12:09:41.710648
353	Delia Elisa	Casal	email353	2021-04-03 12:09:41.710648
354	Daniel	Rocca	email354	2021-04-03 12:09:41.710648
355	Gustavo Adolfo	Hoerth	email355	2021-04-03 12:09:41.710648
356	Ezequiel Amaru	Dominguez	email356	2021-04-03 12:09:41.710648
357	Micaela	Llevinton	email357	2021-04-03 12:09:41.710648
358	Jorge Alberto	Hedeager	email358	2021-04-03 12:09:41.710648
359	Néstor	Haupt	email359	2021-04-03 12:09:41.710648
360	Liliana	Cichy 	email360	2021-04-03 12:09:41.710648
361	Angel	Crego	email361	2021-04-03 12:09:41.710648
362	Benjamin	Aquino	email362	2021-04-03 12:09:41.710648
363	Mónica	Prostmann	email363	2021-04-03 12:09:41.710648
364	Maximiliano Andrés	Romero	email364	2021-04-03 12:09:41.710648
365	Mela	Barzini	email365	2021-04-03 12:09:41.710648
366	Daniel	Ferreyra	email366	2021-04-03 12:09:41.710648
367	Marina	Cortés	email367	2021-04-03 12:09:41.710648
368	Maria Andrea	Homps	email368	2021-04-03 12:09:41.710648
369	Victor Hugo	Coronel Vega	email369	2021-04-03 12:09:41.710648
370	Daniela	Hansen	email370	2021-04-03 12:09:41.710648
371	Diego	Garcia Rufinelli	email371	2021-04-03 12:09:41.710648
372	Damian	Arias	email372	2021-04-03 12:09:41.710648
373	Sandra	Walas	email373	2021-04-03 12:09:41.710648
374	Mariano Francisco	Sirianni	email374	2021-04-03 12:09:41.710648
375	Favio Hernán	Cuadrelli	email375	2021-04-03 12:09:41.710648
376	María Luisa	Eyras	email376	2021-04-03 12:09:41.710648
377	Pedro	Calderon	email377	2021-04-03 12:09:41.710648
378	Anibal	Villalva	email378	2021-04-03 12:09:41.710648
379	Fabiana Haydee	Martinez	email379	2021-04-03 12:09:41.710648
380	Zulma	Pereyra	email380	2021-04-03 12:09:41.710648
381	Victor Hugo	Rodriguez	email381	2021-04-03 12:09:41.710648
382	Lidia Cristina	Segura	email382	2021-04-03 12:09:41.710648
383	Jano	Arrechea	email383	2021-04-03 12:09:41.710648
384	María Graciela	Matiotti	email384	2021-04-03 12:09:41.710648
385	Susana	Malvasio	email385	2021-04-03 12:09:41.710648
386	Mariana Olga	Tagliaferro	email386	2021-04-03 12:09:41.710648
387	Lidia Irma	Fanjul	email387	2021-04-03 12:09:41.710648
388	Romina	Vera	email388	2021-04-03 12:09:41.710648
389	Rene Walter	Lema	email389	2021-04-03 12:09:41.710648
390	Juan Carlos	Fernandez	email390	2021-04-03 12:09:41.710648
391	Edgardo Alberto	Herrera	email391	2021-04-03 12:09:41.710648
392	Gloria	Garrido	email392	2021-04-03 12:09:41.710648
393	Claudia	Barbagallo	email393	2021-04-03 12:09:41.710648
394	Elsa Yolanda	Gonzalez	email394	2021-04-03 12:09:41.710648
395	Romina Paola	Vidal Zinno	email395	2021-04-03 12:09:41.710648
396	Carlos Osky	Alvarez	email396	2021-04-03 12:09:41.710648
397	Beatriz	Piotrowsky	email397	2021-04-03 12:09:41.710648
398	Jorge	Quaglia	email398	2021-04-03 12:09:41.710648
399	Erika	Arce Pérez	email399	2021-04-03 12:09:41.710648
400	Claudia	Neva	email400	2021-04-03 12:09:41.710648
401	Teresa	Lopez	email401	2021-04-03 12:09:41.710648
402	Susana Elena	Chialina	email402	2021-04-03 12:09:41.710648
403	Gustavo Osvaldo	Ocampo	email403	2021-04-03 12:09:41.710648
404	Carlos Ruben	Mozuc	email404	2021-04-03 12:09:41.710648
405	Fernando Oscar	Fabiano	email405	2021-04-03 12:09:41.710648
406	Annalisa	Pensiero	email406	2021-04-03 12:09:41.710648
407	Marta Inés	Velasco	email407	2021-04-03 12:09:41.710648
408	Lucía	Marinucci	email408	2021-04-03 12:09:41.710648
409	Viviana	Araujo	email409	2021-04-03 12:09:41.710648
410	Eugenio	Monaco	email410	2021-04-03 12:09:41.710648
411	Lucia	Pozzi	email411	2021-04-03 12:09:41.710648
412	Mónica	Mingrone	email412	2021-04-03 12:09:41.710648
413	Lorena	Cammarata	email413	2021-04-03 12:09:41.710648
414	Tamara	Ferrer	email414	2021-04-03 12:09:41.710648
415	Marina	Tannchen	email415	2021-04-03 12:09:41.710648
416	Dario	Vetere	email416	2021-04-03 12:09:41.710648
417	Gladis	Delgado	email417	2021-04-03 12:09:41.710648
418	Viviana	Cano	email418	2021-04-03 12:09:41.710648
419	Oscar	Mileti	email419	2021-04-03 12:09:41.710648
420	Lucero	Catarineu	email420	2021-04-03 12:09:41.710648
421	Ricardo	Montecinos	email421	2021-04-03 12:09:41.710648
422	Alejandro Luis	Ferrer	email422	2021-04-03 12:09:41.710648
425	Agustina	Culotta	email425	2021-04-03 12:09:41.710648
426	Carlos Omar	Luciani	email426	2021-04-03 12:09:41.710648
427	Hilda	Acosta	email427	2021-04-03 12:09:41.710648
428	Jorge	Mieres	email428	2021-04-03 12:09:41.710648
430	María Ines	Ayarragaray	email430	2021-04-03 12:09:41.710648
431	Silvia	Amodeo	email431	2021-04-03 12:09:41.710648
432	Stina	Satz	email432	2021-04-03 12:09:41.710648
433	Vincenzo	Carella	email433	2021-04-03 12:09:41.710648
434	Anne	Thiébaut	email434	2021-04-03 12:09:41.710648
435	Cuky	Ramonino	email435	2021-04-03 12:09:41.710648
438	Danilo Dario	Scanarotti	email438	2021-04-03 12:09:41.710648
439	Ricardo	Machuca	email439	2021-04-03 12:09:41.710648
440	Carlos Marcelo	Pont	email440	2021-04-03 12:09:41.710648
442	Ana Laura	Abelenda Fratini	email442	2021-04-03 12:09:41.710648
444	Carlos	Abregu	email444	2021-04-03 12:09:41.710648
445	Mauricio	Acosta Castillo	email445	2021-04-03 12:09:41.710648
446	Natalia	Agosin	email446	2021-04-03 12:09:41.710648
447	Mariano	Agustoni	email447	2021-04-03 12:09:41.710648
448	Ruben	Aiassa	email448	2021-04-03 12:09:41.710648
449	Tomas	Aiello	email449	2021-04-03 12:09:41.710648
450	Oscar	Alem Peralta	email450	2021-04-03 12:09:41.710648
451	Beatriz	Algarañaz Alegre	email451	2021-04-03 12:09:41.710648
452	Shirli	Altmark	email452	2021-04-03 12:09:41.710648
453	Carlos	Alvarez	email453	2021-04-03 12:09:41.710648
454	Natalia	Alvarez	email454	2021-04-03 12:09:41.710648
455	Nestor	Alvarez	email455	2021-04-03 12:09:41.710648
456	Raul	Alvarez	email456	2021-04-03 12:09:41.710648
457	Ricardo	Alvarez	email457	2021-04-03 12:09:41.710648
458	Alejandro	Amondarian	email458	2021-04-03 12:09:41.710648
460	Javier	Amuy	email460	2021-04-03 12:09:41.710648
461	Ana Maria	Angel Ribeiro	email461	2021-04-03 12:09:41.710648
462	Alejandro	Arce	email462	2021-04-03 12:09:41.710648
463	Carlos	Arias	email463	2021-04-03 12:09:41.710648
464	Juan Carlos	Arias	email464	2021-04-03 12:09:41.710648
465	Adriana	Ascanelli	email465	2021-04-03 12:09:41.710648
468	Lala	Ayarragaray	email468	2021-04-03 12:09:41.710648
470	Leda	Ayax	email470	2021-04-03 12:09:41.710648
471	Marta	Aylan	email471	2021-04-03 12:09:41.710648
472	Mabel	Azpiroz	email472	2021-04-03 12:09:41.710648
473	Oscar	Azpiroz	email473	2021-04-03 12:09:41.710648
474	Maria Elena	Balbontin Urtubia	email474	2021-04-03 12:09:41.710648
476	Claudia	Balea	email476	2021-04-03 12:09:41.710648
478	Giselle	Balivo	email478	2021-04-03 12:09:41.710648
479	Leandro	Bartoletti	email479	2021-04-03 12:09:41.710648
483	Micaela	Bauchet	email483	2021-04-03 12:09:41.710648
485	Mario	Belizan	email485	2021-04-03 12:09:41.710648
486	Paula	Bertoni	email486	2021-04-03 12:09:41.710648
489	Leonardo	Blanco	email489	2021-04-03 12:09:41.710648
490	Marta	Blanco	email490	2021-04-03 12:09:41.710648
491	Luciana 	Blas	email491	2021-04-03 12:09:41.710648
492	Eva	Broner	email492	2021-04-03 12:09:41.710648
493	Ethel	Bucci	email493	2021-04-03 12:09:41.710648
494	Daniel	Bustos	email494	2021-04-03 12:09:41.710648
495	Sergio	Cabral	email495	2021-04-03 12:09:41.710648
497	Gladys	Caceres	email497	2021-04-03 12:09:41.710648
499	Rodolfo	Cachela	email499	2021-04-03 12:09:41.710648
500	Sergio	Caldas	email500	2021-04-03 12:09:41.710648
502	Ammillan	Calderon	email502	2021-04-03 12:09:41.710648
503	Guillermo	Calvo	email503	2021-04-03 12:09:41.710648
504	Alexis	Camillieri	email504	2021-04-03 12:09:41.710648
505	Marcelo	Campos	email505	2021-04-03 12:09:41.710648
506	Maria Celeste	Campos	email506	2021-04-03 12:09:41.710648
507	Maria de los Angeles	Campos	email507	2021-04-03 12:09:41.710648
508	Marisa	Canelo	email508	2021-04-03 12:09:41.710648
509	Adriana	Cano	email509	2021-04-03 12:09:41.710648
510	Julia	Cantarell	email510	2021-04-03 12:09:41.710648
511	Juan Jose	Cappelli	email511	2021-04-03 12:09:41.710648
512	Ruth	Cares	email512	2021-04-03 12:09:41.710648
514	Paola	Carniglia	email514	2021-04-03 12:09:41.710648
516	Mara	Caruso	email516	2021-04-03 12:09:41.710648
518	Mauro	Casares	email518	2021-04-03 12:09:41.710648
520	Lorena	Casco	email520	2021-04-03 12:09:41.710648
521	Graciela	Cassiani	email521	2021-04-03 12:09:41.710648
522	Mariana	Catarino	email522	2021-04-03 12:09:41.710648
524	Irene	Ceballos	email524	2021-04-03 12:09:41.710648
525	Rodrigo	Cejas	email525	2021-04-03 12:09:41.710648
526	Patricia	Centurion Espinola	email526	2021-04-03 12:09:41.710648
527	Agata	Cevey	email527	2021-04-03 12:09:41.710648
528	Joaquin	Chaile	email528	2021-04-03 12:09:41.710648
529	Gustavo	Charra	email529	2021-04-03 12:09:41.710648
530	Alejandro	Chavez	email530	2021-04-03 12:09:41.710648
531	Susana	Chialina	email531	2021-04-03 12:09:41.710648
532	Jorge Omar	Chicahuala	email532	2021-04-03 12:09:41.710648
533	Mario	Chicahuala	email533	2021-04-03 12:09:41.710648
534	Carlos	Ciancio	email534	2021-04-03 12:09:41.710648
535	Yamil	Cisneros	email535	2021-04-03 12:09:41.710648
536	Oscar	Cohen	email536	2021-04-03 12:09:41.710648
537	Sandra	Condori	email537	2021-04-03 12:09:41.710648
538	Pablo 	Contartese	email538	2021-04-03 12:09:41.710648
539	Silvina	Correa	email539	2021-04-03 12:09:41.710648
541	Helena	Croce	email541	2021-04-03 12:09:41.710648
544	Nicolas	Croce	email544	2021-04-03 12:09:41.710648
545	Norma	Cruzado	email545	2021-04-03 12:09:41.710648
546	Lola	Cufre	email546	2021-04-03 12:09:41.710648
549	Carolina	Dal Monte	email549	2021-04-03 12:09:41.710648
550	Marcela	Dartiguepeyrou	email550	2021-04-03 12:09:41.710648
551	Miguel	Daulte	email551	2021-04-03 12:09:41.710648
552	Nakens	De la Barra	email552	2021-04-03 12:09:41.710648
554	Alicia	De Lafore	email554	2021-04-03 12:09:41.710648
555	Graciela	Debandi	email555	2021-04-03 12:09:41.710648
556	Daniel	Delbon	email556	2021-04-03 12:09:41.710648
557	Liliana	Delbon	email557	2021-04-03 12:09:41.710648
558	Aldo Alejandro	Delor	email558	2021-04-03 12:09:41.710648
559	Esther	Delvenne	email559	2021-04-03 12:09:41.710648
560	Gaston	Demestri	email560	2021-04-03 12:09:41.710648
561	Carmela	Denesiuk	email561	2021-04-03 12:09:41.710648
562	Adrian	Denza	email562	2021-04-03 12:09:41.710648
563	Mario	Depieri	email563	2021-04-03 12:09:41.710648
564	Natalia 	Di Ciancia	email564	2021-04-03 12:09:41.710648
565	Antonella	Di Giovanni	email565	2021-04-03 12:09:41.710648
566	Makarena 	Diaz	email566	2021-04-03 12:09:41.710648
568	Norma	Diaz	email568	2021-04-03 12:09:41.710648
569	Nilda	Diaz Gonzalez	email569	2021-04-03 12:09:41.710648
570	Gaston 	Domenichetti	email570	2021-04-03 12:09:41.710648
571	Daniel	Dominguez	email571	2021-04-03 12:09:41.710648
572	Maria Alejandra	Feldman	email572	2021-04-03 12:09:41.710648
579	Betty	Fernandez	email579	2021-04-03 12:09:41.710648
580	Enrique	Fernandez	email580	2021-04-03 12:09:41.710648
581	Jonatan	Fernandez	email581	2021-04-03 12:09:41.710648
582	Maria Celina	Fernandez Triches	email582	2021-04-03 12:09:41.710648
584	Esther	Ferreyra	email584	2021-04-03 12:09:41.710648
586	Nicolas	Filipic Masso	email586	2021-04-03 12:09:41.710648
588	Fernando	Filoni	email588	2021-04-03 12:09:41.710648
589	Norma	Flores 	email589	2021-04-03 12:09:41.710648
590	Ernesto	Floridia	email590	2021-04-03 12:09:41.710648
591	Gladys	Fragueiro	email591	2021-04-03 12:09:41.710648
592	Maria Luisa	Frigerio	email592	2021-04-03 12:09:41.710648
593	Cecilia	Frontini	email593	2021-04-03 12:09:41.710648
594	Lorena	Gaitan	email594	2021-04-03 12:09:41.710648
595	Mara	Gaitàn	email595	2021-04-03 12:09:41.710648
596	Elias	Garay	email596	2021-04-03 12:09:41.710648
598	Alberto	Garcia	email598	2021-04-03 12:09:41.710648
599	Maria Ines	Gatti	email599	2021-04-03 12:09:41.710648
602	Iris	Gimenez	email602	2021-04-03 12:09:41.710648
604	Sabrina	Gimenez	email604	2021-04-03 12:09:41.710648
605	Ernesto	Gimeno	email605	2021-04-03 12:09:41.710648
606	Miguel	Giulidoro	email606	2021-04-03 12:09:41.710648
607	Ana Maria	Gomez	email607	2021-04-03 12:09:41.710648
608	Gustavo 	Gomez	email608	2021-04-03 12:09:41.710648
609	Lucrecia	Gomez	email609	2021-04-03 12:09:41.710648
610	Dalila	Gomez Morales	email610	2021-04-03 12:09:41.710648
614	Miguel	Gomiz	email614	2021-04-03 12:09:41.710648
615	Angel	Gonzalez	email615	2021-04-03 12:09:41.710648
616	Jorge	Gonzalez	email616	2021-04-03 12:09:41.710648
618	Margarita	Gonzalez	email618	2021-04-03 12:09:41.710648
619	Mariel	Gonzalez	email619	2021-04-03 12:09:41.710648
620	Matias	Gonzalez	email620	2021-04-03 12:09:41.710648
621	Oscar	Gonzalez	email621	2021-04-03 12:09:41.710648
622	Roberto	Gonzalez	email622	2021-04-03 12:09:41.710648
623	Yessica	Gonzalez	email623	2021-04-03 12:09:41.710648
625	Ana Violeta	Greco	email625	2021-04-03 12:09:41.710648
626	Gerardo	Grisoni	email626	2021-04-03 12:09:41.710648
628	Monica	Gross Brown	email628	2021-04-03 12:09:41.710648
629	Silvia	Guardado	email629	2021-04-03 12:09:41.710648
630	Jorge	Guardia	email630	2021-04-03 12:09:41.710648
631	Leandro	Gracia Romero	email631	2021-04-03 12:09:41.710648
633	Eduardo	Ordas	email633	2021-04-03 12:09:41.710648
\.


--
-- Name: contributors_id_seq; Type: SEQUENCE SET; Schema: cards; Owner: postgres
--

SELECT pg_catalog.setval('cards.contributors_id_seq', 1, false);


--
-- Data for Name: currency; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.currency (id, denomination, value, created_at) FROM stdin;
USD	dolares	1	2020-01-16 19:40:55.827133
EUR	euros	1.11000001	2020-01-16 20:51:56.721029
ARS	sopes	0.0199999996	2020-01-16 20:52:20.994629
\.


--
-- Data for Name: deliveries; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.deliveries (id, card_id, currency_id, amount, user_id, reception_date, final_delivery_id, created_at, comments) FROM stdin;
41	59	ARS	6250	2	2021-03-08	47	2021-04-04 13:25:50.880849	Transferencia BBVA
40	60	ARS	12500	2	2021-03-03	47	2021-04-04 13:24:39.219661	Transferencia BBVA
39	61	ARS	12500	2	2021-03-03	47	2021-04-04 13:24:22.332041	Transferencia BBVA
38	58	ARS	800	2	2021-03-01	47	2021-04-04 13:23:58.83941	Transferencia BBVA
37	58	ARS	800	2	2021-02-01	47	2021-04-04 13:23:06.714017	Transferencia BBVA
36	58	ARS	600	2	2021-01-04	47	2021-04-04 13:22:36.593353	Transferencia BBVA
35	57	ARS	6250	2	2021-03-08	47	2021-04-04 13:18:57.170881	Transferencia BBVA
42	67	ARS	2000	2	2021-03-29	\N	2021-04-05 01:09:22.982472	Por Transferencia BBVA
43	68	ARS	5000	2	2021-04-05	\N	2021-04-06 13:28:21.210485	BBVA
44	58	ARS	800	2	2021-04-05	\N	2021-04-06 13:28:46.892166	BBVA
45	75	ARS	500	11	2021-01-14	\N	2021-04-07 23:54:01.586755	
46	75	ARS	500	11	2021-02-08	\N	2021-04-07 23:54:42.747352	
47	75	ARS	500	11	2021-03-03	\N	2021-04-07 23:55:17.09115	
48	75	ARS	500	11	2021-04-06	\N	2021-04-07 23:55:50.981907	
49	74	ARS	300	11	2021-01-25	\N	2021-04-07 23:56:59.67419	
50	74	ARS	300	11	12021-01-12	\N	2021-04-07 23:57:46.821759	
51	83	ARS	5000	11	2021-01-14	\N	2021-04-07 23:59:04.278108	
52	77	ARS	1300	11	2021-01-15	\N	2021-04-08 00:00:07.646068	
53	77	ARS	1300	11	2021-02-06	\N	2021-04-08 00:00:55.203308	
54	77	ARS	1500	11	2021-03-12	\N	2021-04-08 00:02:09.461931	
55	77	ARS	1500	11	2021-04-07	\N	2021-04-08 00:02:38.015224	
56	78	ARS	2000	11	2021-02-08	\N	2021-04-08 00:04:29.450113	
57	79	ARS	25000	11	2021-02-11	\N	2021-04-08 00:05:20.078546	
58	80	USD	200	11	2021-02-13	\N	2021-04-08 00:08:46.248159	Se recibió por transferencia a cuenta en dólares el 22/05/2020. Se entregó en mano a Patricia C. el 13/2/21
59	81	ARS	2000	11	2021-02-20	\N	2021-04-08 00:09:32.182186	
60	82	ARS	500	11	2021-04-07	\N	2021-04-08 00:10:12.349317	
61	73	ARS	2000	11	2021-04-08	\N	2021-04-08 00:22:48.946076	
62	85	ARS	2000	11	2021-04-12	\N	2021-04-10 16:38:27.368457	
63	86	ARS	1500	10	2021-02-04	48	2021-04-10 18:54:23.309082	enero y febrero 2021
64	87	ARS	3000	10	2021-03-05	49	2021-04-10 18:55:05.914946	
65	88	ARS	25000	10	2021-03-10	50	2021-04-10 18:55:35.243545	
66	89	ARS	25000	10	2021-03-10	51	2021-04-10 18:55:52.567628	
68	91	ARS	9000	10	2021-03-05	52	2021-04-10 18:57:13.7768	
67	90	ARS	1000	10	2021-03-08	53	2021-04-10 18:56:47.867968	enero y febrero 2021
69	92	ARS	4000	10	2021-04-10	54	2021-04-10 20:29:14.619692	Depósito cuenta Eugenia
\.


--
-- Name: deliveries_id_seq; Type: SEQUENCE SET; Schema: cards; Owner: postgres
--

SELECT pg_catalog.setval('cards.deliveries_id_seq', 69, true);


--
-- Data for Name: final_deliveries; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.final_deliveries (id, reception_date, created_at, comments, confirmed) FROM stdin;
47	2021-03-09	2021-04-04 23:46:40.76045	Entregué a Pablo Ales en mano.	t
49	2021-03-14	2021-04-10 18:59:07.53478	Entregado a Fernando C. en Parque La Reja	t
50	2021-03-14	2021-04-10 18:59:36.528886	Entregado a Fernando C. en Parque La Reja	t
51	2021-03-14	2021-04-10 18:59:49.600183	Entregado a Fernando C. en Parque La Reja	t
52	2021-03-14	2021-04-10 19:00:08.754157	Entregado a Fernando C. en Parque La Reja	t
53	2021-03-14	2021-04-10 19:00:22.700304	Entregado a Fernando C. en Parque La Reja	t
48	2021-02-07	2021-04-10 18:58:32.670746	Entregado por Jose Miranda a Pablo A.	t
54	2021-04-12	2021-04-12 20:05:48.253781	Transferí a Gustavo Ocampo	f
\.


--
-- Name: final_deliveries_id_seq; Type: SEQUENCE SET; Schema: cards; Owner: postgres
--

SELECT pg_catalog.setval('cards.final_deliveries_id_seq', 54, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.users (id, first_name, last_name, password, email, created_at, user_type) FROM stdin;
5	Candy	Montenegro	$2y$10$O48LZIE.SG3LE6YZnum.mO5LYqgsfkwx/IJ/A6G5wqOKo9yQ04vzC	candymh@gmail.com	2021-03-27 16:43:32.404935	r
6	Carina	Fichera	$2y$10$eU.v5ZYe2SACpuO7JBTD5Ou4QHiVxpdByS74/SEs8Np.xIN.nkVp6	carinafichera@gmail.com	2021-03-27 16:43:32.405478	r
2	Carlos	Munoz	$2y$10$CTEef3JrubbbpAL27mXCT.qHHlJZe7Qiax.eqcUnuSqH.9WN23pgW	carlos.damian.munoz@gmail.com	2021-03-27 16:43:32.401817	r
7	Celia	Latuf	$2y$10$flnncX/emBezO8VmYHNUcuv0nV4ul1T4a3fa4Jb6Q1VNFjyZVgXp2	celialatuf@gmail.com	2021-03-27 16:43:32.406031	r
8	Daniela	Yastrubni	$2y$10$V9i1SwHqt0rksU3ezP9TSOOqdSIuN3YJvyz4Fp1QAhvhI13yGmB8m	daniyastru@gmail.com	2021-03-27 16:43:32.40659	r
9	David	Parentti	$2y$10$SWU4FSOEa0o8Zk2mJGQfQeHpWVCOujQmJsC1Dcq79tO4HBUR/LDaG	davidparentti@gmail.com	2021-03-27 16:43:32.407115	r
4	Adriana	Ocampo	$2y$10$pgyUfQZ9eN8weoRMa/6Y8.RWkCgXHpGzvBqN/yVAaWrQTN7w1Zr46	humadri@gmail.com	2021-03-27 16:43:32.404329	r
3	Jose Luis	Miranda	$2y$10$RulPKwoNgy.g9aEw8WY2uOdZKvfIM.GUzhFQ4Fv/TPjaUzl8TV6g2	joseluismiranda@gmail.com	2021-03-27 16:43:32.403646	r
15	Nicolás	Blanco	$2y$10$tgNM2pe33CYJEGLfJwoZ6.9EtbveZskfLAZK.as8lK5uNT2cNHgHS	nilascoblan@gmail.com	2021-03-27 16:43:32.410395	r
12	Norma	Melo	$2y$10$Lme09Zd16hNyob2wbwsEOe9rA9E7DjOK2HUnGLK3mv.AoWmUcAIbG	nnildamelo@yahoo.com.ar	2021-03-27 16:43:32.4087	r
13	Patricia	Segovia	$2y$10$qSrNhXENRD.6eoUJkhK7yeLPHzKqzvppRP08DveD4wUsP8mktWxtm	patrihumanista@gmail.com	2021-03-27 16:43:32.409232	r
10	Eugenia	Vittori	$2y$10$SVxbwMISxIMmm0pWi9mVRO.JcW3YQ2ORAZ4kSZ2IuijP345dkLUVy	vittorieugenia@gmail.com	2021-03-27 16:43:32.407646	r
11	Héctor	Méndez	$2y$10$Z58izqeJJiwr6sv9CNvnCuu6UC.P8llJx9jqZsYqoP91Q588EKFR6	hectoromendez@gmail.com	2021-03-27 16:43:32.408194	r
14	Victor	Piccininni	$2y$10$e1gvKgfiX5W8G5SFncMtj.5vO.XncvvWlF2MFngEtWO/1aajKjycW	vpiccininni@gmail.com	2021-03-27 16:43:32.409826	r
1	Comisión	Parque la reja	$2y$10$cScOqpT7G/kNr3Uj.WdXKu81cQ5YvfOSTg33jGjWSrTSpsl/mQai2	info@parquelareja.org	2021-03-27 16:43:01.69685	c
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: cards; Owner: postgres
--

SELECT pg_catalog.setval('cards.users_id_seq', 15, true);


--
-- Name: campaigns campaigns_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.campaigns
    ADD CONSTRAINT campaigns_pkey PRIMARY KEY (id);


--
-- Name: cards cards_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.cards
    ADD CONSTRAINT cards_pkey PRIMARY KEY (id);


--
-- Name: commitments commitments_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.commitments
    ADD CONSTRAINT commitments_pkey PRIMARY KEY (id);


--
-- Name: contributors contributors_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.contributors
    ADD CONSTRAINT contributors_pkey PRIMARY KEY (id);


--
-- Name: currency currency_denomination_key; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.currency
    ADD CONSTRAINT currency_denomination_key UNIQUE (denomination);


--
-- Name: currency currency_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.currency
    ADD CONSTRAINT currency_pkey PRIMARY KEY (id);


--
-- Name: deliveries deliveries_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.deliveries
    ADD CONSTRAINT deliveries_pkey PRIMARY KEY (id);


--
-- Name: final_deliveries final_deliveries_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.final_deliveries
    ADD CONSTRAINT final_deliveries_pkey PRIMARY KEY (id);


--
-- Name: card_contributor unique_card_contributor; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.card_contributor
    ADD CONSTRAINT unique_card_contributor UNIQUE (card_id, contributor_id);


--
-- Name: commitments unique_currency_card; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.commitments
    ADD CONSTRAINT unique_currency_card UNIQUE (currency_id, card_id);


--
-- Name: contributors unique_email; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.contributors
    ADD CONSTRAINT unique_email UNIQUE (email);


--
-- Name: campaigns unique_name; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.campaigns
    ADD CONSTRAINT unique_name UNIQUE (name);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: commitments delete_commitment; Type: TRIGGER; Schema: cards; Owner: postgres
--

CREATE TRIGGER delete_commitment AFTER DELETE ON cards.commitments FOR EACH ROW EXECUTE PROCEDURE public.touch_card_after_relation_delete();


--
-- Name: card_contributor delete_contributor; Type: TRIGGER; Schema: cards; Owner: postgres
--

CREATE TRIGGER delete_contributor AFTER DELETE ON cards.card_contributor FOR EACH ROW EXECUTE PROCEDURE public.touch_card_after_relation_delete();


--
-- Name: deliveries delete_delivery; Type: TRIGGER; Schema: cards; Owner: postgres
--

CREATE TRIGGER delete_delivery AFTER DELETE ON cards.deliveries FOR EACH ROW EXECUTE PROCEDURE public.touch_card_after_relation_delete();


--
-- Name: commitments new_commitment; Type: TRIGGER; Schema: cards; Owner: postgres
--

CREATE TRIGGER new_commitment AFTER INSERT OR UPDATE ON cards.commitments FOR EACH ROW EXECUTE PROCEDURE public.touch_card_after_new_relation();


--
-- Name: card_contributor new_contributor; Type: TRIGGER; Schema: cards; Owner: postgres
--

CREATE TRIGGER new_contributor AFTER INSERT OR UPDATE ON cards.card_contributor FOR EACH ROW EXECUTE PROCEDURE public.touch_card_after_new_relation();


--
-- Name: deliveries new_delivery; Type: TRIGGER; Schema: cards; Owner: postgres
--

CREATE TRIGGER new_delivery AFTER INSERT OR UPDATE ON cards.deliveries FOR EACH ROW EXECUTE PROCEDURE public.touch_card_after_new_relation();


--
-- Name: deliveries on_delivery_removed; Type: TRIGGER; Schema: cards; Owner: postgres
--

CREATE TRIGGER on_delivery_removed AFTER DELETE OR UPDATE ON cards.deliveries FOR EACH ROW EXECUTE PROCEDURE public.clean_final_deliveries();


--
-- Name: campaigns campaign_currency_fk; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.campaigns
    ADD CONSTRAINT campaign_currency_fk FOREIGN KEY (goal_currency) REFERENCES cards.currency(id);


--
-- Name: cards card_campaign_fk; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.cards
    ADD CONSTRAINT card_campaign_fk FOREIGN KEY (campaign_id) REFERENCES cards.campaigns(id);


--
-- Name: card_contributor card_contributor_card; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.card_contributor
    ADD CONSTRAINT card_contributor_card FOREIGN KEY (card_id) REFERENCES cards.cards(id);


--
-- Name: card_contributor card_contributor_contributor; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.card_contributor
    ADD CONSTRAINT card_contributor_contributor FOREIGN KEY (contributor_id) REFERENCES cards.contributors(id);


--
-- Name: cards card_user; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.cards
    ADD CONSTRAINT card_user FOREIGN KEY (user_id) REFERENCES cards.users(id);


--
-- Name: commitments commitment_card; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.commitments
    ADD CONSTRAINT commitment_card FOREIGN KEY (card_id) REFERENCES cards.cards(id);


--
-- Name: commitments commitment_currency_fk; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.commitments
    ADD CONSTRAINT commitment_currency_fk FOREIGN KEY (currency_id) REFERENCES cards.currency(id);


--
-- Name: deliveries delivery_card_fk; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.deliveries
    ADD CONSTRAINT delivery_card_fk FOREIGN KEY (card_id) REFERENCES cards.cards(id);


--
-- Name: deliveries delivery_currency_fk; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.deliveries
    ADD CONSTRAINT delivery_currency_fk FOREIGN KEY (currency_id) REFERENCES cards.currency(id);


--
-- Name: deliveries delivery_final_delivery; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.deliveries
    ADD CONSTRAINT delivery_final_delivery FOREIGN KEY (final_delivery_id) REFERENCES cards.final_deliveries(id);


--
-- Name: deliveries delivery_user; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.deliveries
    ADD CONSTRAINT delivery_user FOREIGN KEY (user_id) REFERENCES cards.users(id);


--
-- Name: SCHEMA cards; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA cards TO fichas;
GRANT USAGE ON SCHEMA cards TO santiago;


--
-- Name: TABLE campaigns; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.campaigns TO fichas;
GRANT SELECT ON TABLE cards.campaigns TO santiago;


--
-- Name: SEQUENCE campaigns_id_seq; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON SEQUENCE cards.campaigns_id_seq TO fichas;
GRANT SELECT ON SEQUENCE cards.campaigns_id_seq TO santiago;


--
-- Name: TABLE card_contributor; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.card_contributor TO fichas;
GRANT SELECT ON TABLE cards.card_contributor TO santiago;


--
-- Name: TABLE cards; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.cards TO fichas;
GRANT SELECT ON TABLE cards.cards TO santiago;


--
-- Name: SEQUENCE cards_id_seq; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON SEQUENCE cards.cards_id_seq TO fichas;
GRANT SELECT ON SEQUENCE cards.cards_id_seq TO santiago;


--
-- Name: TABLE commitments; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.commitments TO fichas;
GRANT SELECT ON TABLE cards.commitments TO santiago;


--
-- Name: SEQUENCE commitments_id_seq; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON SEQUENCE cards.commitments_id_seq TO fichas;
GRANT SELECT ON SEQUENCE cards.commitments_id_seq TO santiago;


--
-- Name: TABLE contributors; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.contributors TO fichas;
GRANT SELECT ON TABLE cards.contributors TO santiago;


--
-- Name: SEQUENCE contributors_id_seq; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON SEQUENCE cards.contributors_id_seq TO fichas;
GRANT SELECT ON SEQUENCE cards.contributors_id_seq TO santiago;


--
-- Name: TABLE currency; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.currency TO fichas;
GRANT SELECT ON TABLE cards.currency TO santiago;


--
-- Name: TABLE deliveries; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.deliveries TO fichas;
GRANT SELECT ON TABLE cards.deliveries TO santiago;


--
-- Name: SEQUENCE deliveries_id_seq; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON SEQUENCE cards.deliveries_id_seq TO fichas;
GRANT SELECT ON SEQUENCE cards.deliveries_id_seq TO santiago;


--
-- Name: TABLE final_deliveries; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.final_deliveries TO fichas;
GRANT SELECT ON TABLE cards.final_deliveries TO santiago;


--
-- Name: SEQUENCE final_deliveries_id_seq; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON SEQUENCE cards.final_deliveries_id_seq TO fichas;
GRANT SELECT ON SEQUENCE cards.final_deliveries_id_seq TO santiago;


--
-- Name: TABLE users; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.users TO fichas;
GRANT SELECT ON TABLE cards.users TO santiago;


--
-- Name: SEQUENCE users_id_seq; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON SEQUENCE cards.users_id_seq TO fichas;
GRANT SELECT ON SEQUENCE cards.users_id_seq TO santiago;


--
-- PostgreSQL database dump complete
--

