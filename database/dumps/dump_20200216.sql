--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.17
-- Dumped by pg_dump version 9.6.17

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: cards; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA cards;


ALTER SCHEMA cards OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: user_role_type; Type: TYPE; Schema: cards; Owner: postgres
--

CREATE TYPE cards.user_role_type AS ENUM (
    'r',
    'c',
    'a'
);


ALTER TYPE cards.user_role_type OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: campaigns; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.campaigns (
    id integer NOT NULL,
    name character varying(255),
    begin_date date,
    end_date date,
    created_at timestamp without time zone DEFAULT now(),
    goal_currency character varying(3),
    goal_amount double precision
);


ALTER TABLE cards.campaigns OWNER TO postgres;

--
-- Name: campaigns_id_seq; Type: SEQUENCE; Schema: cards; Owner: postgres
--

CREATE SEQUENCE cards.campaigns_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cards.campaigns_id_seq OWNER TO postgres;

--
-- Name: campaigns_id_seq; Type: SEQUENCE OWNED BY; Schema: cards; Owner: postgres
--

ALTER SEQUENCE cards.campaigns_id_seq OWNED BY cards.campaigns.id;


--
-- Name: card_contributor; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.card_contributor (
    card_id integer NOT NULL,
    contributor_id integer NOT NULL
);


ALTER TABLE cards.card_contributor OWNER TO postgres;

--
-- Name: cards; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.cards (
    id integer NOT NULL,
    campaign_id integer NOT NULL,
    user_id integer NOT NULL,
    comments text,
    closed date,
    created_at timestamp without time zone DEFAULT now()
);


ALTER TABLE cards.cards OWNER TO postgres;

--
-- Name: cards_id_seq; Type: SEQUENCE; Schema: cards; Owner: postgres
--

CREATE SEQUENCE cards.cards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cards.cards_id_seq OWNER TO postgres;

--
-- Name: cards_id_seq; Type: SEQUENCE OWNED BY; Schema: cards; Owner: postgres
--

ALTER SEQUENCE cards.cards_id_seq OWNED BY cards.cards.id;


--
-- Name: commitments; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.commitments (
    id integer NOT NULL,
    card_id integer NOT NULL,
    currency_id character varying(3) NOT NULL,
    amount real NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);


ALTER TABLE cards.commitments OWNER TO postgres;

--
-- Name: commitments_id_seq; Type: SEQUENCE; Schema: cards; Owner: postgres
--

CREATE SEQUENCE cards.commitments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cards.commitments_id_seq OWNER TO postgres;

--
-- Name: commitments_id_seq; Type: SEQUENCE OWNED BY; Schema: cards; Owner: postgres
--

ALTER SEQUENCE cards.commitments_id_seq OWNED BY cards.commitments.id;


--
-- Name: contributors; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.contributors (
    id integer NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);


ALTER TABLE cards.contributors OWNER TO postgres;

--
-- Name: contributors_id_seq; Type: SEQUENCE; Schema: cards; Owner: postgres
--

CREATE SEQUENCE cards.contributors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cards.contributors_id_seq OWNER TO postgres;

--
-- Name: contributors_id_seq; Type: SEQUENCE OWNED BY; Schema: cards; Owner: postgres
--

ALTER SEQUENCE cards.contributors_id_seq OWNED BY cards.contributors.id;


--
-- Name: currency; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.currency (
    id character varying(3) NOT NULL,
    denomination character varying(255) NOT NULL,
    value real NOT NULL,
    created_at timestamp without time zone DEFAULT now()
);


ALTER TABLE cards.currency OWNER TO postgres;

--
-- Name: deliveries; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.deliveries (
    id integer NOT NULL,
    card_id integer NOT NULL,
    currency_id character varying(3) NOT NULL,
    amount real NOT NULL,
    user_id integer NOT NULL,
    reception_date date NOT NULL,
    final_delivery_id integer,
    created_at timestamp without time zone DEFAULT now(),
    comments text
);


ALTER TABLE cards.deliveries OWNER TO postgres;

--
-- Name: deliveries_id_seq; Type: SEQUENCE; Schema: cards; Owner: postgres
--

CREATE SEQUENCE cards.deliveries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cards.deliveries_id_seq OWNER TO postgres;

--
-- Name: deliveries_id_seq; Type: SEQUENCE OWNED BY; Schema: cards; Owner: postgres
--

ALTER SEQUENCE cards.deliveries_id_seq OWNED BY cards.deliveries.id;


--
-- Name: final_deliveries; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.final_deliveries (
    id integer NOT NULL,
    reception_date date NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    comments text,
    confirmed boolean DEFAULT false NOT NULL
);


ALTER TABLE cards.final_deliveries OWNER TO postgres;

--
-- Name: final_deliveries_id_seq; Type: SEQUENCE; Schema: cards; Owner: postgres
--

CREATE SEQUENCE cards.final_deliveries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cards.final_deliveries_id_seq OWNER TO postgres;

--
-- Name: final_deliveries_id_seq; Type: SEQUENCE OWNED BY; Schema: cards; Owner: postgres
--

ALTER SEQUENCE cards.final_deliveries_id_seq OWNED BY cards.final_deliveries.id;


--
-- Name: users; Type: TABLE; Schema: cards; Owner: postgres
--

CREATE TABLE cards.users (
    id integer NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    created_at timestamp without time zone DEFAULT now(),
    user_type cards.user_role_type NOT NULL
);


ALTER TABLE cards.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: cards; Owner: postgres
--

CREATE SEQUENCE cards.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cards.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: cards; Owner: postgres
--

ALTER SEQUENCE cards.users_id_seq OWNED BY cards.users.id;


--
-- Name: campaigns id; Type: DEFAULT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.campaigns ALTER COLUMN id SET DEFAULT nextval('cards.campaigns_id_seq'::regclass);


--
-- Name: cards id; Type: DEFAULT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.cards ALTER COLUMN id SET DEFAULT nextval('cards.cards_id_seq'::regclass);


--
-- Name: commitments id; Type: DEFAULT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.commitments ALTER COLUMN id SET DEFAULT nextval('cards.commitments_id_seq'::regclass);


--
-- Name: contributors id; Type: DEFAULT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.contributors ALTER COLUMN id SET DEFAULT nextval('cards.contributors_id_seq'::regclass);


--
-- Name: deliveries id; Type: DEFAULT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.deliveries ALTER COLUMN id SET DEFAULT nextval('cards.deliveries_id_seq'::regclass);


--
-- Name: final_deliveries id; Type: DEFAULT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.final_deliveries ALTER COLUMN id SET DEFAULT nextval('cards.final_deliveries_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.users ALTER COLUMN id SET DEFAULT nextval('cards.users_id_seq'::regclass);


--
-- Data for Name: campaigns; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.campaigns (id, name, begin_date, end_date, created_at, goal_currency, goal_amount) FROM stdin;
1	2020	2020-01-01	2020-12-31	2020-01-15 19:40:29.459905	\N	\N
4	2021	2021-02-01	2021-11-30	2020-01-16 20:10:11.389952	\N	\N
10	arreglo sala	2021-03-14	2021-09-14	2020-01-18 18:47:59.713929	\N	\N
\.


--
-- Name: campaigns_id_seq; Type: SEQUENCE SET; Schema: cards; Owner: postgres
--

SELECT pg_catalog.setval('cards.campaigns_id_seq', 12, true);


--
-- Data for Name: card_contributor; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.card_contributor (card_id, contributor_id) FROM stdin;
2	6
3	3
3	4
3	5
4	9
4	10
7	7
10	11
10	12
5	15
5	16
\.


--
-- Data for Name: cards; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.cards (id, campaign_id, user_id, comments, closed, created_at) FROM stdin;
2	1	2	Compromisos para octubre y diciembre	\N	2020-01-15 20:00:18.582692
5	1	4	\N	\N	2020-01-18 18:51:51.204068
7	1	7	\N	\N	2020-01-18 18:51:51.216372
8	10	4	\N	\N	2020-01-18 18:51:51.237613
9	10	1	\N	\N	2020-01-18 18:51:51.248468
10	1	1	\N	\N	2020-01-18 18:51:51.259734
11	10	7	\N	\N	2020-01-18 18:51:51.270874
4	1	1	Compromisos para mayo 2020	\N	2020-01-18 18:51:51.193026
3	1	4	\N	\N	2020-01-18 18:51:51.146338
\.


--
-- Name: cards_id_seq; Type: SEQUENCE SET; Schema: cards; Owner: postgres
--

SELECT pg_catalog.setval('cards.cards_id_seq', 11, true);


--
-- Data for Name: commitments; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.commitments (id, card_id, currency_id, amount, created_at) FROM stdin;
1	2	USD	400	2020-01-16 20:13:34.780098
5	2	ARS	21000	2020-01-16 21:29:03.388615
8	3	USD	250	2020-01-18 19:03:24.918971
9	4	ARS	20000	2020-01-18 19:03:24.93002
11	7	ARS	7000	2020-01-18 19:03:24.952069
12	8	USD	100	2020-01-18 19:03:24.963227
13	7	USD	600	2020-01-18 19:03:24.974279
14	8	ARS	41500	2020-01-18 19:03:24.985489
\.


--
-- Name: commitments_id_seq; Type: SEQUENCE SET; Schema: cards; Owner: postgres
--

SELECT pg_catalog.setval('cards.commitments_id_seq', 23, true);


--
-- Data for Name: contributors; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.contributors (id, first_name, last_name, email, created_at) FROM stdin;
1	carlos	muñoz	cmuñoz@yandex.ru	2020-01-17 21:07:34.545842
2	lucas	muñoz	lmuñoz@yandex.ru	2020-01-17 21:08:09.747137
3	joaquin	chaile	jchaile@yandex.ru	2020-01-17 21:09:16.475446
4	fernando	contreras	fcontreras@yahoo.com	2020-01-17 21:09:16.486025
5	gustavo	luna	gluna@yahoo.com	2020-01-17 21:09:16.497139
6	candy	montenegro	cmontenegro@yandex.ru	2020-01-17 21:09:16.508334
7	tamara	ferrer	tferrer@gmail.com	2020-01-18 18:31:02.907867
9	nacho	arruez	narruez@gmail.com	2020-01-18 18:38:47.669783
10	lucia	marinucci	lmarinucci@gmail.com	2020-01-18 18:38:47.680886
11	diego	dorado	ddorado@gmail.com	2020-01-18 18:38:47.692014
12	eva	guzman	eguzman@gmail.com	2020-01-18 18:38:47.703214
13	osvaldo	herrera	oherrera@gmail.com	2020-01-18 18:38:47.714575
14	alicia	leiva	aleiva@gmail.com	2020-01-18 18:38:47.725249
15	marina	rojas	mrojas@gmail.com	2020-01-18 18:38:47.736446
16	marcelo	loyola	mloyola@gmail.com	2020-01-18 18:38:47.747453
18	david	parenti	dparenti@gmail.com	2020-01-18 18:38:47.769316
19	pablo	ales	pales@gmail.com	2020-01-18 18:38:47.780452
20	patricia	segovia	psegovia@gmail.com	2020-01-18 18:38:47.791417
21	patricia	nagui	pnagui@gmail.com	2020-01-18 18:38:47.802614
22	susana	grillo	sgrillo@gmail.com	2020-01-18 18:38:47.813699
23	blanca	leal	bleal@gmail.com	2020-01-18 18:38:47.824901
8	gladys	delgado	ggomez@gmail.com	2020-01-18 18:38:47.62719
17	melanie	campos	mclutterback@gmail.com	2020-01-18 18:38:47.758429
\.


--
-- Name: contributors_id_seq; Type: SEQUENCE SET; Schema: cards; Owner: postgres
--

SELECT pg_catalog.setval('cards.contributors_id_seq', 23, true);


--
-- Data for Name: currency; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.currency (id, denomination, value, created_at) FROM stdin;
USD	dolares	1	2020-01-16 19:40:55.827133
EUR	euros	1.11000001	2020-01-16 20:51:56.721029
ARS	sopes	0.0199999996	2020-01-16 20:52:20.994629
\.


--
-- Data for Name: deliveries; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.deliveries (id, card_id, currency_id, amount, user_id, reception_date, final_delivery_id, created_at, comments) FROM stdin;
1	2	ARS	25000	2	2020-01-12	\N	2020-01-16 21:25:01.957391	\N
2	2	ARS	8000	2	2020-01-03	1	2020-01-16 21:25:21.173028	\N
4	2	ARS	8000	2	2020-01-03	1	2020-01-16 21:38:41.168315	\N
\.


--
-- Name: deliveries_id_seq; Type: SEQUENCE SET; Schema: cards; Owner: postgres
--

SELECT pg_catalog.setval('cards.deliveries_id_seq', 4, true);


--
-- Data for Name: final_deliveries; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.final_deliveries (id, reception_date, created_at, comments, confirmed) FROM stdin;
1	2020-01-14	2020-01-18 15:30:51.559451	\N	f
\.


--
-- Name: final_deliveries_id_seq; Type: SEQUENCE SET; Schema: cards; Owner: postgres
--

SELECT pg_catalog.setval('cards.final_deliveries_id_seq', 1, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: cards; Owner: postgres
--

COPY cards.users (id, first_name, last_name, password, email, created_at, user_type) FROM stdin;
5	eugenia	vittori	mm22	evittori@gmail.com	2020-01-18 18:42:15.496884	r
6	candy	montenegro	mm22	cmontonegro@gmail.com	2020-01-18 18:44:35.939165	r
7	carina	fichera	mm22	cfichera@yandex.ru	2020-01-18 18:46:01.544422	r
4	carlos	munoz	mm22	cdmunoz@gmail.com	2020-01-18 18:41:44.653928	a
2	patri	centurion	mm22	pcenturion@yandex.ru	2020-01-15 19:52:26.045966	c
1	norma	melo	$2y$10$ZWTR4rkup.91pVgXyPLlFOoOatAB3lmezw0y67wOMPqQtCCLBchAi	nmelo@yandex.ru	2020-01-15 19:49:57.924308	r
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: cards; Owner: postgres
--

SELECT pg_catalog.setval('cards.users_id_seq', 7, true);


--
-- Name: campaigns campaigns_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.campaigns
    ADD CONSTRAINT campaigns_pkey PRIMARY KEY (id);


--
-- Name: cards cards_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.cards
    ADD CONSTRAINT cards_pkey PRIMARY KEY (id);


--
-- Name: commitments commitments_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.commitments
    ADD CONSTRAINT commitments_pkey PRIMARY KEY (id);


--
-- Name: contributors contributors_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.contributors
    ADD CONSTRAINT contributors_pkey PRIMARY KEY (id);


--
-- Name: currency currency_denomination_key; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.currency
    ADD CONSTRAINT currency_denomination_key UNIQUE (denomination);


--
-- Name: currency currency_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.currency
    ADD CONSTRAINT currency_pkey PRIMARY KEY (id);


--
-- Name: deliveries deliveries_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.deliveries
    ADD CONSTRAINT deliveries_pkey PRIMARY KEY (id);


--
-- Name: final_deliveries final_deliveries_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.final_deliveries
    ADD CONSTRAINT final_deliveries_pkey PRIMARY KEY (id);


--
-- Name: card_contributor unique_card_contributor; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.card_contributor
    ADD CONSTRAINT unique_card_contributor UNIQUE (card_id, contributor_id);


--
-- Name: commitments unique_currency_card; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.commitments
    ADD CONSTRAINT unique_currency_card UNIQUE (currency_id, card_id);


--
-- Name: contributors unique_email; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.contributors
    ADD CONSTRAINT unique_email UNIQUE (email);


--
-- Name: campaigns unique_name; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.campaigns
    ADD CONSTRAINT unique_name UNIQUE (name);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: campaigns campaign_currency_fk; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.campaigns
    ADD CONSTRAINT campaign_currency_fk FOREIGN KEY (goal_currency) REFERENCES cards.currency(id);


--
-- Name: cards card_campaign_fk; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.cards
    ADD CONSTRAINT card_campaign_fk FOREIGN KEY (campaign_id) REFERENCES cards.campaigns(id);


--
-- Name: card_contributor card_contributor_card; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.card_contributor
    ADD CONSTRAINT card_contributor_card FOREIGN KEY (card_id) REFERENCES cards.cards(id);


--
-- Name: card_contributor card_contributor_contributor; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.card_contributor
    ADD CONSTRAINT card_contributor_contributor FOREIGN KEY (contributor_id) REFERENCES cards.contributors(id);


--
-- Name: cards card_user; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.cards
    ADD CONSTRAINT card_user FOREIGN KEY (user_id) REFERENCES cards.users(id);


--
-- Name: commitments commitment_card; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.commitments
    ADD CONSTRAINT commitment_card FOREIGN KEY (card_id) REFERENCES cards.cards(id);


--
-- Name: commitments commitment_currency_fk; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.commitments
    ADD CONSTRAINT commitment_currency_fk FOREIGN KEY (currency_id) REFERENCES cards.currency(id);


--
-- Name: deliveries delivery_card_fk; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.deliveries
    ADD CONSTRAINT delivery_card_fk FOREIGN KEY (card_id) REFERENCES cards.cards(id);


--
-- Name: deliveries delivery_currency_fk; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.deliveries
    ADD CONSTRAINT delivery_currency_fk FOREIGN KEY (currency_id) REFERENCES cards.currency(id);


--
-- Name: deliveries delivery_final_delivery; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.deliveries
    ADD CONSTRAINT delivery_final_delivery FOREIGN KEY (final_delivery_id) REFERENCES cards.final_deliveries(id);


--
-- Name: deliveries delivery_user; Type: FK CONSTRAINT; Schema: cards; Owner: postgres
--

ALTER TABLE ONLY cards.deliveries
    ADD CONSTRAINT delivery_user FOREIGN KEY (user_id) REFERENCES cards.users(id);


--
-- Name: SCHEMA cards; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA cards TO fichas;


--
-- Name: TABLE campaigns; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.campaigns TO fichas;


--
-- Name: SEQUENCE campaigns_id_seq; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON SEQUENCE cards.campaigns_id_seq TO fichas;


--
-- Name: TABLE card_contributor; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.card_contributor TO fichas;


--
-- Name: TABLE cards; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.cards TO fichas;


--
-- Name: SEQUENCE cards_id_seq; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON SEQUENCE cards.cards_id_seq TO fichas;


--
-- Name: TABLE commitments; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.commitments TO fichas;


--
-- Name: SEQUENCE commitments_id_seq; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON SEQUENCE cards.commitments_id_seq TO fichas;


--
-- Name: TABLE contributors; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.contributors TO fichas;


--
-- Name: SEQUENCE contributors_id_seq; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON SEQUENCE cards.contributors_id_seq TO fichas;


--
-- Name: TABLE currency; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.currency TO fichas;


--
-- Name: TABLE deliveries; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.deliveries TO fichas;


--
-- Name: SEQUENCE deliveries_id_seq; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON SEQUENCE cards.deliveries_id_seq TO fichas;


--
-- Name: TABLE final_deliveries; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.final_deliveries TO fichas;


--
-- Name: SEQUENCE final_deliveries_id_seq; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON SEQUENCE cards.final_deliveries_id_seq TO fichas;


--
-- Name: TABLE users; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON TABLE cards.users TO fichas;


--
-- Name: SEQUENCE users_id_seq; Type: ACL; Schema: cards; Owner: postgres
--

GRANT ALL ON SEQUENCE cards.users_id_seq TO fichas;


--
-- PostgreSQL database dump complete
--

