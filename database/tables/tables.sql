CREATE TABLE cards.currency(
    id              varchar(3)      PRIMARY KEY,
    denomination    varchar(255)    NOT NULL UNIQUE,
    value           real            NOT NULL
);

CREATE TABLE cards.contributors (
    id          serial          PRIMARY KEY,
    first_name  varchar(255)    NOT NULL,
    last_name   varchar(255)    NOT NULL,
    email       varchar(255)    NOT NULL,
    created_at  timestamp       DEFAULT current_timestamp,
    is_master   boolean         DEFAULT true,
);

CREATE TYPE cards.user_role_type as enum ('r','c','a');
CREATE TABLE cards.users (
    id          serial          PRIMARY KEY,
    first_name  varchar(255)    NOT NULL,
    last_name   varchar(255)    NOT NULL,
    password    varchar(255)    NOT NULL,
    email       varchar(255)    NOT NULL,
    user_type   cards.user_role_type  NOT NULL,     
    created_at  timestamp       DEFAULT current_timestamp
);

CREATE TABLE cards.campaigns (
    id  serial  PRIMARY KEY,
    name    varchar(255),
    begin_date      date,
    end_date        date,
    goal_currency   varchar(3),
    goal_amount     float,
    created_at  timestamp   default current_timestamp
);

CREATE TABLE cards.commitments (
    id          serial      PRIMARY KEY,
    card_id     integer     NOT NULL,
    currency_id varchar(3)  NOT NULL,
    amount      real        NOT NULL,
    created_at  timestamp   default current_timestamp
);

CREATE TABLE cards.deliveries (
    id              serial      PRIMARY KEY,
    card_id         integer     NOT NULL,
    currency_id     varchar(3)  NOT NULL,
    amount          real        NOT NULL,
    user_id         integer     NOT NULL,
    reception_date  date        NOT NULL,
    comments        text,
    final_delivery_id   integer,
    created_at  timestamp   DEFAULT current_timestamp
);

CREATE TABLE cards.final_deliveries (
    id              serial      PRIMARY KEY,
    reception_date  date        NOT NULL,
    comments        text,
    confirmed       boolean     NOT NULL    default false,
    created_at      timestamp   DEFAULT current_timestamp
    last_update     timestamp   DEFAULT current_timestamp
);

CREATE TABLE cards.cards (
    id          serial      PRIMARY KEY,
    campaign_id integer     NOT NULL,
    user_id     integer     NOT NULL,
    comments    text,
    closed      date,
    created_at  timestamp   DEFAULT current_timestamp,
    last_update  timestamp   DEFAULT current_timestamp
);

CREATE TABLE cards.card_contributor (
    card_id         integer NOT NULL,
    contributor_id  integer NOT NULL,
    constraint unique_card_contributor UNIQUE (card_id,contributor_id)
);

ALTER TABLE cards.contributors
ADD CONSTRAINT unique_email unique (email);

ALTER TABLE cards.users
ADD CONSTRAINT unique_email_user_type unique(email,user_type);


ALTER TABLE cards.campaigns
ADD CONSTRAINT unique_name_deletion unique(name);


GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA cards TO fichas;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA cards TO fichas;
