INSERT INTO cards.contributors (first_name, last_name, email) VALUES 
('Ana Laura','Abelenda Fratini','anahumanista@gmail.com'),
('Carlos','Abregu','carlosabregu2002@yahoo.com.ar'),
('Mauricio','Acosta Castillo','mauricie@hotmail.com'),
('Natalia','Agosin','agosinnatalia@hotmail.com'),
('Mariano','Agustoni','marianoagustoni@gmail.com'),
('Ruben','Aiassa','raiassa@yahoo.com.ar'),
('Tomas','Aiello','aiello.tomas@gmail.com'),
('Oscar','Alem Peralta','oscar.alem@gmail.com'),
('Beatriz','Algarañaz Alegre','charitoo@live.com.ar'),
('Shirli','Altmark','email1@mail.com'),
('Carlos','Alvarez','carlos.epsilon50@gmail.com'),
('Natalia','Alvarez','natalia.humanista@gmail.com'),
('Nestor','Alvarez','gallegoalvarez33@hotmail.com'),
('Raul','Alvarez','golan146@fibertel.com.ar'),
('Ricardo','Alvarez','email2@mail.com'),
('Claudia','Alvarez ','aclaudiaalejandra@gmail.com'),
('Alejandro','Amondarian','email3@mail.com'),
('Javier','Amuy','javier1410@gmail.com'),
('Ana Maria','Angel Ribeiro','amar.anita@hotmail.com'),
('Alejandro','Arce','email4@mail.com'),
('Carlos','Arias','email1@mail.com'),
('Juan Carlos','Arias','carlos81052@yahoo.com.ar'),
('Alejandro','Arrechea','janoarrechea@gmail.com'),
('Ignacio','Arruez','nachoarruez@gmail.com'),
('Adriana','Ascanelli','a.ascanelli@gmail.com'),
('Nestor','Avella','njavella@yahoo.com.ar'),
('Lala','Ayarragaray','layarragaray@hotmail.com'),
('Leda','Ayax','leda.mensajera@gmail.com'),
('Marta','Aylan','aylan.lacomunidad@gmail.com'),
('Mabel','Azpiroz','mabyaz@yahoo.com.ar'),
('Oscar','Azpiroz','trenqueos@yahoo.com.ar'),
('Martin','Baez','martinubaldo22@yahoo.com.ar'),
('Maria Elena','Balbontin Urtubia','melena.balbontin@gmail.com'),
('Gabriela','Balbuena','gabrielairisb@hotmail.com'),
('Claudia','Balea','ccbalea@gmail.com'),
('Giselle','Balivo','gisellebalivo@gmail.com'),
('Hector','Baños','dragonplaneador@yahoo.com.ar'),
('Anabella','Barberena','anabela.barberena@gmail.com'),
('Juana','Barragan','jubarragan@yahoo.com.ar'),
('Leandro','Bartoletti','leandro_bartoletti@yahoo.com.ar'),
('Mario','Basile','mario_basile@yahoo.com'),
('Micaela','Bauchet','email1@mail.com'),
('Mario','Belizan','mariobelizan@yahoo.com.ar'),
('Kari','Belossi','karybelossi@gmail.com'),
('Lucila','Bercu','lucilab11@gmail.com'),
('Paula','Bertoni','paulabertoni@yahoo.com'),
('Leonardo','Blanco','flordecontacto@gmail.com'),
('Marta','Blanco','marbelnic@yahoo.com.ar'),
('Luciana ','Blas','email1@mail.com'),
('Eva','Broner','eva.broner@gmail.com'),
('Ethel','Bucci','email1@mail.com'),
('Daniel','Bustos','danielbrm1@gmail.com'),
('Ivan','Bustos','ivanezequielbustos@gmail.com'),
('Sergio','Cabral','sergiocabral69@yahoo.com.ar'),
('Diana','Cabrera','dianamiriamc@hotmail.com'),
('Gladys','Caceres','gladyscaceres275@gmail.com'),
('Rodolfo','Cachela','cachela@gmail.com'),
('Daniela ','Caldas','danido692010@gmail.com'),
('Sergio','Caldas','sergiowaltercaldas@hotmail.com'),
('Ammillan','Calderon','sttyle.a@hotmail.com'),
('Guillermo','Calvo','lordfebre@gmail.com'),
('Alexis','Camillieri','camillieri.alexis@gmail.com'),
('Marcelo','Campos','marcelo1904@hotmail.com'),
('Maria Celeste','Campos','maricele23@gmail.com'),
('Maria de los Angeles','Campos','angelescampos2009@gmail.com'),
('Marisa','Canelo','marisacanelo@yahoo.com.ar'),
('Adriana','Cano','email2@mail.com'),
('Julia','Cantarell','patricia_cantarell@yahoo.com.ar'),
('Juan Jose','Cappelli','jjcappelli@gmail.com'),
('Nestor','Caprara','nestorcaprara@hotmail.com'),
('Ruth','Cares','cares_r@hotmail.com'),
('Amelia','Caresana','acaresana@gmail.com'),
('Paola','Carniglia','email1@mail.com'),
('Sonia','Carosella','soniacarosella@yahoo.com.ar'),
('Mara','Caruso','maisabe@hotmail.com'),
('Delia','Casal','diliaelisacasal@hotmail.com'),
('Mauro','Casares','mauro.casares@gmail.com'),
('Lorena','Casco','lorenacasco79@gmail.com'),
('Graciela','Cassiani','gcassiani@yahoo.com.ar'),
('Estanislao','Castillo','stanisks@gmail.com'),
('Mariana','Catarino','79.mariana@gmail.com'),
('Irene','Ceballos','mariaireneceballos256@gmail.com'),
('Rodrigo','Cejas','email1@mail.com'),
('Patricia','Centurion Espinola','patric@redhumanista.org'),
('Agata','Cevey','agatacevey@gmail.com'),
('Joaquin','Chaile','sidious_20@yahoo.com.ar'),
('Gustavo','Charra','charragustavo@hotmail.com'),
('Alejandro','Chavez','chavezalejandro@yahoo.com.ar'),
('Susana','Chialina','susanachialina4@gmail.com'),
('Jorge Omar','Chicahuala','chicahuala@gmail.com'),
('Mario','Chicahuala','mach_tau@yahoo.com.ar'),
('Carlos','Ciancio','carlosciancio@gmail.com'),
('Yamil','Cisneros','email1@mail.com'),
('Oscar','Cohen','oscar_cohen@hotmail.com'),
('Sandra','Condori','pausini_sandy@yahoo.com.ar'),
('Pablo ','Contartese','pablocontartese@hotmail.com'),
('Victor','Coronel Vega','argotbienesmuebles@yahoo.com.ar'),
('Silvina','Correa','sylvinacorrea@gmail.com');
