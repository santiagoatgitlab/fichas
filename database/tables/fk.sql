-- campaigns
ALTER TABLE cards.campaigns
ADD CONSTRAINT campaign_currency_fk FOREIGN KEY (goal_currency) REFERENCES cards.currency (id);

-- commitments
ALTER TABLE cards.commitments
ADD CONSTRAINT commitment_currency_fk FOREIGN KEY (currency_id) REFERENCES cards.currency (id);
ALTER TABLE cards.commitments
ADD CONSTRAINT commitment_card FOREIGN KEY (card_id) REFERENCES cards.cards (id);

-- deliveries
ALTER TABLE cards.deliveries
ADD CONSTRAINT delivery_card_fk FOREIGN KEY (card_id) REFERENCES cards.cards (id);
ALTER TABLE cards.deliveries
ADD CONSTRAINT delivery_currency_fk FOREIGN KEY (currency_id) REFERENCES cards.currency (id);
ALTER TABLE cards.deliveries
ADD CONSTRAINT delivery_user FOREIGN KEY (user_id) REFERENCES cards.users (id);
ALTER TABLE cards.deliveries
ADD CONSTRAINT delivery_final_delivery FOREIGN KEY (final_delivery_id) REFERENCES cards.final_deliveries (id);
    
-- final_deliveries
ALTER TABLE cards.final_deliveries
ADD CONSTRAINT final_delivery_user_fk FOREIGN KEY (user_id) REFERENCES cards.users (id);

-- cards
ALTER TABLE cards.cards
ADD CONSTRAINT card_campaign_fk FOREIGN KEY (campaign_id) REFERENCES cards.campaigns (id);
ALTER TABLE cards.cards
ADD CONSTRAINT card_user FOREIGN KEY (user_id) REFERENCES cards.users (id);

-- card_contributor
ALTER TABLE cards.card_contributor
ADD CONSTRAINT card_contributor_card FOREIGN KEY (card_id) REFERENCES cards.cards (id);
ALTER TABLE cards.card_contributor
ADD CONSTRAINT card_contributor_contributor FOREIGN KEY (contributor_id) REFERENCES cards.contributors (id);

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA cards TO fichas;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA cards TO fichas;
