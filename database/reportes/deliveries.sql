select
    d.currency_id,
    d.amount,
    d.final_delivery_id,
    fd.confirmed
from
    cards.deliveries d
left join 
    cards.final_deliveries fd on fd.id = d.final_delivery_id
where
    d.card_id = $1
