import { Client as DbClient } from "https://deno.land/x/postgres@v0.5.0/client.ts";

const dbClient = new DbClient({
    hostname : "localhost",
    port     : 5432,
    database : "fichas",
    user     : "fichas",
    password : "mm61Tgy4fh"
})

await dbClient.connect()

const campaignId = 14;

const fichasQuery = await Deno.readTextFile('./fichas.sql')

const result = await dbClient.query({
    text : fichasQuery,
    args : [ campaignId ]
})

const contributorsQuery = await Deno.readTextFile('./contributors.sql')
const commitmentsQuery = await Deno.readTextFile('./commitments.sql')
const deliveriesQuery = await Deno.readTextFile('./deliveries.sql')

const cards = []
for ( let i = 0; i < result.rows.length; i++){
    const [cardId, user, comment] = result.rows[i]

    const contributors = await dbClient.query({
        text : contributorsQuery,
        args : [ cardId ]
    })

    const commitments = await dbClient.query({
        text : commitmentsQuery,
        args : [ cardId ]
    })

    const commitmentsARS = filterByCurrency('ARS', commitments.rows)
    const commitmentsUSD = filterByCurrency('USD', commitments.rows)
    const commitmentsEUR = filterByCurrency('EUR', commitments.rows)

    const deliveries = await dbClient.query({
        text : deliveriesQuery,
        args : [ cardId ] 
    })

    const received = deliveries.rows.filter( ([_,__,delivered]) => {
        return delivered == null
    })
    const delivered = deliveries.rows.filter( ([_,__,___,confirmed]) => {
        return confirmed === false
    })
    const confirmed = deliveries.rows.filter( ([_,__,___,confirmed]) => {
        return confirmed
    })

    const receivedARS = getTotalAmount( 'ARS', received )
    const receivedUSD = getTotalAmount( 'USD', received )
    const receivedEUR = getTotalAmount( 'EUR', received )
    const deliveredARS = getTotalAmount( 'ARS', delivered )
    const deliveredUSD = getTotalAmount( 'USD', delivered )
    const deliveredEUR = getTotalAmount( 'EUR', delivered )
    const confirmedARS = getTotalAmount( 'ARS', confirmed )
    const confirmedUSD = getTotalAmount( 'USD', confirmed )
    const confirmedEUR = getTotalAmount( 'EUR', confirmed )

    cards.push([
        contributors.rows
                    .map( ([name]) => name )
                    .join(' / '),
        user,
        commitmentsARS,
        commitmentsUSD,
        commitmentsEUR,
        receivedARS,
        receivedUSD,
        receivedEUR,
        deliveredARS,
        deliveredUSD,
        deliveredEUR,
        confirmedARS,
        confirmedUSD,
        confirmedEUR,
        receivedARS + deliveredARS + confirmedARS,
        receivedUSD + deliveredUSD + confirmedUSD,
        receivedEUR + deliveredEUR + confirmedEUR,
        '"' + comment.trim().replaceAll('\n','. ').replaceAll('\r','. ') + '"'
    ])

}

function filterByCurrency(targetCurrencyId, amounts){
    return amounts.filter( ([currencyId, amount]) => {
        return currencyId == targetCurrencyId
    })
    .map( ([_,amount]) => amount)
    [0]
}

function getTotalAmount(targetCurrencyId, amounts){
    return amounts.filter( ([currencyId]) => {
        return currencyId == targetCurrencyId
    })
    .reduce( (totalAmount,[_,amount]) => totalAmount + amount, 0)
}


const fields = [[
    "Aportante/s de la ficha",
    "Receptor",
    "Compromiso pesos",
    "Compromiso dólares",
    "Compromiso euros",
    "Recibido pesos",
    "Recibido dólares",
    "Recibido euros",
    "Entregado pesos",
    "Entregado dólares",
    "Entregado euros",
    "Confirmado pesos",
    "Confirmado dólares",
    "Confirmado euros",
    "Total aportado ARS",
    "Total aportado USD",
    "Total aportado EUR",
    "Comentarios"
].join(',')]


const values = cards.map( row => row.join(',') )

const csv = fields.concat(values)

Deno.writeTextFile('./recaudadores2022.csv', csv.join('\n'))
