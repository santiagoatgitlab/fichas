import { Client as DbClient } from "https://deno.land/x/postgres@v0.5.0/client.ts";

const dbClient = new DbClient({
    hostname : "localhost",
    port     : 5432,
    database : "fichas",
    user     : "fichas",
    password : "mm61Tgy4fh"
})

await dbClient.connect()

const query = await Deno.readTextFile('./comision.sql')

const result = await dbClient.query({
    text : query,
    args  : []
})

const fields = [[
    "Fecha de entrega",
    "Receptor",
    "No confirmado en pesos",
    "No confirmado en dólares",
    "No confirmado en euros",
    "Confirmado en pesos",
    "Confirmado en dólares",
    "Confirmado en euros",
    "Comentarios"
].join(',')]

const values = result.rows.map( ([date,fname,lname,currency,amount,comment,confirmed]) => {
    const result = [
        date.toISOString().split('T')[0].split('-').reverse().join('/'),
        fname + ' ' + lname,
        (!confirmed && currency == 'ARS') ? amount : "",
        (!confirmed && currency == 'USD') ? amount : "",
        (!confirmed && currency == 'EUR') ? amount : "",
        (confirmed  && currency == 'ARS') ? amount : "",
        (confirmed  && currency == 'USD') ? amount : "",
        (confirmed  && currency == 'EUR') ? amount : "",
        comment.trim().replaceAll('\n','. ')
    ]
    return result.join(',')
})

const csv = fields.concat(values)

Deno.writeTextFile('./comision.csv', csv.join('\n'))
