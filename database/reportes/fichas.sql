select c.id,
       concat(u.first_name, ' ', u.last_name),
       comments
from cards.cards c
join cards.users u on c.user_id = u.id
where campaign_id = $1
order by id;
