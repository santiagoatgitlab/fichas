select 
    concat (first_name, ' ', last_name)
from
    cards.contributors c
inner join
    cards.card_contributor cc on c.id = cc.contributor_id
where
    cc.card_id = $1
