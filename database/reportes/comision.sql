select
    fd.reception_date,
    min(first_name),
    min(last_name),
    min(d.currency_id),
    sum(d.amount),
    min(fd.comments),
    fd.confirmed
from 
    cards.deliveries d
    join cards.final_deliveries fd
        on d.final_delivery_id = fd.id
    join cards.cards c
        on d.card_id = c.id
    join cards.users u
        on c.user_id = u.id
where c.campaign_id = 4
group by fd.id
order by fd.reception_date;
