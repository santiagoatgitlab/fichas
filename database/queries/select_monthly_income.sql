select date_part('month', d.reception_date) as month,
       date_part('year', d.reception_date) as year,
       sum(amount)
from cards.deliveries d
inner join cards.cards crd on d.card_id = crd.id
inner join cards.campaigns cmp on crd.campaign_id = cmp.id
where cmp.id = 16
    and currency_id = 'USD'
group by
    date_part('month', d.reception_date),
    date_part('year', d.reception_date);
