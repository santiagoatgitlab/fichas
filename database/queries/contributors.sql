select ct.* from cards.contributors ct
join cards.card_contributor cc on cc.contributor_id = ct.id
join cards.cards c on cc.card_id = c.id
join cards.commitments cmt on cmt.card_id = c.id
where ct.id in (
    select ct.id from cards.contributors ct
        join cards.card_contributor cc on cc.contributor_id = ct.id
        join cards.cards c on cc.card_id = c.id
        join cards.deliveries d on d.card_id = c.id
    where c.campaign_id = 4
    group by ct.id
) and ct.id not in (
    select ct.id from cards.contributors ct
        join cards.card_contributor cc on cc.contributor_id = ct.id
        join cards.cards c on cc.card_id = c.id
        join cards.deliveries d on d.card_id = c.id
    where c.campaign_id = 14
    group by ct.id
) and c.campaign_id = 14;
