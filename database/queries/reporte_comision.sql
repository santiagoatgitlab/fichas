select
    d.reception_date as fecha,
    concat (u.first_name,'_',u.last_name) as receptor,
    concat (d.currency_id,'_',d.amount) as monto,
    case
        when fd.confirmed = 't' then 'confirmado'
        else 'a confirmar'
    end as estado
from cards.deliveries d
left join cards.final_deliveries fd on d.final_delivery_id = fd.id
left join cards.users u on d.user_id = u.id
order by d.id;
