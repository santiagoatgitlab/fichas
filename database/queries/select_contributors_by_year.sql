select
    c.id,
    c.first_name,
    c.last_name,
    count(d.id) as "cantidad aportes"
from cards.contributors c
left join cards.card_contributor cc on c.id = cc.contributor_id
left join cards.cards ca on cc.card_id = ca.id
left join cards.commitments com on ca.id = com.card_id
left join cards.deliveries d on ca.id = d.card_id
where ca.campaign_id = 14
group by c.id
order by count(d.id) desc;
