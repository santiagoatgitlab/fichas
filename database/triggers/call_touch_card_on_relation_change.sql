CREATE TRIGGER call_touch_card_on_commitment_change
AFTER INSERT OR UPDATE OR DELETE
ON cards.commitments
EXECUTE PROCEDURE cards.update_card_id(NEW.card_id);
