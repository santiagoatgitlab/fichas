CREATE OR REPLACE FUNCTION clean_final_deliveries()
RETURNS trigger AS $$
BEGIN 
    DELETE FROM cards.final_deliveries
    WHERE id NOT IN (
        SELECT distinct(fd.id) FROM cards.final_deliveries fd
        INNER JOIN cards.deliveries d ON d.final_delivery_id = fd.id
    );
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER on_delivery_removed 
AFTER DELETE OR UPDATE on cards.deliveries
    FOR EACH ROW EXECUTE PROCEDURE clean_final_deliveries();
