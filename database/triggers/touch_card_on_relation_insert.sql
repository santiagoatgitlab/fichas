CREATE OR REPLACE FUNCTION touch_card_after_new_relation()
RETURNS trigger AS $$
BEGIN 
    UPDATE cards.cards
    SET last_update = CURRENT_TIMESTAMP
    WHERE id = NEW.card_id;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER new_commitment
AFTER INSERT OR UPDATE on cards.commitments
    FOR EACH ROW EXECUTE PROCEDURE touch_card_after_new_relation();

CREATE TRIGGER new_delivery 
AFTER INSERT OR UPDATE on cards.deliveries
    FOR EACH ROW EXECUTE PROCEDURE touch_card_after_new_relation();

CREATE TRIGGER new_contributor 
AFTER INSERT OR UPDATE on cards.card_contributor
    FOR EACH ROW EXECUTE PROCEDURE touch_card_after_new_relation();
