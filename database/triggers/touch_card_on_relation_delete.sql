CREATE OR REPLACE FUNCTION touch_card_after_relation_delete()
RETURNS trigger AS $$
BEGIN 
    UPDATE cards.cards
    SET last_update = CURRENT_TIMESTAMP
    WHERE id = OLD.card_id;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER delete_commitment
AFTER DELETE on cards.commitments
    FOR EACH ROW EXECUTE PROCEDURE touch_card_after_relation_delete();

CREATE TRIGGER delete_delivery 
AFTER DELETE on cards.deliveries
    FOR EACH ROW EXECUTE PROCEDURE touch_card_after_relation_delete();

CREATE TRIGGER delete_contributor 
AFTER DELETE on cards.card_contributor
    FOR EACH ROW EXECUTE PROCEDURE touch_card_after_relation_delete();
