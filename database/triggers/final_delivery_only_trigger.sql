CREATE TRIGGER update_final_delivery
AFTER UPDATE of confirmed on cards.final_deliveries
    FOR EACH ROW EXECUTE PROCEDURE touch_final_delivery_on_change();
