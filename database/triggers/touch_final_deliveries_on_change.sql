CREATE OR REPLACE FUNCTION touch_final_delivery_on_change()
RETURNS trigger AS $$
BEGIN 
    UPDATE cards.final_deliveries
    SET last_update = CURRENT_TIMESTAMP
    WHERE id = NEW.id;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER update_final_delivery
AFTER UPDATE of confirmed on cards.final_deliveries
    FOR EACH ROW EXECUTE PROCEDURE touch_final_delivery_on_change();
