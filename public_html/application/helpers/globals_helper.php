<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Application specific global variables
class Globals
{
    public static function get_full_menu()
    {
        return [    
            'create_card'      => true,
            'admin_deliveries' => true,
            'card'             => false,
            'campaign'         => true,
            'another_campaign' => true,
            'report'           => true,
            'new_campaign'     => false,
            'new_contributor'  => false
        ];
    }

    public static function get_basic_menu()
    {
        return [    
            'create_card'      => false,
            'admin_deliveries' => false,
            'another_campaign' => false,
            'report'           => false,
            'new_campaign'     => false,
            'new_contributor'  => false
        ];
    }

}
