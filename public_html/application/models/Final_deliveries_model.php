<?php 
class Final_deliveries_model extends CI_Model {

	public function insert_row($data){
        $result = $this->db->insert('cards.final_deliveries', $data);
        if ($result && $this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        }
        return false;
	}

    public function select_all($depth=0,$conditions=[],$campaign_id=0,$limit=false){
		$this->load->model('Deliveries_model','',TRUE);
		$this->load->model('Users_model','',TRUE);
        $this->db
            ->select("
                cards.final_deliveries.id,
                cards.final_deliveries.reception_date,
                cards.final_deliveries.comments,
                cards.final_deliveries.confirmed,
                (
                    (cards.final_deliveries.confirmed = 'f') OR
                    ((cards.final_deliveries.last_update + interval '" . CONFIRMED_DAYS_AVAILABLE . " days') > CURRENT_TIMESTAMP)
                ) as available,
                cards.deliveries.currency_id as currency,
                cards.deliveries.amount,
                cards.cards.campaign_id,
                cards.users.first_name,
                cards.users.last_name
            ")
            ->from('cards.final_deliveries')
            ->join('cards.deliveries', 'cards.deliveries.final_delivery_id = cards.final_deliveries.id')
            ->join('cards.cards', 'cards.cards.id = cards.deliveries.card_id')
            ->join('cards.users', 'cards.users.id = cards.deliveries.user_id')
            ->where('campaign_id', $campaign_id);
        if (!empty($conditions)){
            foreach($conditions as $condition){
                if (!isset($condition['operator'])){
                    $this->db->where($condition['key'], $condition['value']);
                }
                else{
                    switch ($condition['operator']){
                        case 'in' :
                            $this->db->where_in($condition['key'], $condition['value']);
                            if (empty($condition['value'])){
                                $this->db->reset_query();
                                return [];
                            }
                            break;
                        case 'null' :
                            $this->db->where($condition['value'] . ' is null');
                            break;
                        case 'notnull' :
                            $this->db->where($condition['value'] . ' is not null');
                            break;
                        default :
                            $this->db->where($condition['key'], $condition['operator'], $condition['value']);
                            break;
                    }
                }
            }
        }
        if ($limit !== false){
            $this->db->limit($limit);
        }
        $this->db->order_by('id desc');
        $result = $this->db->get()->result_array();
        if ($depth > 0){
            $depth--;
            for ($i=0; $i<count($result); $i++){
                // passing depth param as hardcoded 0 to avoid recursiveness
                $result[$i]['deliveries']   = $this->Deliveries_model->select_by_final_delivery($result[$i]['id'],0);
            }
        }
        return $result;
    }

    public function select($id,$depth=0){
		$this->load->model('Deliveries_model','',TRUE);
        $result = $this->db
            ->select("
                id,
                reception_date,
                comments,
                confirmed,
                ((confirmed = 'f') OR (last_update + interval '" . CONFIRMED_DAYS_AVAILABLE . " days') > CURRENT_TIMESTAMP) as available,
            ")
            ->from('cards.final_deliveries')
            ->where('id', $id)
            ->get()
            ->result_array();
        $this->db->flush_cache();
        if (!empty($result)){
            if ($depth > 0){
                $depth--;
                // passing depth param as hardcoded 0 to avoid recursiveness
                $result[0]['deliveries']    = $this->Deliveries_model->select_by_final_delivery($result[0]['id'],0);
                return $result[0];
            }
        }
        return $result;
    }

    public function update($id, $values){
        return $this->db
            ->where('id',$id)
            ->set($values)
            ->update('cards.final_deliveries');
    }

    public function mark_as_confirmed($ids){
        $all_ok = true;
        foreach ($ids as $id){
            $all_ok = $all_ok && $this->db
                ->where('id',$id)
                ->set(['confirmed' => true])
                ->update('cards.final_deliveries');
        }
        return $all_ok;
    }

    public function unmark_as_confirmed($ids){
        $all_ok = true;
        foreach ($ids as $id){
            $all_ok = $all_ok && $this->db
                ->where('id',$id)
                ->set(['confirmed' => false])
                ->update('cards.final_deliveries');
        }
        return $all_ok;
    }

    public function delete($id){
        return $this->db->where('id',$id)
            ->delete('cards.final_deliveries');
    }

}

