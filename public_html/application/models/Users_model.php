<?php 
class Users_model extends CI_Model {

    public function select_all($depth=0){
        return $this->db
            ->get('cards.users')
            ->result_array();
    }

    public function select($id){
        $result = $this->db->select('*')
            ->from('cards.users')
            ->where('id', $id)
            ->get()
            ->result_array();
        if (!empty($result)){
            return $result[0];
        }
        $this->db->flush_cache();
        return $result;
    }

    public function check_log($email,$password){
        $user_data =  $this->db->select('id,first_name,last_name,email,password,user_type')
            ->from('cards.users')
            ->where('email', $email)
            ->get()
            ->row_array();
		if (!empty($user_data) && password_verify($password,$user_data['password'])){
			unset($user_data['password']);
			return $user_data;	
		}
		else{
			return [];
		}	
    }

	public function insert($data){
		return $this->db->insert('cards.users',$data);
	}

}
