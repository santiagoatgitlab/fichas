<?php 
class Deliveries_model extends CI_Model {

	public function insert_row($data){
        $result = $this->db->insert('cards.deliveries', $data);
        if ($result && $this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        }
        return false;
	}

    public function select_all($depth=0,$conditions=[],$limit=false, $orderBy=null){
		$this->load->model('Cards_model','',TRUE);
		$this->load->model('Users_model','',TRUE);
		$this->load->model('Final_deliveries_model','',TRUE);
        $this->db
            ->select("
                id,
                currency_id as currency,
                amount,
                user_id,
                reception_date,
                card_id,
                final_delivery_id,
                comments
            ")
            ->from('cards.deliveries');
        if (!empty($conditions)){
            foreach($conditions as $condition){
                if (!isset($condition['operator'])){
                    $this->db->where($condition['key'], $condition['value']);
                }
                else{
                    switch ($condition['operator']){
                        case 'in' :
                            $this->db->where_in($condition['key'], $condition['value']);
                            if (empty($condition['value'])){
                                $this->db->reset_query();
                                return [];
                            }
                            break;
                        case 'null' :
                            $this->db->where($condition['key'] . ' is null');
                            break;
                        case 'notnull' :
                            $this->db->where($condition['key'] . ' is not null');
                            break;
                        default :
                            $this->db->where($condition['key'], $condition['operator'], $condition['value']);
                            break;
                    }
                }
            }
        }
        if ($limit !== false){
            $this->db->limit($limit);
        }
        if (is_null($orderBy)) {
            $this->db->order_by('id desc');
        }
        else {
            $this->db->order_by($orderBy);
        }
        $result = $this->db->get()->result_array();
        if ($depth > 0){
            $depth--;
            for ($i=0; $i<count($result); $i++){
                $card_conditions = [
                    [ 
                        "key"   => "id",
                        "value" => $result[$i]['card_id']
                    ] 
                ];
                $card = $this->Cards_model->select($depth,$card_conditions);
                if (!empty($card)){
                    $result[$i]['card'] = $card[0];
                }
                $result[$i]['user'] = $this->Users_model->select($result[$i]['user_id']);
                unset($result[$i]['card_id']);
                if (!is_null($result[$i]['final_delivery_id'])){
                    // passing depth param as hardcoded 0 to avoid recursiveness
                    $result[$i]['final_delivery'] = $this->Final_deliveries_model->select($result[$i]['final_delivery_id'],0);
                    if (!empty($result[$i]['final_delivery'])){
                        $result[$i]['final_delivery'] = $result[$i]['final_delivery'][0];
                    }
                    unset($result[$i]['final_delivery_id']);
                }
            }
        }
        return $result;
    }

    public function select($id,$depth=0){
		$this->load->model('Cards_model','',TRUE);
		$this->load->model('Users_model','',TRUE);
		$this->load->model('Final_deliveries_model','',TRUE);
        $result = $this->db
            ->select("
                id,
                currency_id as currency,
                amount,
                user_id,
                reception_date,
                card_id,
                final_delivery_id,
                comments
            ")
            ->from('cards.deliveries')
            ->where('id', $id)
            ->get()
            ->result_array();
        if (!empty($result)){
            if ($depth > 0){
                $depth--;
                $result[0]['card']              = $this->Cards_model->select($result[0]['card_id'],$depth);
                $result[0]['final_delivery']    = $this->Final_deliveries_model->select($result[0]['final_delivery_id'],$depth);
                $result[0]['user']              = $this->Users_model->select($result[0]['user_id']);
                unset($result[0]['card_id']);
                unset($result[0]['final_delivery_id']);
                return $result[0];
            }
        }
        return $result;
    }

    public function select_by_card($card_id,$depth){
		$this->load->model('Final_deliveries_model','',TRUE);
        $this->db->flush_cache();
        $result = $this->db
            ->select("
                id,
                currency_id as currency,
                amount,
                reception_date,
                final_delivery_id,
                comments
            ")
            ->from('cards.deliveries')
            ->where('card_id', $card_id)
            ->order_by('id desc')
            ->get()
            ->result_array();
        if ($depth > 0){
            $depth--;
            for ($i=0; $i<count($result); $i++){
                $result[$i]['final_delivery'] = $this->Final_deliveries_model->select($result[$i]['final_delivery_id'],$depth);
                unset($result[$i]['final_delivery_id']);
            }
        }
        return $result;
    }

    public function select_by_final_delivery($final_delivery_id,$depth){
		$this->load->model('Cards_model','',TRUE);
        $result = $this->db
            ->select("
                id,
                currency_id as currency,
                amount,
                reception_date,
                card_id
            ")
            ->from('cards.deliveries')
            ->where('final_delivery_id', $final_delivery_id)
            ->get()
            ->result_array();
        if ($depth > 0){
            $depth--;
            for ($i=0; $i<count($result); $i++){
                $result[$i]['card'] = $this->Cards_model->select($result[$i]['card_id'],$depth);
                unset($result[$i]['card_id']);
            }
        }
        return $result;
    }

    public function update($id, $values){
        return $this->db
            ->where('id',$id)
            ->set($values)
            ->update('cards.deliveries');
    }

    public function mark_as_delivered($ids,$comment,$date){
        $all_ok = true;
        if (count($ids) > 0){
            $data = [ 
                'reception_date' => $date,
                'comments' => $comment
            ];
            $result = $this->db->insert('cards.final_deliveries', $data);
            if ($result && $this->db->affected_rows() > 0) {
                foreach ($ids as $delivery_id){
                    $final_delivery_id = $this->db->insert_id();
                    $all_ok = $all_ok && $this->db
                        ->where('id', $delivery_id)
                        ->set(['final_delivery_id' => $final_delivery_id])
                        ->update('cards.deliveries');
                }
            }
            else{
                $all_ok = false;
            }
        }
        return $all_ok;
    }

    public function unmark_as_delivered($ids){
        $all_ok = true;
        foreach ($ids as $id){
            $all_ok = $all_ok && $this->db
                ->where('id',$id)
                ->set(['final_delivery_id' => null])
                ->update('cards.deliveries');
        }
        return $all_ok;
    }

    public function delete($ids){
        return $this->db->where_in('id',$ids)
            ->delete('cards.deliveries');
    }

    public function select_delivered(){

    }

}
