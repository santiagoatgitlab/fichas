<?php 
class Cards_model extends CI_Model {

	public function insert_row($data){
        $result = $this->db->insert('cards.cards', $data);
        if ($result && $this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        }
        return false;
	}

    public function select($depth=0,$conditions=[],$limit=false){

		$this->load->model('Campaigns_model','',TRUE);
		$this->load->model('Users_model','',TRUE);
		$this->load->model('Commitments_model','',TRUE);
		$this->load->model('Deliveries_model','',TRUE);
        $this->db->flush_cache();
        $this->db
            ->select("
                id,
                comments,
                case when closed is not null then 'si' else 'no' end as cerrada,
                campaign_id,
                user_id
            ")
            ->from('cards.cards')
            ->order_by('cards.last_update', 'desc');
        if (is_array($conditions) && !empty($conditions)){
            foreach($conditions as $condition){
                if (!isset($condition['operator'])){
                    $this->db->where($condition['key'], $condition['value']);
                }
                else{
                    switch ($condition['operator']){
                        case 'in' :
                            $this->db->where_in($condition['key'], $condition['value']);
                            if (empty($condition['value'])){
                                $this->db->reset_query();
                                return [];
                            }
                            break;
                        default :
                            $this->db->where($condition['key'], $condition['operator'], $condition['value']);
                            break;
                    }
                }
            }
        }
        if ($limit !== false){
            $this->db->limit($limit);
        }
        $this->db->order_by('id');
        $result = $this->db->get()->result_array();
        $this->db->flush_cache();
        if (!empty($result)){
            if ($depth > 0){
                $depth--;
                for ($i = 0; $i < count($result); $i++){
                    $contributors = $this->db
                        ->select("cn.id,first_name,last_name,email")
                        ->from('cards.contributors cn')
                        ->join('cards.card_contributor cc', 'cc.contributor_id = cn.id')
                        ->where('cc.card_id',$result[$i]['id'])
                        ->get()
                        ->result_array();
                    $this->db->flush_cache();
                    $campaign_filters = [
                        [ "key" => "id", "value" => $result[$i]['campaign_id'] ]
                    ];
                    $delivery_filters = [
                        [ "key" => "card_id", "value" => $result[$i]['id'] ]
                    ];
                    $result[$i]['campaign']     = $this->Campaigns_model->select(1,$campaign_filters)[0];
                    $result[$i]['user']         = $this->Users_model->select($result[$i]['user_id']);
                    $result[$i]['contributors'] = $contributors;
                    $result[$i]['commitments']  = $this->Commitments_model->select_by_card($result[$i]['id']);
                    $this->db->flush_cache();
                    $result[$i]['deliveries']   = $this->Deliveries_model->select_all($depth,$delivery_filters);
                    unset($result[$i]['campaign_id']);
                    unset($result[$i]['user_id']);
                    $this->db->flush_cache();
                }
            }
        }
        return $result;
    }


    public function update($id, $key_value_pairs){
        return $this->db
            ->where('id',$id)
            ->set($key_value_pairs)
            ->update('cards.cards');
    }

    public function delete($id){

        $this->db->where('card_id', $id)
            ->delete('cards.card_contributor');

        $this->db->where('card_id', $id)
            ->delete('cards.commitments');

        return $this->db->where('id',$id)
            ->delete('cards.cards');

    }

    public function refresh_contributors_list($card_id,$contributors_ids){
        $this->db->where('card_id',$card_id)
           ->delete('cards.card_contributor'); 
        $no_errors = true;
        foreach ($contributors_ids as $contributor_id){
            $no_errors = $no_errors && $this->db->insert('cards.card_contributor',[
                "card_id"           => intval($card_id),
                "contributor_id"    => intval($contributor_id)
            ]);
        }
        return $no_errors;
    }

}
