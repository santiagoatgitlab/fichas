<?php 
class Commitments_model extends CI_Model {

	public function insert_row($data){
        $result = $this->db->insert('cards.commitments', $data);
        if ($result && $this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        }
        return false;
	}

    public function select_all($depth=0){
		$this->load->model('Cards_model','',TRUE);
        $result = $this->db
            ->select("
                id,
                currency_id as currency,
                amount,
                card_id
            ")
            ->from('cards.commitments')
            ->get()
            ->result_array();
        if ($depth > 0){
            $depth--;
            for ($i=0; $i<count($result); $i++){
                $result[$i]['card'] = $this->Cards_model->select($result[$i]['card_id'],$depth);
                unset($result[$i]['card_id']);
            }
        }
        return $result;
    }

    public function select($id,$depth=0){
		$this->load->model('Cards_model','',TRUE);
        $result = $this->db
            ->select("
                id,
                currency_id as currency,
                amount,
                card_id
            ")
            ->from('cards.commitments')
            ->where('id', $id)
            ->get()
            ->result_array();
        if (!empty($result)){
            if ($depth > 0){
                $depth--;
                $result[0]['card'] = $this->Cards_model->select($result[0]['card_id'],$depth);
                unset($result[0]['card_id']);
            }
            return $result[0];
        }
        return $result;
    }

    public function select_by_card($card_ids){
		$this->load->model('Cards_model','',TRUE);
        $this->db->flush_cache();
        return $this->db
            ->select("
                id,
                currency_id as currency,
                amount
            ")
            ->from('cards.commitments')
            ->where_in('card_id', $card_ids)
            ->get()
            ->result_array();
    }

    public function update($id, $values){
        return $this->db
            ->where('id',$id)
            ->set($values)
            ->update('cards.commitments');
    }

    public function delete($ids){
        return $this->db->where_in('id',$ids)
            ->delete('cards.commitments');
    }

    public function delete_by_card($card_id){
        return $this->db->where('card_id',$card_id)
            ->delete('cards.commitments');
    }

}

