<?php 
class Campaigns_model extends CI_Model {

	public function insert_row($data){
        $result = $this->db->insert('cards.campaigns', $data);
        if ($result && $this->db->affected_rows() > 0) {
            return $this->db->insert_id();
        }
        return false;
	}

    public function select_all($depth=0){
        $campaigns =  $this->db
            ->select("id,name,begin_date,end_date, CASE WHEN end_date >= CURRENT_DATE THEN 'true' ELSE 'false' END as active")
            ->order_by('id')
            ->get('cards.campaigns')
            ->result_array();
        if ($depth > 0){
            $this->load->model('Cards_model','',TRUE);
            foreach ($campaigns as $campaign){
                $cards_conditions = [
                    [
                        "key" => "campaign_id",
                        "value" => $campaign["id"]
                    ]
                ];
                $campaign["cards"] = $this->Cards_model->select(0,$cards_conditions);
            }
        }
        return $campaigns;

    }

    public function select($depth=0,$conditions=[],$limit=false){

        $this->db->select('*')
            ->from('cards.campaigns');
        if (!empty($conditions)){
            foreach($conditions as $condition){
                if (!isset($condition['operator'])){
                    $this->db->where($condition['key'], $condition['value']);
                }
                else{
                    switch ($condition['operator']){
                        case 'in' :
                            $this->db->where_in($condition['key'], $condition['value']);
                            break;
                        default :
                            $this->db->where($condition['key'], $condition['operator'], $condition['value']);
                            break;
                    }
                }
            }
        }
        if ($limit !== false){
            $this->db->limit($limit);
        }
        $result = $this->db->get()->result_array();

        return $result;
    }

    public function update($id, $values){
        return $this->db
            ->where('id',$id)
            ->set($values)
            ->update('cards.campaigns');
    }

    public function delete($id){
        echo "deleting campaign $id";
        return $this->db->where('id',$id)
            ->delete('cards.campaigns');
    }

    public function isActive($id): Bool{
        return (Bool) $this->db->select('id')
            ->from('cards.campaigns')
            ->where('id',$id)
            ->where('end_date >=', date('Y-m-d'))
            ->get()->result_array();
    }
}
