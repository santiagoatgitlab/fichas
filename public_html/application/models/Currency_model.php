<?php 
class Currency_model extends CI_Model {

	public function insert_row($data){
        return $this->db->insert('cards.currency', $data);
	}

    public function select_all($depth=0){
        return $this->db
            ->get('cards.currency')
            ->result_array();
    }

    public function select($id,$depth=0){
        return $this->db->select('*')
            ->from('cards.currency')
            ->where('id', $id)
            ->get()
            ->result_array();
    }

    public function update($id, $values){
        return $this->db
            ->where('id',$id)
            ->set($values)
            ->update('cards.currency');
    }

    public function delete($id){
        return $this->db->where('id',$id)
            ->delete('cards.currency');
    }

}
