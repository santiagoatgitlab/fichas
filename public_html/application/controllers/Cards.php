<?php
defined('BASEPATH') OR exit('No direct script access allowed'); class Cards extends MY_Controller { /** * Index Page for this controller.  * * Maps to the following URL * 		http://example.com/index.php/welcome *	- or - * 		http://example.com/index.php/welcome/index *	- or - * Since this controller is set as the default controller in * config/routes.php, it's displayed at http://example.com/ *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function new_card($campaign_id) {

		$this->load->model('Campaigns_model','',TRUE);
		$this->load->model('Cards_model','',TRUE);
		$this->load->model('Commitments_model','',TRUE);

		if ( empty($this->input->post()) ){

            $menu = Globals::get_full_menu();
            $menu['create_card'] = false;
            $menu['campaign']    = true;
            $data['menu']  		 = $menu;
            $data['campaign_id'] = $campaign_id;

            $conditions = [
                [
                    'key'   => 'id',
                    'value' => $campaign_id
                ]
            ];

            $data['campaign']        = $this->Campaigns_model->select(1, $conditions)[0];
            $data['scripts'] 	     = ['new_card.js'];
            $data['csrf_token_name'] = $this->security->get_csrf_token_name();
            $data['csrf_hash']       = $this->security->get_csrf_hash();
            $data['active_campaign'] = $this->Campaigns_model->isActive($campaign_id) ? true : false;
            if (!$data['active_campaign']){
                header('location: /campania/' . $campaign_id);
            }
            $this->layout('create_card',$data);

        }
        else{
            
            $no_errors = true;
            
            $campaign_id  = $this->input->post('campaign_id');
            $contributors = $this->input->post('contributors');
            $commitments  = $this->input->post('commitments');
            $comments     = $this->input->post('comments');

            $card_data = [
                'user_id'     => $_SESSION['id'],
                'campaign_id' => $campaign_id,
                'comments'    => $comments
            ];

            $no_errors &= $card_id = $this->Cards_model->insert_row($card_data);
            
            if ($card_id !== false){

                $no_errors &= $this->Cards_model->refresh_contributors_list($card_id,$contributors);

                foreach ($commitments as $currency => $amount){
                    if (!empty($amount)){
                        $commitment_data = [
                            'card_id'     => $card_id,
                            'currency_id' => $currency,
                            'amount'      => $amount
                        ];
                        $no_errors &= $this->Commitments_model->insert_row($commitment_data);
                    }
                }

            }

            $_SESSION['card_created'] = $no_errors;
            $this->session->mark_as_flash('card_created');
            header('location: /campania/' . $campaign_id);

        }

    }

    public function create_card(){
 
		$this->load->model('Cards_model','',TRUE);

        $card_data        = $this->input->post('card_data');
        $contributors_ids = $this->input->post('contributors_ids');

        $result = $this->Cards_model->insert_row($card_data);
        if ($result !== false){
            $card_id = $result;
            $result = $this->Cards_model->refresh_contributors_list($card_id,$contributors_ids);
        }
        echo json_encode(["status" => $result ? 0 : EXIT_ERROR]);
    }
            
    public function show($card_id){

		$this->load->model('Cards_model','',TRUE);

        $card_id = intval($card_id);

        $menu         = Globals::get_full_menu();
		$data['menu'] = $menu;
        $conditions   = [
            [
                'key'   => 'id',
                'value' => $card_id
            ]
        ];
        
        $data['success_message'] = '';
        $data['error_message']   = '';
        if (isset($_SESSION['delivery_created'])){
			$data['success_message'] = 'La entrega fué ingresada correctamente';
        }
        else if (isset($_SESSION['delivery_updated'])){
			$data['success_message'] = 'La entrega fué actualizada correctamente';
        }
        else if (isset($_SESSION['delivery_removed'])){
			$data['success_message'] = 'La entrega fué eliminada correctamente';
        }

        $data['card'] = $this->Cards_model->select(2,$conditions)[0];
        $data['card_name'] = implode(' - ', array_reduce($data['card']['contributors'], function ($accum, $item){
            return array_merge ($accum, [ ucfirst($item['first_name']) . ' ' . ucfirst($item['last_name'])] );
        }, []));
        $data['user_id'] = $_SESSION['id'];
        $data['is_admin'] = $_SESSION['user_type'] == 'a';
        $data['campaign_id'] = $data['card']['campaign']['id'];
        $data['active_campaign'] = $this->Campaigns_model->isActive($data['campaign_id']) ? true : false;
        $data['menu']['create_card'] = $data['active_campaign'] ? true : false;
        $this->layout('card',$data);

    }

    public function edit($card_id){

		$this->load->model('Cards_model','',TRUE);
		$this->load->model('Commitments_model','',TRUE);
		$this->load->model('Campaigns_model','',TRUE);

        $card_id = intval($card_id);

		if ( empty($this->input->post()) ){


            $menu = Globals::get_full_menu();
            $menu['create_card'] = false;
            $menu['campaign']    = true;
            $menu['card']        = true;
            $data['menu']        = $menu;
            $conditions = [
                [
                    'key'   => 'id',
                    'value' => $card_id
                ]
            ];
            
            $data['success_message'] = '';
            $data['error_message']   = '';

            $data['card'] = $this->Cards_model->select(1,$conditions)[0];

            $data['contributor'] = !empty($data['card']['contributors'])
                                ? $data['card']['contributors'][0]
                                : null;
            $data['extra_contributors'] = isset($data['card']['contributors'])
                                ? count($data['card']['contributors']) > 1
                                    ? array_slice($data['card']['contributors'], 1, count($data['card']['contributors']))
                                    : []
                                : [];

            $data['commitments'] = [];
            for ($i = 0; $i < count($data['card']['commitments']); $i++){
                $data['commitments'][$data['card']['commitments'][$i]['currency']] = 
                    $data['card']['commitments'][$i]['amount'];
            }

            $data['card_name']          = implode(' - ', array_reduce($data['card']['contributors'], function ($accum, $item){
                return array_merge ($accum, [ ucfirst($item['first_name']) . ' ' . ucfirst($item['last_name'])] );
            }, []));

            $data['scripts'] 	     = ['new_card.js'];
            $data['csrf_token_name'] = $this->security->get_csrf_token_name();
            $data['csrf_hash']       = $this->security->get_csrf_hash();
            $data['card_id']         = $data['card']['id'];
            $data['campaign_id']     = $data['card']['campaign']['id'];
            $data['is_admin']        = $_SESSION['user_type'] == 'a';

            $data['active_campaign'] = $this->Campaigns_model->isActive($data['campaign_id']) ? true : false;

            $this->layout('edit_card',$data);

        }
        else{

            $card_id      = $this->input->post('card_id');
            $contributors = $this->input->post('contributors');
            $commitments  = $this->input->post('commitments');
            $comments     = $this->input->post('comments');

            $contributors = array_filter($contributors, function ($contributor_id) {
                return $contributor_id !== "";  
            });

            $update = boolval($this->Cards_model->update($card_id, [ "comments" => $comments ] ));
            $update = $update && $this->Cards_model->refresh_contributors_list($card_id,$contributors);
            $update = $update && $this->Commitments_model->delete_by_card($card_id);
            foreach($commitments as $currency_id => $amount){
                if (!empty($amount)){
                    $row = [
                        'card_id'     => intval($card_id),
                        'currency_id' => $currency_id,
                        'amount'      => intval($amount)
                    ];
                    $update = $update && $this->Commitments_model->insert_row($row);
                }
            }

            header('location: /ficha/' . $card_id);

        }

    }

    public function load_rows($last_row_id){
            $this->load->model('Cards_model','',TRUE);
        $data['cards'] = $this->Cards_model->select_all(1,1,CARDS_LIST_ROWS);
		$view = $this->twig->render('cards_rows',$data);
        echo json_encode([
            "status"  => 0,
            "content" => $view
        ]);

    }

    public function load_by_campaign($campaign_id,$filter,$order){
            
		$this->load->model('Cards_model','',TRUE);

        $conditions = [
            [
                "key"   => "campaign_id",
                "value" => $campaign_id
            ]
        ];
        if ($filter == 'owner'){
            $conditions[] = [
                "key"   => "user_id",
                "value" => $_SESSION['id']
            ];
        }
        $cards = $this->Cards_model->select(1,$conditions,false,"");

        for($i=0; $i<count($cards); $i++){
            $contributors_text = '';
            foreach ($cards[$i]['contributors'] as $contributor){
                if ($contributors_text != ''){
                    $contributors_text .= ', ';
                }
                $contributors_text .= ucwords($contributor['first_name'] . ' ' . $contributor['last_name']);
            }
            $cards[$i]['contributors_text'] = $contributors_text;
        }

        if ($order == 'date'){
            $data['cards'] = $cards;
        }
        else{
            $data['cards'] = $this->sort_by_field($cards,'contributors_text',$order);
        }

		$view = $this->twig->render('cards_list',$data);
        echo json_encode([
            "status"  => 0,
            "content" => $view
        ]);

    }

    public function search_by_contributor($campaign_id){

        $search_str = $this->input->post('search_str');

		$this->load->model('Contributors_model','',TRUE);
		$this->load->model('Campaigns_model','',TRUE);
        $contributors = $this->Contributors_model->select(1,[],false,$search_str);

        $card_ids = [];
        foreach($contributors as $contributor){
            foreach($contributor['cards'] as $card){
                if (!in_array($card['id'],$card_ids)){
                    $card_ids[] = intval($card['id']);
                }
            }
        }

        $conditions = [];
        $cards      = [];
        if (!empty($card_ids)){
            $conditions = [
                [
                    "key"      => "id",
                    "operator" => "in",
                    "value"    => $card_ids
                ],
                [
                    "key"   => "campaign_id",
                    "value" => $campaign_id
                ]
            ];
            $cards = $this->Cards_model->select(1,$conditions);

            for($i=0; $i<count($cards); $i++){
                $contributors_text = '';
                foreach ($cards[$i]['contributors'] as $contributor){
                    if ($contributors_text != ''){
                        $contributors_text .= ', ';
                    }
                    $contributors_text .= ucwords($contributor['first_name'] . ' ' . $contributor['last_name']);
                }
                $cards[$i]['contributors_text'] = $contributors_text;
            }
        }
        $data['cards'] = $cards;

		$view = $this->twig->render('cards_list',$data);
        echo json_encode([
            "status"  => 0,
            "content" => $view,
            "rainbow" => $this->security->get_csrf_hash()
        ]);

    }

    public function update(){

		$this->load->model('Cards_model','',TRUE);

        $card_id         = $this->input->post('card_id');
        $key_value_pairs = $this->input->post('key_value_pairs');

        $result = $this->Cards_model->update($card_id,$key_value_pairs);
        echo json_encode(["status" => $result ? 0 : EXIT_ERROR]);

    }

    public function update_contributors_list(){

		$this->load->model('Cards_model','',TRUE);

        $card_id          = $this->input->post('card_id');
        $contributors_ids = $this->input->post('contributors_ids');

        $result = $this->Cards_model->refresh_contributors_list($card_id,$contributors_ids);
        echo json_encode(["status" => $result ? 0 : EXIT_ERROR]);

    }

    public function remove(){

        $this->check_log(['a']);
		$this->load->model('Cards_model','',TRUE);

        $id = $this->input->post('id');

        $result = $this->Cards_model->delete($id);
        echo json_encode(["status" => $result ? 0 : EXIT_ERROR]);

    }

    private function sort_by_field($array,$field,$direction){
        for($ceil = count($array) - 1; $ceil > 0; $ceil--){
            for($j = 0; $j < $ceil; $j++){

                $condition = $direction === 'asc'
                    ? $array[$j][$field] > $array[$j+1][$field]
                    : $array[$j][$field] < $array[$j+1][$field];

                if ($condition){
                    $aux         = $array[$j];
                    $array[$j]   = $array[$j+1];
                    $array[$j+1] = $aux;
                }

            }
        }

        return $array;
    }
}
?>
