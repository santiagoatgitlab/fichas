<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller {

    /** * Index Page for this controller.  * * Maps to the following URL * 		http://example.com/index.php/welcome *	- or - * 		http://example.com/index.php/welcome/index *	- or - * Since this controller is set as the default controller in * config/routes.php, it's displayed at http://example.com/ *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function show($campaign_id){

		$this->load->model('Campaigns_model','',TRUE);
		$this->load->model('Cards_model','',TRUE);
		$this->load->model('Commitments_model','',TRUE);
		$this->load->model('Contributors_model','',TRUE);
		$this->load->model('Deliveries_model','',TRUE);
		$this->load->model('Final_deliveries_model','',TRUE);

		$menu  		    = Globals::get_full_menu();
        $menu['report'] = false;
        $data['menu']   = $menu;

        $data['campaign'] = $this->Campaigns_model->select(0,[['key'=>'id','value'=>$campaign_id]])[0];
        $data['campaign_id'] = $data['campaign']['id'];


        //cards
        $campaign_cards         = $this->Cards_model->select(0,[['key'=>'campaign_id','value'=>$campaign_id]]);
        $data['total_cards']    = count($campaign_cards);


        //contributors
        $cards_ids = array_map(function($card){
            return $card['id'];
        }, $campaign_cards);
        $campaign_contributors      = $this->Contributors_model->select_by_card($cards_ids);
        $data['total_contributors'] = $campaign_contributors->num_rows();


        //commitments
        $campaign_commitments   = $this->Commitments_model->select_by_card($cards_ids);
        $data['commitments']    = $this->sumUpByCurrency($campaign_commitments);

        // deliveries
        $deliveries = $this->Deliveries_model->select_all(0,[
            ['key'=>'card_id','operator'=>'in','value'=>$cards_ids],
            ['key'=>'final_delivery_id','operator'=>'null']
        ]);
        $data['collectors'] = $this->sumUpByCurrency($deliveries);

        //delivered to commision
        $final_deliveries   = $this->Final_deliveries_model->select_all(0,[],$campaign_id);
        $commision = array_filter($final_deliveries, function($delivery){
            return $delivery['confirmed'] == 'f';
        });
        $data['commision'] = $this->sumUpByCurrency($commision);

        //delivered to commision and confirmed
        $confirmed = array_filter($final_deliveries, function($delivery){
            return $delivery['confirmed'] == 't';
        });

        $data['confirmed'] = $this->sumUpByCurrency($confirmed);

        $data['totals']['ARS'] = $this->getTotals($data, 'ARS');
        $data['totals']['USD'] = $this->getTotals($data, 'USD');
        $data['totals']['EUR'] = $this->getTotals($data, 'EUR');

        $deliveriesMonths = $this->Deliveries_model->select_all(0,[
            ['key'=>'card_id','operator'=>'in','value'=>$cards_ids],
        ], false, 'reception_date');
        $data['amountsByMonth'] = $this->sumUpByMonth($deliveriesMonths);

        $this->layout('report',$data);
    }

    private function sumUpByMonth($deliveries) {

        $months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        foreach ($deliveries as $delivery){
            $datetime  = DateTime::createFromFormat('Y-m-d', $delivery['reception_date']);
            $yearmonth = $months[$datetime->format('n')-1].'-'.$datetime->format('Y');
            $deliveriesByMonth[$yearmonth][] = $delivery;
        }


        foreach ($deliveriesByMonth as $yearmonth => $monthDeliveries) {
            $result[] = [
                'month'   => explode('-',$yearmonth)[0],
                'year'    => explode('-',$yearmonth)[1],
                'amounts' => $this->sumUpByCurrency($monthDeliveries),
            ];
        }

        return $result;

    }

    private function sumUpByCurrency($deliveries){

        $result = [
            "ARS" => 0,
            "USD" => 0,
            "EUR" => 0
        ];

        foreach ($deliveries as $delivery){
            $result[$delivery['currency']] += $delivery['amount'];
        }

        return $result;

    }

    private function getTotals($data, $currency){

        return $data['collectors'][$currency] + $data['commision'][$currency] + $data['confirmed'][$currency];

    }
}
?>
