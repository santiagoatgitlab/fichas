<?php
defined('BASEPATH') OR exit('No direct script access allowed'); class Commitments extends MY_Controller { /** * Index Page for this controller.  * * Maps to the following URL * 		http://example.com/index.php/welcome *	- or - * 		http://example.com/index.php/welcome/index *	- or - * Since this controller is set as the default controller in * config/routes.php, it's displayed at http://example.com/ *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function new_commitment(){
 
		$this->load->model('Commitments_model','',TRUE);

        $commitment_data      = $this->input->post('commitment_data');

        $result = $this->Commitments_model->insert_row($commitment_data);
        echo json_encode(["status" => $result ? 0 : EXIT_ERROR]);
    }
            
    public function get_all_by_card(){
            
		$this->load->model('Commitments_model','',TRUE);

        $card_id = $this->input->post('card_id');

        $data['commitments'] = $this->Commitments_model->select_by_card($card_id);
		$view = $this->twig->render('commitments_list',$data);
        echo json_encode([
            "status"    => 0,
            "content"   => $view
        ]);

    }

    public function update(){

		$this->load->model('Commitments_model','',TRUE);

        $key_value_pairs    = $this->input->post('key_value_pairs');
        unset($key_value_pairs['id']);

        $result = $this->Commitments_model->update($key_value_pairs['id'],$key_value_pairs);
        echo json_encode(["status" => $result ? 0 : EXIT_ERROR]);

    }

    public function delete(){

		$This->load->model('Commitment_model','',TRUE);

        $ids = $this->input->post('ids');

        $result = $this->Commitment_model->delete($ids);
        echo json_encode(["status" => $result ? 0 : EXIT_ERROR]);

    }

}
?>

