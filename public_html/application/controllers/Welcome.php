<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function login()
	{
		$this->layout('login');
	}
	public function select_campaign()
	{
		$this->layout('select_campaign');
	}
	public function cards()
	{
		$this->layout('campaign');
	}
	public function card()
	{
		$this->layout('card');
	}
	public function create_card()
	{
		$this->layout('create_card');
	}
	public function tutorial()
	{
		$this->layout('tutorial');
	}
	public function report()
	{
		$this->layout('report');
	}
	public function manage_deliveries()
	{
		$this->layout('manage_deliveries');
	}
	public function commission_panel()
	{
		$this->layout('commission_panel');
	}
	public function commission_history()
	{
		$this->layout('commission_history');
	}
	public function create_delivery()
	{
		$this->layout('create_delivery');
	}
}
