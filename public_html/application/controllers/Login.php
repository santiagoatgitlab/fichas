<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('Users_model','',TRUE);
		$this->data['error'] = false;
		$this->data['email'] = '';
		
		if (isset($_SESSION['id'])){
            header('location: /');
            exit;
		}
		
		if ( !empty($this->input->post()) ){

            $email      = $this->input->post('email');
            $password   = $this->input->post('password');
			
			$user_data = $this->Users_model->check_log($email,$password);
			
			if (!empty($user_data)){
				
                $_SESSION['id']         = $user_data['id'];
                $_SESSION['email']      = $user_data['email'];
                $_SESSION['first_name'] = $user_data['first_name'];
                $_SESSION['last_name']  = $user_data['last_name'];
                $_SESSION['user_type']  = $user_data['user_type'];

                header('location: /');
                exit;
				
			}
			else{
				$this->data['error'] = true;
				$this->data['email'] = $this->input->post('email');
			}
		}
		
        $data['csrf_token_name']    = $this->security->get_csrf_token_name();
        $data['csrf_hash']          = $this->security->get_csrf_hash();
		$data['login_page']			= true;
		$this->layout('login',$data);
	}

    public function user_home(){

        $this->check_log(['a','c','r']);

        if ($_SESSION['user_type'] == 'c'){
            // comisión
            header('location: /campanias');
            exit;
        }
        else{
            // Recaudador
            header('location: /campanias');
            exit;
        }
    }
	
	public function logout(){
		
		unset($_SESSION['id']);
		unset($_SESSION['email']);
		unset($_SESSION['first_name']);
		unset($_SESSION['last_name']);
		unset($_SESSION['user_type']);
		header('location: /login');
        exit;
		
	}

}

