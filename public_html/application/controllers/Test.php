<?php
defined('BASEPATH') OR exit('No direct script access allowed'); class Test extends MY_Controller { /** * Index Page for this controller.  * * Maps to the following URL * 		http://example.com/index.php/welcome *	- or - * 		http://example.com/index.php/welcome/index *	- or - * Since this controller is set as the default controller in * config/routes.php, it's displayed at http://example.com/ *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct(){
        parent::__construct();
    }

	public function currency($op,$id=0){

		$this->load->model('Currency_model','',TRUE);
        if ($op == 'insert') {
            $curr = [
                "id" => "ARS",
                "denomination" => "pesos",
                "value" => 0.02
            ];
            echo $this->Currency_model->insert_row($curr);
        }

        if ($op == 'select') {

            if ($id != 0){
                $data['currencies'] = $this->Currency_model->select_all();
            }
            else{
                $data['currencies'] = $this->Currency_model->select($id);
            }
            $this->layout('main', $data);
        }

        if ($op == 'update'){
            $values = [
                "denomination"   => "sopes"
            ];
            echo $this->Currency_model->update($id,$values);
        }

        if ($op == 'delete') {
            echo $this->Currency_model->delete($id);
        }

	}

    public function contributors($op,$id=0){

		$this->load->model('Contributors_model','',TRUE);
        if ($op == 'select'){
            if ($id != 0){
                $result = $this->Contributors_model->select($id);
            }
            else{
                $result = $this->Contributors_model->select_all();
            }
        }
        echo '<pre>'; var_dump($result); echo '</pre>';

    }

    public function users($op,$id=0){

		$this->load->model('Users_model','',TRUE);
        if ($op == 'select'){
            if ($id != 0){
                $result = $this->Users_model->select($id);
            }
            else{
                $result = $this->Users_model->select_all();
            }
        }

        echo '<pre>'; var_dump($result); echo '</pre>';

    }

    public function campaigns($op,$id=0){

		$this->load->model('Campaigns_model','',TRUE);

        if ($op == 'insert'){
            $cam = [
                "name"        => "2019",
                "begin_date"  => "2019-01-01",
                "end_date"    => "2019-12-31"
            ];
            $result = $this->Campaigns_model->insert_row($cam);
        }

        if ($op == 'select'){
            if ($id != 0){
                $result = $this->Campaigns_model->select($id);
            }
            else{
                $result = $this->Campaigns_model->select_all();
            }
        }

        if ($op == 'update'){
            $values = [
                "begin_date"   => "2021-02-01",
                "end_date" => "2021-11-30"
            ];
            $result = $this->Campaigns_model->update($id,$values);
        }

        if ($op == 'delete') {
            $result = $this->Campaigns_model->delete([11,12]);
        }

        echo '<pre>'; var_dump($result); echo '</pre>';
    }

    public function cards($op,$id=0,$depth=0){

		$this->load->model('Cards_model','',TRUE);

        if ($op == 'insert'){
            $cam = [
                "campaign_id"   => 1,
                "user_id"       => 2,
                "comments"      => "Compromisos para octubre y diciembre"
            ];
            $result = $this->Cards_model->insert_row($cam);
        }

        if ($op == 'select'){
            if ($id != 0){
                $result = $this->Cards_model->select($id,$depth);
            }
            else{
                $result = $this->Cards_model->select_all($depth);
            }
        }

        if ($op == 'select_range'){
            $result = $this->Cards_model->select_all($depth,2,5);
        }

        if ($op == 'update'){
            $values = [
                "user_id"    => 4
            ];
            $result = $this->Cards_model->update($id,$values);
        }

        if ($op == 'delete') {
            $result = $this->Cards_model->delete($id);
        }

        if ($op == 'add'){
            $list = [9,10];
            $result = $this->Cards_model->refresh_contributors_list($id,$list);
        }

        echo '<pre>'; var_dump($result); echo '</pre>';
    }

	public function commitments($op,$id=0,$depth=0){

		$this->load->model('Commitments_model','',TRUE);

        if ($op == 'select'){
            if ($id != 0){
                $result = $this->Commitments_model->select($id,$depth);
            }
            else{
                $result = $this->Commitments_model->select_all($depth);
            }
        }

        if ($op == 'insert'){
            $comm = [
                "card_id"       => 2,
                "currency_id"   => "EUR",
                "amount"        => "200"
            ];
            $result = $this->Commitments_model->insert_row($comm);
        }

        if ($op == 'update'){
            $values = [
                "amount" => 3000
            ];
            $result = $this->Commitments_model->update($id,$values);
        }

        if ($op == 'delete') {
            $result = $this->Commitments_model->delete($id);
        }

        echo '<pre>'; var_dump($result); echo '</pre>';

	}

    public function deliveries($op,$id=0,$depth=0){

		$this->load->model('Deliveries_model','',TRUE);

        if ($op == 'insert'){
            $del = [
                "card_id"           => 2,
                "currency_id"       => "ARS",
                "amount"            => 3000,
                "user_id"           => 2,
                "reception_date"    => "2020-01-16"
            ];
            $result = $this->Deliveries_model->insert_row($del);
        }

        if ($op == 'select'){
            if ($id != 0){
                $result = $this->Deliveries_model->select($id,$depth);
            }
            else{
                $result = $this->Deliveries_model->select_all($depth);
            }
        }

        if ($op == 'update'){
            $values = [
                "final_delivery_id"    => 1
            ];
            $result = $this->Deliveries_model->update($id,$values);
        }

        if ($op == 'delete') {
            $result = $this->Deliveries_model->delete($id);
        }

        echo '<pre>'; var_dump($result); echo '</pre>';
    }

    public function final_deliveries($op,$id=0,$depth=0){

		$this->load->model('Final_deliveries_model','',TRUE);

        if ($op == 'insert'){
            $del = [
                "user_id"           => 2,
                "reception_date"    => "2020-01-14"
            ];
            $result = $this->Final_deliveries_model->insert_row($del);
        }

        if ($op == 'select'){
            if ($id != 0){
                $result = $this->Final_deliveries_model->select($id,$depth);
            }
            else{
                $result = $this->Final_deliveries_model->select_all($depth);
            }
        }

        if ($op == 'update'){
            $values = [
                "reception_date"    => "2020-01-03"
            ];
            $result = $this->Final_deliveries_model->update($id,$values);
        }

        if ($op == 'delete') {
            $result = $this->Final_deliveries_model->delete($id);
        }

        echo '<pre>'; var_dump($result); echo '</pre>';
    }

    function csrf(){
        echo "name: " . $this->security->get_csrf_token_name() . '<br>';
        echo "value: " . $this->security->get_csrf_hash();
    }
}
