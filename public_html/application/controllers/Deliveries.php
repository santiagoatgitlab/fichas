<?php
defined('BASEPATH') OR exit('No direct script access allowed'); class Deliveries extends MY_Controller { /** * Index Page for this controller.  * * Maps to the following URL * 		http://example.com/index.php/welcome *	- or - * 		http://example.com/index.php/welcome/index *	- or - * Since this controller is set as the default controller in * config/routes.php, it's displayed at http://example.com/ *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function new_delivery($card_id){

		$this->load->model('Cards_model','',TRUE);
        $this->load->model('Deliveries_model','',TRUE);
        $this->load->model('Campaigns_model','',TRUE);
 
		if ( empty($this->input->post()) ){

            $menu                    = Globals::get_full_menu();
            $menu['card']            = true;
            $menu['create_card']     = false;
            $data['menu']  		     = $menu;
            $data['csrf_token_name'] = $this->security->get_csrf_token_name();
            $data['csrf_hash']       = $this->security->get_csrf_hash();
            $data['card']            = $this->Cards_model->select(1, [ [ 'key' => 'id', 'value' => $card_id ] ] )[0];
            $data['card_id']         = $data['card']['id'];
            $data['campaign_id']     = $data['card']['campaign']['id'];
            $data['card_name']       = implode(' - ', array_reduce($data['card']['contributors'], function ($accum, $item){
                return array_merge ($accum, [ ucfirst($item['first_name']) . ' ' . ucfirst($item['last_name'])] );
            }, []));
            $data['active_campaign'] = $this->Campaigns_model->isActive($data['campaign_id']) ? true : false;
            $data['scripts']         = ['create_delivery.js'];
            if (!$data['active_campaign']){
                header('location: /ficha/' . $card_id);
            }


            $data['mindate'] = (new DateTime())->sub(new DateInterval('P1Y'))->format('Y-m-d');
            $data['maxdate'] = (new DateTime())->add(new DateInterval('P1Y'))->format('Y-m-d');
            $this->layout('create_delivery',$data);

        }
        else{
            $no_errors = true;
            
            $card_id        = $this->input->post('card_id');
            $reception_date = $this->input->post('reception_date');
            $currency       = $this->input->post('currency');
            $amount         = $this->input->post('amount');
            $comments       = $this->input->post('comments');

            $delivery_data = [
                'user_id'           => $_SESSION['id'],
                'card_id'           => $card_id,
                'reception_date'    => $reception_date,
                'comments'          => $comments,
                'currency_id'       => $currency,
                'amount'            => $amount
            ];

            $no_errors &= $this->Deliveries_model->insert_row($delivery_data);

            $_SESSION['delivery_created'] = $no_errors;
            $this->session->mark_as_flash('delivery_created');
            header('location: /ficha/' . $card_id);

        }
    }
            
    public function get_all_by_card(){
            
		$this->load->model('Deliveries_model','',TRUE);

        $card_id = $this->input->post('card_id');

        $data['deliveries'] = $this->Deliveries_model->select_by_card($card_id);
		$view = $this->twig->render('deliveries_list',$data);
        echo json_encode([
            "status"    => 0,
            "content"   => $view
        ]);

    }

    public function update($delivery_id){

        $this->load->model('Deliveries_model','',TRUE);
        $this->load->model('Campaigns_model','',TRUE);
        $delivery = $this->Deliveries_model->select($delivery_id)[0];
        $card_id  = $delivery['card_id'];
 
		if ( empty($this->input->post()) ){
            $data['delivery']        = $this->Deliveries_model->select($delivery_id)[0];
            
            $menu  		             = Globals::get_full_menu();
            $menu['create_card']     = false;
            $menu['card']            = true;
            $data['menu']  		     = $menu;
            $data['csrf_token_name'] = $this->security->get_csrf_token_name();
            $data['csrf_hash']       = $this->security->get_csrf_hash();
            $data['card']            = $this->Cards_model->select(1, [ [ 'key' => 'id', 'value' => $card_id ] ] )[0];
            $data['card_id']         = $data['card']['id'];
            $data['campaign_id']     = $data['card']['campaign']['id'];
            $data['card_name']       = implode(' - ', array_reduce($data['card']['contributors'], function ($accum, $item){
                return array_merge ($accum, [ ucfirst($item['first_name']) . ' ' . ucfirst($item['last_name'])] );
            }, []));

            $data['comments_placeholder'] = "Completar con comentarios que nos sirvan de referencia personal. Ejemplo: Si fue por transferencia o en mano, etc.";

            $data['active_campaign'] = $this->Campaigns_model->isActive($data['campaign_id']) ? true : false;
            $data['scripts']  = ['create_delivery.js'];
            $data['is_admin'] = $_SESSION['user_type'] == 'a';

            $data['mindate'] = (new DateTime())->sub(new DateInterval('P1Y'))->format('Y-m-d');
            $data['maxdate'] = (new DateTime())->add(new DateInterval('P1Y'))->format('Y-m-d');
            $this->layout('create_delivery',$data);

        }
        else{

            $no_errors = true;
            
            $card_id        = $this->input->post('card_id');
            $reception_date = $this->input->post('reception_date');
            $amount         = $this->input->post('amount');
            $currency       = $this->input->post('currency');
            $comments       = $this->input->post('comments');

            $key_value_pairs = [
                'reception_date' => $reception_date,
                'amount'         => $amount,
                'currency_id'    => $currency,
                'comments'       => $comments
            ];

            $no_errors &= $this->Deliveries_model->update($delivery_id,$key_value_pairs);


            $_SESSION['delivery_updated'] = $no_errors;
            $this->session->mark_as_flash('delivery_updated');
            header('location: /ficha/' . $card_id);

        }

    }

    public function manage($campaign_id){

        $user_id = $_SESSION['id'];

		$this->load->model('Deliveries_model','',TRUE);
		$this->load->model('Campaigns_model','',TRUE);

        $menu = Globals::get_full_menu();
        $menu['admin_deliveries']   = false;
        $menu['create_card']        = false;
        $data['menu'] = $menu;


        //
        // select all the needed data for showing total amounts
        //

        $non_delivered_filters = [
            [ 'key' => 'final_delivery_id', 'operator' => 'null' ],
            [ 'key' => 'user_id', 'value' => $user_id ]
        ];
        $non_delivered = $this->Deliveries_model->select_all(1,$non_delivered_filters);

        // filter deliveries belonging to different campaigns
        $non_delivered = array_filter($non_delivered, function ($delivery) use ($campaign_id) {
            return $delivery['card']['campaign_id'] == $campaign_id;
        });

        $delivered_filters = [
            [ 'key' => 'final_delivery_id', 'operator' => 'notnull' ],
            [ 'key' => 'user_id', 'value' => $user_id ]
        ];
        $delivered = $this->Deliveries_model->select_all(1,$delivered_filters);

        // filter deliveries belonging to different campaigns
        $delivered = array_filter($delivered, function ($delivery) use ($campaign_id) {
            return $delivery['card']['campaign_id'] == $campaign_id;
        });

        $confirmed = [];
        foreach($delivered as $current_delivered){
            if ($current_delivered['final_delivery']['confirmed'] === 't'){
                $confirmed[] = $current_delivered;
            }
        }

        $delivered_not_confirmed = [];
        foreach($delivered as $current_delivered){
            if ($current_delivered['final_delivery']['confirmed'] === 'f'){
                $delivered_not_confirmed[] = $current_delivered;
            }
        }

        //
        // do the additions
        //

        $data['amounts']['non_delivered']   = $this->sumUpByCurrency($non_delivered);
        $data['amounts']['delivered']       = $this->sumUpByCurrency($delivered_not_confirmed);
        $data['amounts']['confirmed']       = $this->sumUpByCurrency($confirmed);


        //
        // get user cards
        //

        $deliveries_filters = [
            [ 
                'key'   => 'user_id',
                'value' => $user_id
            ]
        ];

        $data['scripts'] 	     = ['manage_deliveries.js'];
        $data['csrf_token_name'] = $this->security->get_csrf_token_name();
        $data['csrf_hash']       = $this->security->get_csrf_hash();
        $deliveries              = $this->Deliveries_model->select_all(2,$deliveries_filters);

        // filter deliveries belonging to different campaigns

        $data['deliveries'] = array_filter($deliveries, function ($delivery) use ($campaign_id) {
            return $delivery['card']['campaign']['id'] == $campaign_id
                && (empty($delivery['final_delivery']) || $delivery['final_delivery']['confirmed'] == 'f');
        });

        // confirmed deliveries
        $confirmed_filters = [
            [ 'key' => 'confirmed', 'value' => true ],
            [ 'key' => 'deliveries.user_id', 'value' => $user_id ]
        ];
        $confirmed = $this->Final_deliveries_model->select_all(1,$confirmed_filters, $campaign_id);
        $data['confirmed'] = array_reduce($confirmed, function ($carry, $item){
                if (isset($carry[$item['id']])){
                    $carry[$item['id']]['amount'] += $item['amount'];
                }
                else{
                $carry[$item['id']] = [
                    'id'             => $item['id'],
                    'reception_date' => $item['reception_date'],
                    'comments'       => $item['comments'],
                    'currency'       => $item['currency'],
                    'amount'         => $item['amount'],
                    'confirmed'      => $item['confirmed'] == 't' ? true : false,
                    'available'      => $item['available'] == 't' ? true : false,
                    'user'           => ucfirst($item['first_name']) . ' ' . ucfirst($item['last_name'])
                ];
            }
            return $carry;
        });

        $campaign_conditions = [ [ 'key' => 'id', 'value' => $campaign_id ] ];
        $data['campaign']    = $this->Campaigns_model->select(0,$campaign_conditions)[0];
        $data['campaign_id'] = $data['campaign']['id'];

        $this->layout('manage_deliveries',$data);

    }

    public function markAsDelivered(){

        $this->load->model('Deliveries_model','',TRUE);

        $ids        = explode(',',$this->input->post('ids'));
        $comment    = $this->input->post('comment');
        $date       = $this->input->post('date');

        $response = [ "status" => 1 ];
        if ($this->Deliveries_model->mark_as_delivered($ids,$comment,$date)){
            $response = [ "status" => 0 ];
        }
        echo json_encode($response);

    }

    public function unmarkAsDelivered(){

        $this->load->model('Deliveries_model','',TRUE);

        $ids = explode(',',$this->input->post('ids'));

        $response = [ "status" => 1 ];
        if ($this->Deliveries_model->unmark_as_delivered($ids)){
            $response = [ "status" => 0 ];
        }
        echo json_encode($response);

    }

    private function sumUpByCurrency($deliveries){


        $result = [
            "ARS" => 0,
            "USD" => 0,
            "EUR" => 0
        ];

        foreach ($deliveries as $delivery){
            $result[$delivery['currency']] += $delivery['amount'];
        }

        return $result;

    }

    public function delete() {

        $this->check_log(['a']);
        $this->load->model('Deliveries_model','',TRUE);

        $ids = $this->input->post('ids');

        $result = $this->Deliveries_model->delete($ids);
        $_SESSION['delivery_removed'] = $result;
        $this->session->mark_as_flash('delivery_removed');
        echo json_encode(["status" => $result ? 0 : EXIT_ERROR]);

    }

    public function confirm($campaign_id){

        $user_id = $_SESSION['id'];

        $this->load->model('Final_deliveries_model','',TRUE);
        $this->load->model('Campaigns_model','',TRUE);

        //
        // select all the needed data for showing total amounts
        //

        $non_confirmed_filters = [
            [ 'key' => 'confirmed', 'value' => false ]
        ];
        $non_confirmed = $this->Final_deliveries_model->select_all(0,$non_confirmed_filters, $campaign_id);

        $confirmed_filters = [
            [ 'key' => 'confirmed', 'value' => true ]
        ];
        $confirmed = $this->Final_deliveries_model->select_all(1,$confirmed_filters, $campaign_id);

        //
        // do the additions
        //

        $data['amounts']['non_confirmed']   = $this->sumUpByCurrency($non_confirmed);
        $data['amounts']['confirmed']       = $this->sumUpByCurrency($confirmed);

        $deliveries = $this->Final_deliveries_model->select_all(0,[], $campaign_id);
        $data['deliveries'] = array_reduce($deliveries, function ($carry, $item){
                if (isset($carry[$item['id']])){
                $carry[$item['id']]['amount'] += $item['amount'];
                }
                else{
                $carry[$item['id']] = [
                    'id'             => $item['id'],
                    'reception_date' => $item['reception_date'],
                    'comments'       => $item['comments'],
                    'currency'       => $item['currency'],
                    'amount'         => $item['amount'],
                    'confirmed'      => $item['confirmed'] == 't' ? true : false,
                    'available'      => $item['available'] == 't' ? true : false,
                    'user'           => ucfirst($item['first_name']) . ' ' . ucfirst($item['last_name'])
                ];
            }
            return $carry;
        });

        $data['scripts'] 	     = ['confirm_deliveries.js'];
        $data['csrf_token_name'] = $this->security->get_csrf_token_name();
        $data['csrf_hash']       = $this->security->get_csrf_hash();
        $campaign_conditions     = [ [ 'key' => 'id', 'value' => $campaign_id ] ];
        $data['campaign']        = $this->Campaigns_model->select(0,$campaign_conditions)[0];

        $this->layout('commission_panel',$data);

    }

    public function markAsConfirmed(){

		$this->load->model('Final_deliveries_model','',TRUE);

        $ids        = explode(',',$this->input->post('ids'));
        
        $response = [ "status" => 1 ];
        if ($this->Final_deliveries_model->mark_as_confirmed($ids)){
            $response = [ "status" => 0 ];
        }
        echo json_encode($response);
        
    }

    public function unmarkAsConfirmed(){

		$this->load->model('Final_deliveries_model','',TRUE);

        $ids = explode(',',$this->input->post('ids'));
        
        $response = [ "status" => 1 ];
        if ($this->Final_deliveries_model->unmark_as_confirmed($ids)){
            $response = [ "status" => 0 ];
        }
        echo json_encode($response);
        
    }
    
}
?>
