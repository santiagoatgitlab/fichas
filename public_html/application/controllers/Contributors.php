<?php
defined('BASEPATH') OR exit('No direct script access allowed'); class Contributors extends MY_Controller { /** * Index Page for this controller.  * * Maps to the following URL * 		http://example.com/index.php/welcome *	- or - * 		http://example.com/index.php/welcome/index *	- or - * Since this controller is set as the default controller in * config/routes.php, it's displayed at http://example.com/ *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function new_contributor(){
 
		$this->load->model('Contributors_model','',TRUE);

        $first_name = $this->input->post('first_name');
        $last_name  = $this->input->post('last_name');
        $email      = $this->input->post('email');

        $contributor_data = [
            'first_name' => $first_name,
            'last_name'  => $last_name,
            'email'      => $email,
        ];

        $result = $this->Contributors_model->insert_row($contributor_data);

        $_SESSION['add_contributor_attempt_status'] = $result;
        $_SESSION['add_contributor_attempt_name']   = "$first_name $last_name";

        $this->session->mark_as_flash('add_contributor_attempt_status');
        $this->session->mark_as_flash('add_contributor_attempt_name');
        header('location: /admin');
        
    }
            
    public function edit($contributor_id){

		$this->load->model('Contributors_model','',TRUE);

        $contributor_id = $this->input->post('contributor_id');

        $data['contributor'] = $this->Contributors_model->select($contributor_id,1);
        $this->layout('contributor',$data);

    }

    public function get_all(){
            
		$this->load->model('Contributors_model','',TRUE);
        $data['contributors'] = $this->Contributors_model->select_all();
		$view = $this->twig->render('contributors_list',$data);
        echo json_encode([
            "status"    => 0,
            "content"   => $view
        ]);

    }

    public function update(){

		$this->load->model('Contributors_model','',TRUE);

        $key_value_pairs    = $this->input->post('key_value_pairs');
        unset($key_value_pairs['id']);

        $result = $this->Contributors_model->update($key_value_pairs['id'],$key_value_pairs);
        echo json_encode(["status" => $result ? 0 : EXIT_ERROR]);

    }

    public function delete(){

		$this->load->model('Contributor_model','',TRUE);

        $ids = $this->input->post('ids');

        $result = $this->Contributor_model->delete($ids);
        echo json_encode(["status" => $result ? 0 : EXIT_ERROR]);

    }

    public function search($search_str,$request_id){
        
		$this->load->model('Contributors_model','',TRUE);
        $search_str = implode(' ', explode('__SPACE__', $search_str));
        $contributors = $this->Contributors_model->select(0,[],5,$search_str);
        $data['contributors'] = $contributors;
		$view = $this->twig->render('contributors_for_search',$data);

        $response = [
            "status" => !empty($contributors) ? 0 : EXIT_ERROR,
            "content" => $view,
            "request_id" => $request_id
        ]; 
        echo json_encode($response);

    }


}
?>
