<?php
defined('BASEPATH') OR exit('No direct script access allowed'); class Campaigns extends MY_Controller { /** * Index Page for this controller.  * * Maps to the following URL * 		http://example.com/index.php/welcome *	- or - * 		http://example.com/index.php/welcome/index *	- or - * Since this controller is set as the default controller in * config/routes.php, it's displayed at http://example.com/ *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function create(){

        $this->check_log(['a']);

        if ( empty($this->input->post()) ){
            $menu  		                = Globals::get_basic_menu();
            $data['menu']               = $menu;
            $data['scripts'] 	        = ['new_campaign.js'];
            $data['csrf_token_name']    = $this->security->get_csrf_token_name();
            $data['csrf_hash']          = $this->security->get_csrf_hash();
            $this->layout('create_campaign',$data);
        }
        else{
            $this->load->model('Campaigns_model','',TRUE);
            $data = $this->input->post();
            $_SESSION['campaign_created'] = (boolean) $this->Campaigns_model->insert_row($data);
            header('location: /campanias');
        }

    }

    public function new_campaign(){
 
		$this->load->model('Campaigns_model','',TRUE);

        $campaign_data      = $this->input->post('campaign_data');

        $result = $this->Campaigns_model->insert_row($campaign_data);
        echo json_encode(["status" => $result ? 0 : EXIT_ERROR]);
    }
            
    public function edit($campaign_id){

		$this->load->model('Campaigns_model','',TRUE);

        $campaign_id = $this->input->post('campaign_id');

        $conditions = [
            [
                "key"       => "id",
                "value"     => $campaign_id,
            ]
        ];
        $data['campaign'] = $this->Campaigns_model->select(1,$conditions)[0];
        $this->layout('campaign',$data);

    }

    public function load_page(){
         
        // sends the type of users that can see this page 
        // r: recaudador
        // a: administrador
        $this->check_log(['r','a','c']);

		$menu  		             = Globals::get_basic_menu();
        $data['menu']            = $menu;
        $data['scripts'] 	     = ['campaigns.js'];
        $data['user_type']       = $_SESSION['user_type'];
        $data['csrf_token_name'] = $this->security->get_csrf_token_name();
        $data['csrf_hash']       = $this->security->get_csrf_hash();

        $this->layout('select_campaign',$data);

    }

	public function load_one($id){

        if ($_SESSION['user_type'] == 'c'){
            header('location: /confirmar_entregas/' . $id);
            exit;
        }
		$this->load->model('Campaigns_model','',TRUE);

        $data['status'] = CAMPAIGN_STATUS_EDITING;
        if (isset($_SESSION['campaign_status'])){
            /*if ($_SESSION['campaign_status'] == CAMPAIGN_STATUS_CARD_CREATED){*/
            $data['status'] = $_SESSION['campaign_status'];
        }
		$data['campaign_id']	    = $id;
        $menu                       = Globals::get_full_menu();
        $menu['campaign']           = false;

        $conditions = [
            [
                'key'   => 'id',
                'value' => $id
            ]
        ];

        $data['campaign']            = $this->Campaigns_model->select(1, $conditions)[0];
		$data['menu']  			     = $menu;
        $data['csrf_token_name']     = $this->security->get_csrf_token_name();
        $data['csrf_hash']           = $this->security->get_csrf_hash();
        $data['scripts'] 		     = ['campaign_cards.js'];
        $data['active_campaign']     = $this->Campaigns_model->isActive($id) ? true : false;
        $data['menu']['create_card'] = $data['active_campaign'] ? true : false;
        $this->layout('campaign',$data);

	}

    public function get_all(){
            
		$this->load->model('Campaigns_model','',TRUE);
        $data = $this->data;
        $data['campaigns'] = $this->Campaigns_model->select_all(1);
		$view = $this->twig->render('campaigns_list',$data);
        echo json_encode([
            "status"    => 0,
            "content"   => $view
        ]);

    }

    public function update(){

		$this->load->model('Campaigns_model','',TRUE);

        $key_value_pairs    = $this->input->post('key_value_pairs');
        unset($key_value_pairs['id']);

        $result = $this->Campaigns_model->update($key_value_pairs['id'],$key_value_pairs);
        echo json_encode(["status" => $result ? 0 : EXIT_ERROR]);

    }

    public function delete($id){

        $this->check_log(['a']);
		$this->load->model('Campaigns_model','',TRUE);

        $result = $this->Campaigns_model->delete($id);
        header('location: /campanias');

    }

}
?>
