<?php
defined('BASEPATH') OR exit('No direct script access allowed'); class Admin extends MY_Controller { /** * Index Page for this controller.  * * Maps to the following URL * 		http://example.com/index.php/welcome *	- or - * 		http://example.com/index.php/welcome/index *	- or - * Since this controller is set as the default controller in * config/routes.php, it's displayed at http://example.com/ *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function index(){

        $data['scripts'] = ['admin.js'];

        $data['add_contributor_attempt_status'] = -1;
        if (isset($_SESSION['add_contributor_attempt_status'])) {
            $data['add_contributor_attempt_status'] = $_SESSION['add_contributor_attempt_status'];
            $data['add_contributor_attempt_name']    = $_SESSION['add_contributor_attempt_name'];
        }

        $data['csrf_token_name'] = $this->security->get_csrf_token_name();
        $data['csrf_hash']       = $this->security->get_csrf_hash();
        $this->check_log(['a']);
        $this->layout('admin', $data);
    }
}
