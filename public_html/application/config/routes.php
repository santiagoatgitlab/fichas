<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['/']                                       = 'login/user_home';
$route['login']                                   = 'login';
$route['logout']                                  = 'login/logout';
$route['campanias']                               = 'campaigns/load_page';
$route['campania/(:num)']                         = 'campaigns/load_one/$1';
$route['nueva_campania']                          = 'campaigns/create';
$route['remove_campaign/(:num)']                  = 'campaigns/delete/$1';
$route['nueva_ficha/(:num)']                      = 'cards/new_card/$1';
$route['ficha/(:num)']                            = 'cards/show/$1';
$route['editar_ficha/(:num)']                     = 'cards/edit/$1';
$route['eliminar_ficha']                          = 'cards/remove';
$route['get_campaigns']                           = 'campaigns/get_all';
$route['get_campaign_cards/(:num)/(:any)/(:any)'] = 'cards/load_by_campaign/$1/$2/$3';
$route['search_contributors/(:any)/(:num)']       = 'contributors/search/$1/$2';
$route['search_campaign/(:any)']                  = 'cards/search_by_contributor/$1';
$route['nueva_entrega/(:num)']                    = 'deliveries/new_delivery/$1';
$route['editar_entrega/(:num)']                   = 'deliveries/update/$1';
$route['gestionar_entregas/(:num)']               = 'deliveries/manage/$1';
$route['confirmar_entregas/(:num)']               = 'deliveries/confirm/$1';
$route['mark_as_delivered']                       = 'deliveries/markAsDelivered';
$route['unmark_as_delivered']                     = 'deliveries/unmarkAsDelivered';
$route['mark_as_confirmed']                       = 'deliveries/markAsConfirmed';
$route['unmark_as_confirmed']                     = 'deliveries/unmarkAsConfirmed';
$route['remove_delivery']                         = 'deliveries/delete';
$route['admin']                                   = 'admin/index';
$route['agregar_aportante']                       = 'contributors/new_contributor';
$route['historial_comision']                      = 'welcome/commission_history';
$route['cards']                                   = 'welcome/cards';
$route['card']                                    = 'welcome/card';
$route['informe/(:num)']                          = 'reports/show/$1';
$route['informe']                                 = 'welcome/report';
$route['create_card']                             = 'welcome/create_card';
$route['tutorial']                                = 'welcome/tutorial';
// default
$route['default_controller']                = 'login/user_home';
$route['404_override']                      = '';
$route['translate_uri_dashes']              = FALSE;
