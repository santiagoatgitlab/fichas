<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller 
 { 

    function __construct(){
        parent::__construct();
        $current_url = current_url();
        $url_parts = explode('/',$current_url);
        if ( $url_parts[count($url_parts)-1] != 'login'){
            $this->check_log(['a','r','c']);
        }
    }
	
	var $data = array();
	
	public function check_log($user_types){

		if (!isset($_SESSION['id'])){
			header('location: /login');
            exit;
		}
		else{
            if (!in_array($_SESSION['user_type'],$user_types)){
                header('location: /');
                exit;
            }

			$this->data['id']         = $_SESSION['id'];
			$this->data['email']      = $_SESSION['email'];
			$this->data['first_name'] = $_SESSION['first_name'];
			$this->data['last_name']  = $_SESSION['last_name'];
			$this->data['user_type']  = $_SESSION['user_type'];
		}
		
	}	 
	 
	   //Load layout    
	public function layout($view,$data=array()) {
		 // making template and send data to view.
		$data = array_merge($this->data,$data);
		$data['main'] = $this->twig->render($view,$data);
		$this->twig->display('layout',$data);
   	}

}
