'use strict';

document.addEventListener("DOMContentLoaded", function() {
    init_dates();
})
function init_dates(){

    document.querySelector('.begin_date').addEventListener("change", function(e){
        let min_date = e.target.value;
        document.querySelector('.end_date').setAttribute('min', min_date);
    });

    document.querySelector('.end_date').addEventListener("change", function(e){
        let max_date = e.target.value;
        document.querySelector('.begin_date').setAttribute('max', max_date);
    });
}
