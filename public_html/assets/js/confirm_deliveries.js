'use strict'

const isConfirmed      = elem => elem.querySelector('.status').value == 2
const isNonConfirmed   = elem => elem.querySelector('.status').value == 1
const isChecked        = elem => elem.querySelector('.check') && elem.querySelector('.check').checked
const _confirm         = () => { changeStatus('/mark_as_confirmed', isNonConfirmed) }
const unconfirm        = () => { changeStatus('/unmark_as_confirmed', isConfirmed) }
const getContributions = () => Array.from(document.querySelectorAll('.delivery_item'))

document.addEventListener("DOMContentLoaded", function() {
    Array.from(document.querySelectorAll('input[type=checkbox]')).forEach(e => { e.checked = false })
    document.querySelector('#confirm').addEventListener('click', _confirm)
    document.querySelector('#unconfirm').addEventListener('click', unconfirm)
    Array.from(document.querySelectorAll('input[type=checkbox]')).forEach( elem => {
        elem.addEventListener('change', handleCheckboxChange)
    })
    handleCheckboxChange()
});

function handleCheckboxChange(){
    Array.from(document.querySelectorAll('button')).forEach( elem => elem.disabled = true )
    if (getContributions().filter(isNonConfirmed).some(isChecked)){
        document.querySelector('#confirm').disabled = false
    }
    if (getContributions().filter(isConfirmed).some(isChecked)){
        document.querySelector('#unconfirm').disabled = false
    }
}

function changeStatus(url, statusFilter){
    const ids = getContributions()
                .filter(isChecked)
                .filter(statusFilter)
                .map( elem => elem.querySelector('.id').value )
                .join(',')

    if (ids != ''){
        const fichas_rainbow = document.querySelector('.rainbow').value
        fetch( url, {
            method : 'POST',
            headers : {
                'Content-type' : 'application/x-www-form-urlencoded'
            },
            body : `ids=${ids}&fichas_rainbow=${fichas_rainbow}`
        })
        .then( response => response.json() )
        .then( () => { location.reload() })
        document.querySelector('.status_bar').style.display = 'block';
        document.querySelector('#confirm').disabled = true
        document.querySelector('#unconfirm').disabled = true
    }
}
