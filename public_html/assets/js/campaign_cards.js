let chars_left_for_request  = 3;
let current_timeout_id      = 0;
let timeout_time            = 400;
let order                   = 'date'
let filter                  = 'none'

document.addEventListener("DOMContentLoaded", function() {
	load_cards();
	let search_input = document.querySelector('.search input');
    search_input.addEventListener('keyup', function (event) {
        key_up();
    });
    document.querySelector('.sort-az').addEventListener('click', function(e){
        if (order == 'date'){
            order = 'asc'
            document.querySelector('.sort-az button').classList.remove('filter_disable');
        }
        else{
            order = 'desc'
            document.querySelector('.sort-az').style.display = 'none';
            document.querySelector('.sort-za').style.display = 'flex';
        }
        reload_cards()
    });
    document.querySelector('.sort-za').addEventListener('click', function(e){
        order = 'date'
        reload_cards()
        document.querySelector('.sort-za').style.display    = 'none';
        document.querySelector('.sort-az').style.display    = 'flex';
        document.querySelector('.sort-az button').classList.add('filter_disable');
    });
    document.querySelector('.owner-cards').addEventListener('click', function(e){
        if (filter === 'none'){
            document.querySelector('.owner-cards button').classList.remove('filter_disable');
            filter = 'owner'
        }
        else{
            document.querySelector('.owner-cards button').classList.add('filter_disable');
            filter = 'none'
        }
        reload_cards()
    });
});

function reset_buttons(){
    order   = 'date'
    filter  = 'none'
    document.querySelector('.sort-az button').classList.add('filter_disable');
    document.querySelector('.owner-cards button').classList.add('filter_disable');
    document.querySelector('.sort-za').style.display    = 'none';
    document.querySelector('.sort-az').style.display    = 'flex';
}

function key_up(){

    reset_buttons()
    document.querySelector('.cards_list').style.display = 'none';
    document.querySelector('.loader').style.display     = 'flex';
    chars_left_for_request--;
    window.clearTimeout(current_timeout_id);
    current_timeout_id = window.setTimeout(search, timeout_time);
    if (chars_left_for_request == 0){
        window.clearTimeout(current_timeout_id);
        search();
    }
}

function reload_cards(){
	document.querySelector('.search input').value = '';
    load_cards()
}

function load_cards(){

    let xhr = new XMLHttpRequest();
    let campaign_id = document.querySelector('#campaign_id').value;
    document.querySelector('.loader').style.display         = 'flex';
    document.querySelector('.cards_list').style.display     = 'none';

	// Setup our listener to process completed requests
	xhr.onload = function () {

		if (xhr.status >= 200 && xhr.status < 300) {

			let cards_box 	= document.querySelector('.cards_list');
			let response    = JSON.parse(xhr.responseText);
			
			if (response.status == 0){
				cards_box.innerHTML = response.content;
                document.querySelector('.loader').style.display     = 'none';
                document.querySelector('.cards_list').style.display = 'block';
			}

		} else {
            show_internet_warning()
		}

	};

	xhr.onerror = function () {
        show_internet_warning()
    }

	xhr.open('GET', '/get_campaign_cards/'+campaign_id + '/' + filter + '/' + order)
	xhr.send()

}

function search(){

	let search_str = encodeURIComponent(document.querySelector('.search input').value);

    if (search_str == ""){
        load_cards();
        return;
    }

    let xhr = new XMLHttpRequest();
    let campaign_id = document.querySelector('#campaign_id').value;
    let cards_box 	= document.querySelector('.cards_list');

    let rainbow 	= document.querySelector('#fichas_rainbow').value;
    let params      = 'search_str=' + search_str;

    params         += '&fichas_rainbow=' + rainbow;

    cards_box.innerHTML = "";

    console.log('searching: ' + search_str + ' for campaign: ' + campaign_id);

	// Setup our listener to process completed requests
	xhr.onreadystatechange = function () {

		if (xhr.status >= 200 && xhr.status < 300) {

			let response    = JSON.parse(xhr.responseText);
			
			if (response.status == 0){
				cards_box.innerHTML = response.content;
                document.querySelector('.loader').style.display         = 'none';
                document.querySelector('.cards_list').style.display     = 'block';
                document.querySelector('#fichas_rainbow').value         = response.rainbow;
			}

		} else {
			console.log('The request failed!');
		}

	};


	xhr.open('POST', '/search_campaign/'+campaign_id);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send(params);

    chars_left_for_request = 3;
}
