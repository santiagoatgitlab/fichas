document.addEventListener("DOMContentLoaded", function() {
    console.log('Campaigns file ready');
    load_campaigns();
});

function load_campaigns(){

    var xhr = new XMLHttpRequest();

    // Setup our listener to process completed requests
    xhr.onload = function () {

        if (xhr.status >= 200 && xhr.status < 300) {

            let campaigns_box = document.querySelector('.campaign_list');
            let response      = JSON.parse(xhr.responseText);
            
            if (response.status == 0){
                campaigns_box.innerHTML = response.content;
            }
            console.log('success!', response);

        } else {
            show_internet_warning()
        }

    };

    xhr.onerror = function () {
        show_internet_warning()
    }

    xhr.open('GET', '/get_campaigns');
    xhr.send();

}
