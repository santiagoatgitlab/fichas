document.addEventListener("DOMContentLoaded", function() {
	init_menu_button();
    Array.from(document.querySelectorAll('.close_modal, .cancel_modal')).forEach( elem => elem.addEventListener('click',(event) => {
        event.target.closest('.modal').style.display = 'none'
    }))
    Array.from(document.querySelectorAll('.open_modal')).forEach( elem => elem.addEventListener('click',(event) => {
        document.querySelector(`#${elem.dataset.modalName}`).style.display = 'flex'
    }))
});

function init_menu_button(){
	let menu_button = document.querySelector('.menu_toggle');
	if (menu_button != null){
		let nav 		= document.querySelector('nav');
		document.querySelector('body').addEventListener('click', function (event) {
			nav.style.display = 'none';
		});
		menu_button.addEventListener('click', function (event) {
            console.log('click on menu button');
            console.log('nav style display: ', nav.style.display);

			event.stopPropagation();
			if (nav.offsetParent === null){
				nav.style.display = 'block';
			}
			else{
				nav.style.display = 'none';
			}
		}, false);
	}
}

function show_internet_warning(){
    document.querySelector('#warning_internet').style.display = 'flex'
}
