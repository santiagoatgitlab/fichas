document.addEventListener("DOMContentLoaded", function() {
    if (! document.querySelector("#active_campaign").value){
        const inputs = ['input', 'select', 'textarea', 'button']
        document.querySelectorAll(inputs.join(',')).forEach( elem => elem.setAttribute('disabled', true));
    }

    document.querySelector('#delete-delivery').addEventListener('click', removeDelivery)
})

function removeDelivery() {
    const deliveryId     = document.querySelector('#delivery_id').value
    const fichas_rainbow = document.querySelector('.rainbow').value

    fetch( '/remove_delivery', {
        method : 'POST',
        headers : {
            'Content-type' : 'application/x-www-form-urlencoded'
        },
        body : `ids=${deliveryId}&fichas_rainbow=${fichas_rainbow}`
    })
    .then( response => response.json() )
    .then( result => { window.history.back() })
}
