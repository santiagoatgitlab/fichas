'use strict'

let selectedDeliveries = []

const fetchHeaders       = { 'Content-type' : 'application/x-www-form-urlencoded' }
const nonDeliveredFields = [ 'id', 'currency', 'commitment_amount' ]
const currencies         = [ 'ARS', 'USD', 'EUR' ]

const getRainbow       = () => document.querySelector('.rainbow').value
const getDate          = () => document.querySelector('#date').value
const getCheckboxes    = () => document.querySelectorAll('input[type=checkbox]')
const getContributions = () => Array.from(document.querySelectorAll('.delivery_item.checkable')).filter(isChecked)
const getComment       = () => encodeURIComponent(document.querySelector('#comment').value)
const closeCommentBox  = (event) => event.target.closest('.modal').style.display = 'none'

const isNonDelivered     = elem => elem.querySelector('.status').value == 0
const isDelivered        = elem => elem.querySelector('.status').value == 1
const isChecked          = elem => elem.querySelector('.check').checked
const mapDelivered       = elem => elem.querySelector('.id').value
const mapNonDelivered    = elem => nonDeliveredFields.reduce( getInnerFields, { elem, result : [] } ).result
const getFetchBody       = ids  => `ids=${ids}&comment=${getComment()}&fichas_rainbow=${getRainbow()}`
const getDeliveredBody   = ids  => `ids=${ids}&fichas_rainbow=${getRainbow()}`
const getUndeliveredBody = ids  => `ids=${ids}&date=${getDate()}&comment=${getComment()}&fichas_rainbow=${getRainbow()}`

const addEventListener      = (elem, eventName, target)         => elem.addEventListener(eventName, target)
const getFetchOptions       = (ids,getBody)                     => ({ method: 'POST', headers: fetchHeaders, body: getBody(ids) })
const appendIdIfCurrency    = (curr,idsList,[id,currency])      => curr == currency ? idsList.concat([id])   : idsList
const sumIfCurrency         = (curr, total,[_,currency,amount]) => curr == currency ? total+Number(amount)   : total
const addCorrespondingId    = ([currency, ids], delivery)       => [ currency, appendIdIfCurrency(currency,ids,delivery) ]
const sumCorrespondingValue = ([currency, amount], delivery)    => [ currency, sumIfCurrency(currency,amount,delivery) ]
const groupIdsByCurrency    = (idLists,delivery)                => idLists.map( idList => addCorrespondingId(idList,delivery) )
const sumByCurrency         = (amounts,delivery)                => amounts.map( amount => sumCorrespondingValue(amount,delivery) )
const fillUpCurrencySums    = ([currency, amount])              => document.querySelector(`.sums .${currency}`).innerHTML = `${new Intl.NumberFormat('es-AR').format(Number(amount))}`
const getInnerFields        = ({ elem, result},field)           => ({ elem, result : result.concat( getValueAmountOrContent(elem.querySelector(`.${field}`))) })

const getValueAmountOrContent = elem => elem.value != undefined ? elem.value : (elem.dataset.amount != undefined ? elem.dataset.amount : elem.innerHTML)

document.addEventListener("DOMContentLoaded", function() {
    document.querySelector('#deliverStep1').addEventListener('click', openDeliverModal)
    document.querySelector('#undeliver').addEventListener('click', undeliver)
    Array.from(getCheckboxes()).forEach( elem => {
        addEventListener(elem, 'change', handleCheckboxChange)
        elem.checked = false
    })
    handleCheckboxChange()
});

function handleCheckboxChange(){
    Array.from(document.querySelectorAll('button')).forEach( elem => elem.disabled = true )
    if (getContributions().filter(isNonDelivered).some(isChecked)){
        document.querySelector('#deliverStep1').disabled = false
    }
    if (getContributions().filter(isDelivered).some(isChecked)){
        document.querySelector('#undeliver').disabled = false
    }
}

function gatherSelected(statusFilter,statusMap){
    selectedDeliveries = getContributions().filter(statusFilter).map(statusMap)
}

function openDeliverModal(){
    gatherSelected(isNonDelivered, mapNonDelivered)
    const amounts = selectedDeliveries
                        .reduce(sumByCurrency, currencies.map( c => [ c, 0 ]))
                        .forEach(fillUpCurrencySums)
	document.querySelector('#comment_box').style.display = 'flex';
    document.querySelector('#deliver').addEventListener('click', deliver)
    document.querySelector('#deliver').disabled = false
}

function deliver(){
    runOperation(selectedDeliveries.reduce(groupIdsByCurrency, currencies.map( c => [ c, [] ]))
        .filter( ([_,idList]) => idList.length > 0 )
        .map( ([_,idList]) => idList.join(',')), '/mark_as_delivered', getUndeliveredBody
    )
}

function undeliver(){
    gatherSelected(isDelivered,mapDelivered)
    runOperation([selectedDeliveries.join(',')], '/unmark_as_delivered', getDeliveredBody)
}

function runOperation(ids, url, bodyGetter){

    if (ids.length > 0){
        let count = 0;
        ids.forEach( (currencyIds) => {
            fetch( url, getFetchOptions(currencyIds,bodyGetter) )
            .then( response => response.json() ) 
            .then( (response) => { count+1 == ids.length ? location.reload() : count++ } )
            .catch( error => { show_internet_warning() })
        })
        document.querySelector('.status_bar').style.display = 'block';
        document.querySelector('#deliverStep1').disabled = true
        document.querySelector('#undeliver').disabled = true
    }

}
