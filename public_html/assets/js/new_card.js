'use strict';

const settings = {
    log : true
}

let last_search_str = '';

const SEARCH_MAX_KEY_PRESS = 3;
const SEARCH_TIMEOUT = 300;

const form_errors = {
    contributors : "contributors",
    money : "money" 
}

const key_codes = {
    BACKSPACE   : 8,
    ENTER       : 13,
    UP_ARROW    : 38,
    DOWN_ARROW  : 40
}

let search_control = {
    onGoingTimeoutId    : 0,
    onGoingRequestId    : 0,
    pressedKeys         : 0,
    contributor_system  : null
}

document.addEventListener("DOMContentLoaded", function() {

    document.querySelector(".add_contributor").addEventListener("click",add_contributor);
    if (document.querySelector(".remove-card")) {
        document.querySelector(".remove-card").addEventListener("click",remove_card);
    }
    document.querySelector('.loader').style.display = 'none';
    init_contributors();
    init_amount_inputs();
    document.querySelector('form').reset();
    document.querySelectorAll('input.amount')[0].focus();

})

function init_contributors(){

    let inputs = document.querySelectorAll('input.contributor');
    let removeButtons = document.querySelectorAll('button.remove');

    for (let i = 0; i < inputs.length; i++){
        let new_input = inputs[i].cloneNode(true);
        
        new_input.value = inputs[i].value;
        inputs[i].parentNode.replaceChild(new_input,inputs[i]);

        new_input.addEventListener("keypress",function handleKeyPress(e) { 
            if (e.keyCode == key_codes.ENTER) {
                e.preventDefault(); 
                return false;
            }
        });
        new_input.addEventListener("keyup",function handleKeyUp(e) { search_key_up(this,e); });
        new_input.addEventListener("blur",function handleBlur() { hide_options(this) });
        new_input.addEventListener("change",function handleChange(e) {
            document.querySelector('.form_error.contributors').style.display = 'none';
            e.target.style.border = 'none';
        });
        new_input.focus();
    }

    for (let i = 0; i < removeButtons.length; i++){
        let new_button = removeButtons[i].cloneNode(true);
        
        removeButtons[i].parentNode.replaceChild(new_button,removeButtons[i]);

        new_button.addEventListener("click", function (){
            let contributor_system = this.closest('.contributor_system');
            if (document.querySelectorAll('.extra_contributor').length > 1){
                contributor_system.remove();
            }
            else{
                contributor_system.querySelector('input.contributor').value = '';
                contributor_system.querySelector('input.contributor_id').value = '';
                contributor_system.classList.add('idle');
            }
        });
    }
}

function init_amount_inputs(){
    const amount_inputs = document.querySelectorAll('input.amount');
    for (let amount_input of amount_inputs){
        amount_input.addEventListener("change",function handleChange(e) {
            document.querySelector('.form_error.money').style.display = 'none';
            e.target.style.border = 'none';
        });
    }
}

function add_contributor(){
    
    let extra_idle = document.querySelector(".extra_contributor.idle");
    let input_name = 'contributors[]';

    if (extra_idle !== null){
        extra_idle.classList.remove('idle');
        extra_idle.querySelector('input.contributor_id').name = input_name;
    }
    else{
        let extras_container = document.querySelector(".extra_contributors");
        let extra_last  = document.querySelector(".extra_contributor:last-child");
        let new_extra   = extra_last.cloneNode(true);
        new_extra.querySelector('input.contributor').value = '';
        extras_container.appendChild(new_extra);
    }
    
    init_contributors();
    document.querySelector('.extra_contributor:last-child input.contributor').focus();

}

function search_key_up(search_input,e){

    let contributor_system = search_input.closest('.contributor_system');
    switch (e.keyCode){
        case key_codes.ENTER :
            let option = contributor_system.querySelector('.options .option.selected');
            select_option(option);
            hide_options(search_input);
        break;
        case key_codes.UP_ARROW :
            mark_previous_option(search_input);
        break;
        case key_codes.DOWN_ARROW :
            mark_next_option(search_input);
        break;
        default:
            alter_search_status(contributor_system,e.keyCode);
        break;
    }

}

function hide_options(search_input){

    let contributor_system = search_input.closest('.contributor_system');
    contributor_system.querySelector('.options').style.display = 'none';
    rescend_pending_request()
}

function rescend_pending_request(){
    search_control.contributor_system   = null
    search_control.onGoingTimeoutId     = 0;
    search_control.onGoingRequestId     = 0;
    search_control.pressedKeys          = 0;
}

function alter_search_status(contributor_system,key_code){

    let request_sent = false;

    function reset_ongoing_timeout_if_set(){
        if (search_control.onGoingTimeoutId != 0){
            clearTimeout(search_control.onGoingTimeoutId);
        }
    }

    search_control.pressedKeys++;
    search_control.contributor_system = contributor_system;

    const options_length    = contributor_system.querySelectorAll('.options .option').length;
	const search_str        = contributor_system.querySelector('input.contributor').value;

    if (search_str.trim() != ""){
        if (search_control.onGoingTimeoutId == 0){
            reset_ongoing_timeout_if_set();
            search_control.onGoingTimeoutId = setTimeout(function (){
                send_options_request(); 
            }, SEARCH_TIMEOUT);
        }
        else if (search_control.pressedKeys == SEARCH_MAX_KEY_PRESS){
            reset_ongoing_timeout_if_set();
            send_options_request();
            request_sent = true;
        }
        if (!request_sent){
            remove_remaining_options(contributor_system);
        }
        if (last_search_str == ''){
            show_loader(contributor_system);
        }
    }
    else{
        reset_ongoing_timeout_if_set();
        hide_loader(contributor_system);
        hide_options(contributor_system.querySelector('input.contributor'));
        rescend_pending_request();
        last_search_str = '';
    }
}

function remove_remaining_options(contributor_system){
    const search_str    = contributor_system.querySelector('input.contributor').value;
    const options       = contributor_system.querySelectorAll('.options .option');
    if (options.length > 0){
        for (let option of options){
            const norm_option   = option.innerHTML.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
            const norm_search   = search_str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
            if (!norm_option.includes(norm_search)){
                option.remove();
            }
        }
    }
    if (contributor_system.querySelectorAll('.options .option').length == 0){
        show_loader(contributor_system);
    }
}

function show_loader(contributor_system){
    contributor_system.querySelector('.loader').style.display = 'flex';
    contributor_system.querySelector('.options').style.display = 'none';
}

function hide_loader(contributor_system){
    contributor_system.querySelector('.loader').style.display   = 'none';
    contributor_system.querySelector('.options').style.display  = 'block';
}

function send_options_request(){

    search_control.onGoingTimeoutId = 0;
    search_control.pressedKeys = 0;       


    let contributor_system = search_control.contributor_system;
    if (Date.now() - search_control.lastTimestamp < 1000){
        search_control.lastTimestamp = Date.now()
        return;
    }
    
	let search_str = contributor_system.querySelector('input.contributor').value;
    search_str = search_str.split(' ').join('__SPACE__')
    search_control.onGoingRequestId++;

    if (search_str == ""){
        return;
    }

    let options = contributor_system.querySelector('.options');
    let xhr     = new XMLHttpRequest();

	xhr.open('GET', '/search_contributors/'+search_str+'/'+search_control.onGoingRequestId);
	xhr.send();

	// Setup our listener to process completed requests
	xhr.onload = function () {

		if (xhr.status >= 200 && xhr.status < 300) {

			let response    = JSON.parse(xhr.responseText);
                        
            if (response.request_id == search_control.onGoingRequestId){
                if (response.status == 0){
                    options.innerHTML = response.content;
                }
                else{
                    options.innerHTML = "";
                }
                hide_loader(contributor_system);
            }
            else{
            }

		} else {
            show_internet_warning()
			hide_loader(contributor_system)
		}
        if (options.querySelectorAll('.option').length > 0){
            options.style.display = 'block';
            options.querySelector('.option:first-child').classList.add('selected');
            init_options();
        }

	};

	xhr.onerror = function () {
        show_internet_warning()
		hide_loader(contributor_system)
    }

    last_search_str = search_str;
}

function init_options(){
    let options = document.querySelectorAll('.options .option');
    for (let i = 0; i < options.length; i++){
        if (typeof handleOptionClick != 'undefined'){
            options[i].removeEventListener('mousedown',handleOptionClick);
        }
        if (typeof handleOptionHover != 'undefined'){
            options[i].removeEventListener('mouseover',handleOptionHover);
        }
        options[i].addEventListener('mousedown',function handleOptionClick() { select_option(this) });
        options[i].addEventListener('mouseover',function handleOptionHover() { mark_option(this) });
    }

}

function select_option(option){

    let contributor_system  = option.closest('.contributor_system');
    let id_input            = contributor_system.querySelector('input.contributor_id');
    let search_input        = contributor_system.querySelector('input.contributor');
    id_input.value          = option.dataset.id;
    search_input.value      = option.innerHTML;

}

function unmark_all_options(){
    let options = document.querySelectorAll('.options .option');
    for (let i = 0; i < options.length; i++){
        options[i].classList.remove('selected');
    }
}

function mark_option(option){
    unmark_all_options();
    option.classList.add('selected');
}

function mark_previous_option(search_input){
    let contributor_system      = search_input.closest('.contributor_system');
    let options                 = contributor_system.querySelector('.options');
    let current_marked_option   = contributor_system.querySelector('.options .option.selected');
    unmark_all_options();
    if (current_marked_option.previousElementSibling != null){
        current_marked_option.previousElementSibling.classList.add('selected');
    }
    else{
        options.querySelector('.option:last-child').classList.add('selected');
    }
}

function mark_next_option(search_input){
    let contributor_system      = search_input.closest('.contributor_system');
    let options                 = contributor_system.querySelector('.options');
    let current_marked_option   = contributor_system.querySelector('.options .option.selected');
    unmark_all_options();
    if (current_marked_option.nextElementSibling != null){
        current_marked_option.nextElementSibling.classList.add('selected');
    }
    else{
        options.querySelector('.option:first-child').classList.add('selected');
    }
}

function remove_card() {

    const id      = document.querySelector('#card_id').value
    const rainbow = document.querySelector('.rainbow').value

    fetch( '/eliminar_ficha', {
        method: 'POST',
        headers: { 'Content-type' : 'application/x-www-form-urlencoded' },
        body: `id=${id}&fichas_rainbow=${rainbow}`,
    })
    .then( response => response.json() )
    .then( (response) => {
        document.querySelector('.cancel_modal').click()
        document.querySelector('.back-to-campaign').click()
    } )
    .catch( error => { show_internet_warning() })

}

function validate_form(){
    document.querySelector(`.form_error`).style.display = 'none';
    let error_type = null;

    //contributors
    let inputs = document.querySelectorAll('.contributor_system:not(.idle) input.contributor');
    for (let input of inputs){
        const contributorName = input.value
        let options = input.closest('.contributor_system').querySelectorAll('.contributor_options .options li');
        let validContributor = false;
        for (let option of options){
            if (contributorName !== "" && contributorName === option.innerHTML){
                validContributor = true;
                break;
            }
        }
        if (!validContributor){
            error_type = form_errors.contributors;
            input.classList.add('input_with_error');
            break;
        }
    }

    //money
    if (error_type === null){
        error_type = form_errors.money;
        let inputs = document.querySelectorAll('input.amount');
        for (let input of inputs){
            if (input.value !== ""){
                error_type = null;
            }
        }
    }

    if (error_type !== null){
        document.querySelector(`.form_error.general`).style.display = 'block';
        document.querySelector(`.form_error.${error_type}`).style.display = 'block';
        return false;
    }
    
    return true;
}

function print_log(text_or_object){
    if (settings.log){
        console.log(text_or_object);
    }
}
